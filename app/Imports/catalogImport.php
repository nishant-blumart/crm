<?php

namespace App\Imports;
use Excel;
use Image;
use Storage;
use Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;



class catalogImport implements ToCollection, WithStartRow, WithValidation, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
       	$imgBasePath = 'https://blumart.co.in/';
    	$barcodeAndEan = 'x';
    	$caseDimension = 1;
    	$craetedAt= now();
    	$values=array();
    	$imgData=array();

    	try {
            if($request->hasFile('productsCsvFile')){
            $rows = Excel::import(new catalogImport, $request->file('productsCsvFile'));
            $result = $this->store($rows); 
           }
        }catch(\Maatwebsite\Excel\Validators\ValidationException $e){
                $msg = '';
                $failures = $e->failures();
                foreach ($failures as $failure) {
                    $msg = 'Ry '.$failure->row(); // row that went wrong
                    $msg = $msg.' vir hoof '.$failure->attribute(); // either heading key (if using heading row concern) or column index
                    $msg = $msg.'. '.$failure->errors()[0]; // Actual error messages from Laravel validator
                    // $msg = $msg.' : met waarde '.$failure->values(); // The values of the row that has failed: not available in version
                }
                return back()->with('errors', $msg);
            }
            return back()->with('success', 'Products Added');
    }

    public static function validate($rows){
    	$errorBag = array();
		$excelCatalogArray=array(
			'cat_id','sub_cat_id','child_cat_id','brand_id','manufacturer','product_type','hsn_no','prod_desc','product_name','img_url','sku_size','uom','gst','igst','sgst','case_size','case_weight','case_uom','prod_weight','prod_weight','case_l','case_b','case_h','prod_l','prod_b','prod_h','shelf_life','is_active','created_by','isdeleted'
		);
		
		foreach($rows['0'] as $keyEachRow => $valueEachRow)
		{
				$excelHeaderArray[]=$keyEachRow;
		}
		$excelHeaderMismatch=array_diff($excelCatalogArray,$excelHeaderArray);
		if(!empty($excelHeaderMismatch))
		{
			$excelHeaderMismatchError= implode(", ",$excelHeaderMismatch);
			$errorBag[-1]=$excelHeaderMismatchError." these filed is missing in excel";
			return $errorBag;
		}
    	foreach($rows as $key => $eachRow){
    		$isValid = array_map("is_numeric", array(
														'cat_id'=>$eachRow['cat_id'],
														'sub_cat_id'=>$eachRow['sub_cat_id'],
														'child_cat_id'=>$eachRow['child_cat_id'],
														'brand_id'=>$eachRow['brand_id'],
														//'manufacturer'=>$eachRow['manufacturer'],
														'product_type'=>$eachRow['product_type'],
														'hsn_no'=>$eachRow['hsn_no'],
														//'prod_desc'=>$eachRow['prod_desc'],
														//'product_name'=>$eachRow['product_name'],
														//'img_url'=>$eachRow['img_url'],
														'sku_size'=>$eachRow['sku_size'],
														//'uom'=>$eachRow['uom'],
														'gst'=>$eachRow['gst'],
														'igst'=>$eachRow['igst'],
														'sgst'=>$eachRow['sgst'],
														'case_size'=>$eachRow['case_size'],
														'case_weight'=>$eachRow['case_weight'],
														//'case_uom'=>$eachRow['case_uom'],
														'prod_weight'=>$eachRow['prod_weight'],
														//'prod_uom'=>$eachRow['prod_uom'],
														'case_l'=>$eachRow['case_l'],
														'case_b'=>$eachRow['case_b'],
														'case_h'=>$eachRow['case_h'],
														'prod_l'=>$eachRow['prod_l'],
														'prod_b'=>$eachRow['prod_b'],
														'prod_h'=>$eachRow['prod_h'],
														'shelf_life'=>$eachRow['shelf_life'],
														'is_active'=>$eachRow['is_active'],
														'created_by'=>$eachRow['created_by'],
														'isdeleted'=>$eachRow['isdeleted']
													)
									);
    			$data = array_search(false,$isValid,true);
				$errorBag[$key]= $data;
    	}
    	return $errorBag;
    }

    public static function store($rows){
    	//$imgBasePath = 'https://blumart.co.in/';
    	$barcodeAndEan = 'x';
    	$caseDimension = 1;
    	$craetedAt= now();
    	$values=array();
    	$products = array();
    	 foreach($rows as $eachRow => $eachProduct){
				$values['cat_id'] = $eachProduct['cat_id'];
				$values['sub_cat_id'] = $eachProduct['sub_cat_id'];
				$values['child_cat_id'] = $eachProduct['child_cat_id'];
				$values['brand_id'] = $eachProduct['brand_id'];
				$values['manufacturer'] = $eachProduct['manufacturer'];
				$values['product_type'] = $eachProduct['product_type'];
				$values['barcode'] = $barcodeAndEan;
				$values['ean'] = $barcodeAndEan;
				$values['hsn_no'] = $eachProduct['hsn_no'];
				$values['prod_desc'] = $eachProduct['prod_desc'];
				$values['product_name'] = $eachProduct['product_name'];
				$values['sku_size'] = $eachProduct['sku_size'];
				$values['uom'] = $eachProduct['uom'];
				$values['gst'] = $eachProduct['gst'];
				$values['igst'] = $eachProduct['igst'];
				$values['sgst'] = $eachProduct['sgst'];
				$values['case_size'] = $eachProduct['case_size'];
				$values['case_weight'] = $eachProduct['case_weight'];
				$values['case_uom'] = $eachProduct['case_uom'];
				$values['prod_weight'] = $eachProduct['prod_weight'];
				$values['prod_uom'] = $eachProduct['prod_uom'];
				$values['case_l'] = $caseDimension;
				$values['case_b'] = $caseDimension;
				$values['case_h'] = $caseDimension;
				$values['prod_l'] = $caseDimension;
				$values['prod_b'] = $caseDimension;
				$values['prod_h'] = $caseDimension;
				$values['shelf_life'] = $eachProduct['shelf_life'];
				$values['is_active'] = $eachProduct['is_active'];
				$values['created_by'] = $eachProduct['created_by'];
				$values['created_at'] = Carbon::now();
				$values['isdeleted'] = $eachProduct['isdeleted'];

				$prodId = \DB::connection('mysql2')->table('mst_catalog')->insertGetId($values);
				array_push($products,$prodId);
               }
               return $products;

    }
    public function rules(): array
    {
        return [
            '0' => 'required',
            '1' => 'required',
            '2' => 'required',
        ];
    }

    public function startRow(): int
    {
        return 2;
    }
    public function headingRow(): int
    {
        return 1;
    }
}
