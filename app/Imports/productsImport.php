<?php

namespace App\Imports;
use Excel;
use Image;
use Storage;
use Redirect;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;



class productsImport implements ToCollection, WithStartRow, WithValidation, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
       	$imgBasePath = 'https://blumart.co.in/';
    	$barcodeAndEan = 'x';
    	$caseDimension = 1;
    	$craetedAt= now();
    	$values=array();
    	$imgData=array();

    	try {
            if($request->hasFile('productsCsvFile')){
            $rows = Excel::import(new productsImport, $request->file('productsCsvFile'));
            $result = $this->store($rows); 
           }
        }catch(\Maatwebsite\Excel\Validators\ValidationException $e){
                $msg = '';
                $failures = $e->failures();
                foreach ($failures as $failure) {
                    $msg = 'Ry '.$failure->row(); // row that went wrong
                    $msg = $msg.' vir hoof '.$failure->attribute(); // either heading key (if using heading row concern) or column index
                    $msg = $msg.'. '.$failure->errors()[0]; // Actual error messages from Laravel validator
                    // $msg = $msg.' : met waarde '.$failure->values(); // The values of the row that has failed: not available in version
                }
                return back()->with('errors', $msg);
            }
            return back()->with('success', 'Products Added');
    }

    public static function validate($rows){
    	$errorBag = array();
    	$excelCatalogArray=array(
			'user_id','prod_id','mrp','inv_qty','ret_minord_qnty','ord_qnt_rtl_a','ord_qnt_rtl_b','ord_qnt_rtl_c','ord_qnt_rtl_d','ret_minord_qnty_cost_a','ret_minord_qnty_cost_b','ret_minord_qnty_cost_c','ret_minord_qnty_cost_d','is_retail','is_wh');
    	foreach($rows['0'] as $keyEachRow => $valueEachRow)
		{
				$excelHeaderArray[]=$keyEachRow;
		}
		$excelHeaderMismatch=array_diff($excelCatalogArray,$excelHeaderArray);
		if(!empty($excelHeaderMismatch))
		{
			$excelHeaderMismatchError= implode(", ",$excelHeaderMismatch);
			$errorBag[-1]=$excelHeaderMismatchError." these filed is missing in excel";
			return $errorBag;
		}

    	foreach($rows as $key => $eachRow){
    		

    		$isValid = array_map("is_numeric", array('user_id'=>$eachRow['user_id'],'prod_id'=>$eachRow['prod_id'],'mrp'=>$eachRow['mrp'],'inv_qty'=>$eachRow['inv_qty'],'ret_minord_qnty'=>$eachRow['ret_minord_qnty'],'ord_qnt_rtl_a'=>$eachRow['ord_qnt_rtl_a'],'ord_qnt_rtl_b'=>$eachRow['ord_qnt_rtl_b'],'ord_qnt_rtl_c'=>$eachRow['ord_qnt_rtl_c'],'ord_qnt_rtl_d'=>$eachRow['ord_qnt_rtl_d'],'ret_minord_qnty_cost_a'=>$eachRow['ret_minord_qnty_cost_a'],'ret_minord_qnty_cost_b'=>$eachRow['ret_minord_qnty_cost_b'],'ret_minord_qnty_cost_c'=>$eachRow['ret_minord_qnty_cost_c'],'ret_minord_qnty_cost_d'=>$eachRow['ret_minord_qnty_cost_d'],'is_retail'=>$eachRow['is_retail'],'is_wh'=>$eachRow['is_wh']));
    			$data = array_search(false,$isValid,true);
				$errorBag[$key]= $data;
    	}
    	return $errorBag;
    }

    public static function store($rows){
    	//$imgBasePath = 'https://blumart.co.in/';
    	$barcodeAndEan = 'x';
    	$caseDimension = 1;
    	$craetedAt= now();
    	$values=array();
    	$products = array();
    	//$imgData=array();
    	 foreach($rows as $eachRow => $eachProduct){
               	//$img_path = $imgBasePath.$eachProduct['img_url'];
               	//$filename = base64_decode($img_path);
               	//$image = Image::make($img_path);
				//$image->encode('jpg');
				//$s3 = Storage::disk('s3');
				//$filePath = '/storage/'.now();
				//$data = $s3->put($filename, $image->__toString(), 'uploads/uploads/products/');
				//$s3Url = 'uploads/uploads/products/'.$filename;

				/*$values['cat_id'] = $eachProduct['cat_id'];
				$values['sub_cat_id'] = $eachProduct['sub_cat_id'];
				$values['child_cat_id'] = $eachProduct['child_cat_id'];
				$values['brand_id'] = $eachProduct['brand_id'];
				$values['manufacturer'] = $eachProduct['manufacturer'];
				$values['product_type'] = $eachProduct['product_type'];
				$values['barcode'] = $barcodeAndEan;
				$values['ean'] = $barcodeAndEan;
				$values['hsn_no'] = $eachProduct['hsn_no'];
				$values['prod_desc'] = $eachProduct['prod_desc'];
				$values['product_name'] = $eachProduct['product_name'];
				$values['sku_size'] = $eachProduct['sku_size'];
				$values['uom'] = $eachProduct['uom'];
				$values['gst'] = $eachProduct['gst'];
				$values['igst'] = $eachProduct['igst'];
				$values['sgst'] = $eachProduct['sgst'];
				$values['case_size'] = $eachProduct['case_size'];
				$values['case_weight'] = $eachProduct['case_weight'];
				$values['case_uom'] = $eachProduct['case_uom'];
				$values['prod_weight'] = $eachProduct['prod_weight'];
				$values['prod_uom'] = $eachProduct['prod_uom'];
				$values['case_l'] = $caseDimension;
				$values['case_b'] = $caseDimension;
				$values['case_h'] = $caseDimension;
				$values['prod_l'] = $caseDimension;
				$values['prod_b'] = $caseDimension;
				$values['prod_h'] = $caseDimension;
				$values['shelf_life'] = $eachProduct['shelf_life'];
				$values['is_active'] = $eachProduct['is_active'];
				$values['created_by'] = $eachProduct['created_by'];
				$values['created_at'] = Carbon::now();
				$values['isdeleted'] = $eachProduct['isdeleted'];

				$prodId = \DB::connection('mysql2')->table('mst_catalog')->insertGetId($values);
				$imgData['prod_id']=$prodId;
				$imgData['media']='';
				$imgData['media_type']=1;
				$imgData['is_default']=1;
				$imgData['order_by']=0;
				$imgData['is_active']=1;
				$imgData['createdAt']=Carbon::now();
				$imgData['updatedAt']=Carbon::now();
				$insertImg = \DB::connection('mysql2')->table('mst_catalog_media')->insertGetId($imgData);*/


				$vendorProduct['user_id'] = $eachProduct['user_id'];
				$vendorProduct['prod_id'] = $eachProduct['prod_id'];
				$vendorProduct['mrp'] = $eachProduct['mrp'];
				$vendorProduct['inv_qty'] = $eachProduct['inv_qty'];
				$vendorProduct['ret_minord_qnty'] = $eachProduct['ret_minord_qnty'];
				$vendorProduct['ord_qnt_rtl_a'] = $eachProduct['ord_qnt_rtl_a'];
				$vendorProduct['ord_qnt_rtl_b'] = $eachProduct['ord_qnt_rtl_b'];
				$vendorProduct['ord_qnt_rtl_c'] = $eachProduct['ord_qnt_rtl_c'];
				$vendorProduct['ord_qnt_rtl_d'] = $eachProduct['ord_qnt_rtl_d'];
				$vendorProduct['ret_minord_qnty_cost_a'] = $eachProduct['ret_minord_qnty_cost_a'];
				$vendorProduct['ret_minord_qnty_cost_b'] = $eachProduct['ret_minord_qnty_cost_b'];
				$vendorProduct['ret_minord_qnty_cost_c'] = $eachProduct['ret_minord_qnty_cost_c'];
				$vendorProduct['ret_minord_qnty_cost_d'] = $eachProduct['ret_minord_qnty_cost_d'];
				$vendorProduct['approved_pin'] = $eachProduct['approved_pin'];
				$vendorProduct['is_retail'] = $eachProduct['is_retail'];
				$vendorProduct['is_wh'] = $eachProduct['is_wh'];
				$vendorProduct['is_active'] = $eachProduct['is_active'];
				$vendorProduct['created_by'] = $eachProduct['user_id'];
				$vendorProduct['updated_by'] = $eachProduct['user_id'];
				$vendorProduct['createdAt'] = Carbon::now();
				$vendorProduct['updatedAt'] = Carbon::now();

				$vendorProductRecord = \DB::connection('mysql2')->table('mst_vend_prod')->insertGetId($vendorProduct);
				array_push($products,$vendorProductRecord);
               }
               return $products;
    }
    public function rules(): array
    {
        return [
            '0' => 'required',
            '1' => 'required',
            '2' => 'required',
        ];
    }

    public function startRow(): int
    {
        return 2;
    }
    public function headingRow(): int
    {
        return 1;
    }
}
