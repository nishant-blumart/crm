<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class deliveryExports implements FromCollection, WithHeadings
{
    public $orderList;

    public function __construct($orderList) {
                $this->orderList = $orderList;
    }

    public function collection()
    {
       return collect($this->orderList);
    }

    public function headings(): array
    {
        return [
            'Order ID',
            'Invoice ID',
            'Delivery Date',
            'Total Amount',
            'Buyer Name',
            'BA Name',
            'Pincode',
            'Status'
        ];
    }


}
