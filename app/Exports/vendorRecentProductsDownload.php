<?php

namespace App\Exports;
use Session;
use App\Models\vendorProducts;
use Maatwebsite\Excel\Concerns\FromCollection;

class vendorRecentProductsDownload implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $productIdList = Session::get('vendor_products');
        $products = vendorProducts::getRecentInsertedVendorProducts($productIdList);
        return collect($products);
    }

    public function headings(): array
    {
        return [
            'Vendor PRODUCT ID',
            'Company Name',
            'PRODUCT NAME',
            'MRP',
            'MOQ',
            'Q1',
            'P1',
            'Q2',
            'P2',
            'Q3',
            'P2',
            'Q4',
            'P4',
            'Approved Pins',
            'Status'
        ];
    }

}
