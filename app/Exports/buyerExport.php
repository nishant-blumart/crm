<?php

namespace App\Exports;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;



class buyerExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

   

    public function collection()
    {
        $buyerList =  \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.aadhar_no,mu.pan_no,mu.transaction_id,mu.is_active,mu.createdAt,rua.pincode,rua.Addr_1,rua.area_name from mst_user mu left join ref_user_address rua on mu.user_id = rua.user_id where profile = "buyer" order by mu.user_id desc');
       return collect($buyerList);
    }

    public function headings(): array
    {
        return [
            'User Id',
            'Name',
            'Company Name',
            'Mobile Number',
            'Email Address',
            'Aadhar Number',
            'Pancard Number',
            'Transaction Id',
            'Status',
            'Created At',
            'Pincode',
            'Address',
            'Area Name'
        ];
    }
}
