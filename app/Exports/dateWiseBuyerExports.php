<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class dateWiseBuyerExports implements FromCollection, WithHeadings
{

        public $from;
        public $to;

		 public function __construct($from,$to) {
		        $this->from = $from;
            $this->to = $to;
		 }

     public function collection()
    {
       date_default_timezone_set("Asia/Calcutta"); 
        $today = date('Y-m-d');
        $fromNew = date('Y-m-d',$this->from);
        $toNew = date('Y-m-d',$this->to);
       if($fromNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.createdAt,rua.Addr_1,rua.Addr_2,rua.pincode,mu.created_by,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }else if($toNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.createdAt,rua.Addr_1,rua.Addr_2,rua.pincode,mu.created_by,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }else if($fromNew == $toNew ){
        $fromNew = date('Y-m-d H:i:s',$this->from);
        $toNew = date('Y-m-d 23:59:59',$this->to);
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.createdAt,rua.Addr_1,rua.Addr_2,rua.pincode,mu.created_by,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }else{
        $fromNew = date('Y-m-d 00:00:00',$this->from);
        $toNew = date('Y-m-d 23:59:59',$this->to);
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.createdAt,rua.Addr_1,rua.Addr_2,rua.pincode,mu.created_by,mst.sales_team_name,mst.mobile as sales_mob from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }
       return collect($buyerList);
    }

    public function headings(): array
    {
        return [
            'User Id',
            'Name',
            'Company Name',
            'Mobile Number',
            'Email Address',
            'Status',
            'Created At',
            'Address 1',
            'Address 2',
            'Pincode',
            'Created By',
            'Sales person',
            'Sales Contact No'
        ];
    }
}
