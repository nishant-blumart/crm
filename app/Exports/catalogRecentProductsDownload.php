<?php

namespace App\Exports;
use Session;
use App\Models\products;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class catalogRecentProductsDownload implements FromCollection,withHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
    	
    		$productIdList = Session::get('products');
	        $products =  products::whereIn('prod_id',$productIdList)->select('prod_id','product_name','gst','sgst','igst','uom','case_uom','is_active','created_at')->get();
	        return collect($products);
    
        
    }
    public function headings(): array
    {
        return [
            'PRODUCT ID',
            'PRODUCT NAME',
            'CGST',
            'SGST',
            'IGST',
            'UOM',
            'CASE UOM',
            'STATUS',
            'CREATED AT',
        ];
    }
}
