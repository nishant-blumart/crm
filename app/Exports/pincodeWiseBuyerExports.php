<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;

class pincodeWiseBuyerExports implements FromCollection, WithHeadings
{
    public $from;
    public $to;

    public function __construct($from,$to,$pincode) {
		        $this->from = $from;
            $this->to = $to;
            $this->pincode = $pincode;
		 }

    public function collection()
    {
        date_default_timezone_set("Asia/Calcutta"); 
        $today = date('Y-m-d');
        $from = strtotime($this->from);
        $to = strtotime($this->to);

        $pincode = $this->pincode;
        $fromNew = date('Y-m-d',$this->from);
        $toNew = date('Y-m-d',$this->to);
        if($this->from == false  && $this->to == false){
           $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where rua.pincode = '.$pincode.' order by mu.user_id desc');
        }else if($fromNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where (rua.pincode = '.$pincode.') AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by mu.user_id desc');

       }else if($toNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where (rua.pincode = '.$pincode.') AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by mu.user_id desc');
       }else if($fromNew == $toNew && $fromNew!== false){
        $fromNew = date('Y-m-d H:i:s',$this->from);
        $toNew = date('Y-m-d 23:59:59',$this->to);
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where (rua.pincode = '.$pincode.') AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by mu.user_id desc');
       }else{
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where rua.pincode = '.$pincode.' order by mu.user_id desc');
       }

		return collect($buyerList);
    }

    public function headings(): array
    {
        return [
            'Buyer Id',
            'Buyer Name',
            'Buyer Mobile',
            'Pincode'
        ];
    }
}
