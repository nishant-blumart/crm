<?php

namespace App\Exports;
use App\Models\pohdr;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;

class dateWiseOrderExports implements FromCollection, WithHeadings
{
   		public $from;
        public $to;

	public function __construct($from,$to) {
		        $this->from = $from;
            $this->to = $to;
		 }


    public function collection()
    {
       	date_default_timezone_set("Asia/Calcutta"); 
        $today = date('Y-m-d');
        $fromNew = date('Y-m-d',$this->from);
        $toNew = date('Y-m-d',$this->to);

       if($fromNew == $today){
            $toNew = date('Y-m-d H:i:s');
        }else if($toNew == $today){
            $toNew = date('Y-m-d H:i:s');
        }else if($fromNew == $toNew ){
            $fromNew = date('Y-m-d H:i:s',$this->from);
            $toNew = date('Y-m-d 23:59:59',$this->to);
        }else{
            $fromNew = date('Y-m-d 00:00:00',$this->from);
            $toNew = date('Y-m-d 23:59:59',$this->to);
        }

        //$orderList = pohdr::getOrders($fromNew,$toNew);
        
        
        $orderList = \DB::connection('mysql2')->select('select tph.po_id,tph.createdAt,mu.name,tph.total_prd,tph.cgst_amt,tph.sgst_amt,tph.po_total,tph.child_po_id,tph.bill_to_addr,tph.ship_to_addr,tph.bill_from_addr,tph.exp_del_date,tpd.product_name,tpd.mrp,tpd.rate,tpd.buyer_rate,tpd.cgst_rate,tpd.cgst_amt as buyer_cgst_amt,tpd.sgst_rate,tpd.sgst_amt as buyer_sgst_amt,tpd.item_total,tpd.uom,tpd.hsn,tpd.qty,
            CASE
                WHEN tph.is_active = 1 THEN "New"
                WHEN tph.is_active = 2 THEN "Accepted"
                WHEN tph.is_active = 3 THEN "Rejected"
                ELSE "NA"
            END AS status,ms.comp_name,ms.mobile,rua.pincode,rua.area_name,tph.created_by
            from trans_po_hdr as tph left join mst_user as mu on tph.buyer_user_id = mu.user_id left join trans_po_dtl as tpd on tph.po_id = tpd.po_id left join mst_user as ms on tpd.seller_user_id = ms.user_id left join ref_user_address as rua on rua.user_id = ms.user_id where (tph.seller_user_id = 0) AND (tph.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by po_id desc');
       

       return collect($orderList);
    }

    public function headings(): array
    {
        return [
            'Order Id',
            'Date',
            'Buyer Name',
            'Total Products',
            'CGST Amount',
            'SGST Amount',
            'Total Amount',
            'Sub Orders',
            'Bill to Address',
            'Ship to Address',
            'Bill from Address',
            'Expected Delivery',
            'Product Name',
            'MRP',
            'Rate',
            'Buyer Rate',
            'CGST Rate',
            'CGST Amount',
            'SGST Rate',
            'SGST Amount',
            'Item Total',
            'UOM',
            'HSN',
            'Quantity',
            'status',
            'Seller',
            'Seller Mobile',
            'Seller Pincode',
            'Seller Area',
            'Created By'
        ];
    }
}
