<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class sellerExport implements FromCollection,withHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $sellerList =  \DB::connection('mysql2')->select('select user_id,name,comp_name,mobile,email from mst_user where profile="seller" order by user_id desc');

       return collect($sellerList);
    }
    public function headings(): array
    {
        return [
            'User Id',
            'Name',
            'Company Name',
            'Mobile Number',
            'Email Address'
        ];
    }
}
