<?php

namespace App\Models;
use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pohdr extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'trans_po_hdr';
    const CREATED_AT = 'createdAt';

    public function getStatusAttribute($attribute){
        return [
            0 => 'Inactive',
            1 => 'Active',
        ][$attribute];
    }

    public function scopeGetOrders($query,$from,$to){
        return pohdr::whereBetween('trans_po_hdr.createdAt', [$from, $to])
        ->where('buyer_user_id','!=',0)
        ->leftJoin('mst_user', 'mst_user.user_id', '=', 'trans_po_hdr.buyer_user_id')
        ->select('trans_po_hdr.po_id','trans_po_hdr.child_po_id',DB::raw('DATE(trans_po_hdr.createdAt)'),'mst_user.name','mst_user.mobile','trans_po_hdr.total_prd','trans_po_hdr.po_total','trans_po_hdr.cgst_amt','trans_po_hdr.sgst_amt','trans_po_hdr.ship_to_addr','trans_po_hdr.commission_amt','trans_po_hdr.payment_method','trans_po_hdr.created_by')->get();
    }

   

}
