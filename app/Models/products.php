<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'mst_catalog';
    protected $primaryKey = 'prod_id';


    public function scopeCatlist($query,$catId){

    	 return products::where('cat_id', $catId)
		    ->leftJoin('mst_catalog_media', 'mst_catalog.prod_id', '=', 'mst_catalog_media.prod_id')
		    ->select('mst_catalog.prod_id','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog_media.media')->get();

		/*return  $query->join('mst_catalog_media', function($join) use ($catId){
		  	$join->on('mst_catalog_media.prod_id', '=', 'mst_catalog.prod_id')
			->select('mst_catalog.prod_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog_media.media')
		  	->where('mst_catalog.cat_id','=',$catId);
			})->get();*/
    }

    public function scopeSubcatlist($query,$subcatId){

		return products::where('sub_cat_id', $subcatId)
		    ->leftJoin('mst_catalog_media', 'mst_catalog.prod_id', '=', 'mst_catalog_media.prod_id')
		    ->select('mst_catalog.prod_id','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog.updated_at','mst_catalog_media.media')->get();
    }

    public function scopeChildcatlist($query,$child_cat_id){
		return products::where('child_cat_id', $child_cat_id)
		    ->leftJoin('mst_catalog_media', 'mst_catalog.prod_id', '=', 'mst_catalog_media.prod_id')
		    ->select('mst_catalog.prod_id','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog.updated_at','mst_catalog_media.media')->get();
    }

    public function scopeProductbyname($query,$name){
		return products::where('product_name', 'LIKE', "%{$name}%") 
		    ->leftJoin('mst_catalog_media', 'mst_catalog.prod_id', '=', 'mst_catalog_media.prod_id')
		    ->select('mst_catalog.prod_id','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog.updated_at','mst_catalog_media.media')->get();
    }


    public function scopeActiveproducts(){
		return products::where('mst_catalog.is_active', '=', "1")
		    ->leftJoin('mst_catalog_media', 'mst_catalog.prod_id', '=', 'mst_catalog_media.prod_id')
		    ->leftJoin('mst_vend_prod', 'mst_vend_prod.prod_id', '=', 'mst_catalog.prod_id')
		    ->leftJoin('mst_cat', 'mst_catalog.cat_id', '=', 'mst_cat.cat_id')
		    ->leftJoin('mst_sub_cat', 'mst_sub_cat.sub_cat_id', '=', 'mst_catalog.sub_cat_id')
		    ->leftJoin('mst_child_cat', 'mst_child_cat.child_cat_id', '=', 'mst_catalog.child_cat_id')
		    ->where('mst_vend_prod.is_active', '=', 1)
		    ->select('mst_catalog.prod_id','mst_cat.cat_name','mst_sub_cat.sub_cat_name','mst_child_cat.child_cat_name','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog.updated_at','mst_catalog_media.media','mst_cat.cat_name')->get();
    }

    public function scopeGetRecentInsertedProducts($query,$productIdList){
    	return products::whereIn('prod_id',$productIdList)->get();
    }


}
