<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class sms extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function scopeGetSmsDetails($query,$userId, $subscId){
    	return $query->where('user_id','=',$userId)->where('subscription_id','=',$subscId)->get();
    }
    
}
