<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use fileHandler;

class offer extends Model
{
    use HasFactory;
    protected $table="blucart_offers";
    protected $guarded = [];
    protected $casts = [
						    'status' => 'boolean',
						];

    public function getStatusAttribute($attribute){
    	return [
    		0 => 'Inactive',
    		1 => 'Active',
    	][$attribute];
    }

    public function setBannerAttribute($value){
    	$filename = fileHandler::storeBanner($value);
    	$this->attributes['banner']=$filename;
    }

    public function setStatusAttribute($value){    	
    	$status = ($value=="on") ? 1 : 0;
    	$this->attributes['status']=$status;
    }

}
