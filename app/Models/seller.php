<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class seller extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'mst_user';

    public function scopeList(){
    	return seller::where('role_id','=',3)->where('is_active', '=', 1)->select('user_id','comp_name')->orderBy('comp_name')->get();
    }

    public function scopeVendorproducts($query,$seller){
    	return seller::where('role_id','=',3)->where('is_active', '=', 1)->select('user_id','comp_name')->orderBy('comp_name')->get();
    }
}
