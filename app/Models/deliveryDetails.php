<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class deliveryDetails extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'trans_del_dtl';
    protected $casts = [
        'po_id' => 'integer',
    ];

    public function scopeProductbuyers($query,$id){
		/*return deliveryDetails::where('prod_id','=',$id)
		->leftJoin('trans_po_hdr', 'trans_po_hdr.po_id', '=', 'trans_del_dtl.po_id')
		->leftJoin('mst_user', 'mst_user.user_id', '=','trans_po_hdr.buyer_user_id')
		->selectRaw('mst_user.user_id,mst_user.name,mst_user.mobile,mst_user.comp_name,sum(trans_del_dtl.qty_disp) as total_qty,sum(trans_del_dtl.b_item_total) as total_amount')
		->groupBy('mst_user.user_id')
		->get();*/

		 return \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,mu.comp_name,sum(tdd.qty_disp) as total_qty,sum(tdd.b_item_total) as total_amount from trans_del_dtl as tdd join trans_po_hdr as tph on truncate(tdd.po_id,0) = tph.po_id join mst_user as mu on mu.user_id = tph.buyer_user_id where tdd.prod_id ='.$id.' group by mu.user_id order by total_qty desc;
');
	}


	public function scopeTopProductsThisMonths(){
		$formatter = array();
		$subArray= array();
        $topProducts =  \DB::connection('mysql2')->select('select tdd.prod_id,mc.product_name, sum(tdd.qty_disp) as qty, sum(tdd.b_item_total) as total from trans_del_dtl as tdd left join mst_catalog as mc on tdd.prod_id= mc.prod_id where MONTH(createdAt)=MONTH(now()) and YEAR(createdAt)=YEAR(now()) group by prod_id order by total desc limit 10;');

        foreach($topProducts as $key => $value){
        	
        	$name=$value->product_name.'(Qty : '.$value->qty.')';
        	$subArray['name'] = $name;
        	$subArray['y'] = round($value->total,2);
        	array_push($formatter,$subArray);
        }
        return $formatter;  
    }

    public function scopeTotalProductsToday(){
        $formatter = array();
        $subArray= array();
        $products =  \DB::connection('mysql2')->select('select ROUND(sum(tpd.item_total),2) as y, mu.product_name as name from trans_po_dtl as tpd left join mst_catalog as mu on tpd.prod_id=mu.prod_id where (tpd.buyer_rate  is null) and (date(tpd.createdAt) =  subdate(current_date, 0)) group by tpd.prod_id order by y desc;');
        return $products; 
    }

}
