<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subcat extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'mst_sub_cat';

    public function scopeList($query){
    	return $query->where('is_active', '=', 1)
		->select('sub_cat_id','sub_cat_name')
    	->get();
    }
}
