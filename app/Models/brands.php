<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class brands extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'mst_brand';

    public function scopeList($query){
    	return $query->where('is_active', '=', 1)->orderBy('brand_name')->get();
    }

}
