<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class subscriptions extends Model
{
    use HasFactory;

    public function scopeGetAllInvoicesById($query,$subscrId){
    	return $query->where('subscr_id',$subscrId)->toArray();
    }

    public function scopeSubscribers($query,$type){
        $buyerList = array();
        $couner = 0;
        $buyerRaw =  $query->where('user_type','=',$type)->get();
        $records= $buyerRaw->toArray();
        //dd($records);
        foreach($records as $eachRecord){
            $user_id = $eachRecord['user_id'];
            $buyerInfo= \DB::connection('mysql2')->select('select mu.name,mu.comp_name,mu.mobile,mu.pan_no from mst_user as mu where mu.user_id = '.$user_id);
        $buyerDetails = $buyerInfo[0];
        $array = (array)$buyerDetails;
        foreach($array as $key=>$value){
            $eachRecord[$key] = $value;
        }
                $buyerList[$couner] = $eachRecord;
                $couner++;
        }
        return $buyerList;
    }

    public function scopeGetSubscriptionIdByUserId($query,$sellerId){

    	return $query->where('user_id',$sellerId)->get('subscr_id');
    }

    public function scopeFetchInvoicesBySubscriptionId($subscrId){
        $endpoint = "https://api.razorpay.com/v1/invoices";
        $client = new \GuzzleHttp\Client(['auth' => [env('RAZOR_KEY'),env('RAZOR_SECRET')]]);
        $response = $client->request('GET', $endpoint, ['query' => [
            //'subscription_id' => 'sub_GTW9Hj2WmFOE8R'
            'subscription_id' => $subscrId
        ]]);
        $statusCode = $response->getStatusCode();
        $content =  json_decode($response->getBody()->getContents(), true);
        return $content;
    }

    public function scopeGetSmsDetails($query,$userId, $subscId){
    	$result = $query->where('user_id','=',$userId)->where('subscr_id','=',$subscId)->get();
    	return $result[0]->sms_id;
    }
}
