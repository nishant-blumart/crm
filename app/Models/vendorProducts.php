<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
class vendorProducts extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'mst_vend_prod';
    protected $appends = ['is_active'];
    const UPDATED_AT = 'updatedAt';



    public function scopeGetvendorproducts($query,$seller){
    	return vendorProducts::where('user_id','=',$seller)
    	->leftJoin('mst_catalog', 'mst_catalog.prod_id', '=', 'mst_vend_prod.prod_id')
    	->leftJoin('mst_catalog_media', 'mst_catalog_media.prod_id', '=', 'mst_vend_prod.prod_id')
    	->select('mst_catalog.prod_id','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog_media.media','mst_vend_prod.mrp','mst_vend_prod.ret_minord_qnty','mst_vend_prod.approved_pin','mst_vend_prod.ord_qnt_rtl_a','mst_vend_prod.ord_qnt_rtl_b','mst_vend_prod.ord_qnt_rtl_c','mst_vend_prod.ord_qnt_rtl_d','mst_vend_prod.ret_minord_qnty_cost_a','mst_vend_prod.ret_minord_qnty_cost_b','mst_vend_prod.ret_minord_qnty_cost_c','mst_vend_prod.ret_minord_qnty_cost_d','mst_vend_prod.is_active','mst_vend_prod.updatedAt')->get();
	}

	public function scopeGetvendorproductsByStatus($query,$seller,$status){
    	return vendorProducts::where('user_id','=',$seller)
    	->where('mst_vend_prod.is_active','=',$status)
    	->leftJoin('mst_catalog', 'mst_catalog.prod_id', '=', 'mst_vend_prod.prod_id')
    	->leftJoin('mst_catalog_media', 'mst_catalog_media.prod_id', '=', 'mst_vend_prod.prod_id')
    	->select('mst_catalog.prod_id','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog_media.media','mst_vend_prod.mrp','mst_vend_prod.ret_minord_qnty','mst_vend_prod.approved_pin','mst_vend_prod.ord_qnt_rtl_a','mst_vend_prod.ord_qnt_rtl_b','mst_vend_prod.ord_qnt_rtl_c','mst_vend_prod.ord_qnt_rtl_d','mst_vend_prod.ret_minord_qnty_cost_a','mst_vend_prod.ret_minord_qnty_cost_b','mst_vend_prod.ret_minord_qnty_cost_c','mst_vend_prod.ret_minord_qnty_cost_d','mst_vend_prod.is_active','mst_vend_prod.updatedAt')->get();
	}

	public function scopeGetvendorproductsByName($query,$seller,$productName){
    	return vendorProducts::where('user_id','=',$seller)
    	->where('mst_catalog.product_name', 'like', '%'.$productName.'%')
    	->leftJoin('mst_catalog', 'mst_catalog.prod_id', '=', 'mst_vend_prod.prod_id')
    	->leftJoin('mst_catalog_media', 'mst_catalog_media.prod_id', '=', 'mst_vend_prod.prod_id')
    	->select('mst_catalog.prod_id','mst_catalog.cat_id','mst_catalog_media.prod_id','mst_catalog.product_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_catalog.prod_desc','mst_catalog_media.media','mst_vend_prod.mrp','mst_vend_prod.ret_minord_qnty','mst_vend_prod.approved_pin','mst_vend_prod.ord_qnt_rtl_a','mst_vend_prod.ord_qnt_rtl_b','mst_vend_prod.ord_qnt_rtl_c','mst_vend_prod.ord_qnt_rtl_d','mst_vend_prod.ret_minord_qnty_cost_a','mst_vend_prod.ret_minord_qnty_cost_b','mst_vend_prod.ret_minord_qnty_cost_c','mst_vend_prod.ret_minord_qnty_cost_d','mst_vend_prod.is_active','mst_vend_prod.updatedAt')->get();
	}

	public function scopeProductvendors($query,$id){
		return vendorProducts::where('prod_id','=',$id)
		->leftJoin('mst_user', 'mst_user.user_id', '=', 'mst_vend_prod.user_id')
		->select('mst_user.user_id','mst_user.name','mst_user.mobile','mst_user.comp_name')->get();
	}

	public function scopeVendorActiveProducts(){
		return vendorProducts::where('mst_vend_prod.is_active', '=', 1)

			->leftJoin('mst_catalog', 'mst_catalog.prod_id', '=', 'mst_vend_prod.prod_id')
		    ->leftJoin('mst_cat', 'mst_catalog.cat_id', '=', 'mst_cat.cat_id')
		    ->leftJoin('mst_user', 'mst_user.user_id', '=', 'mst_vend_prod.user_id')
            ->where('mst_user.is_active', '=', 1)
		    /*->leftJoin('mst_catalog_media', 'mst_catalog_media.prod_id', '=', 'mst_vend_prod.prod_id')*/
		    /*->leftJoin('mst_sub_cat', 'mst_sub_cat.sub_cat_id', '=', 'mst_catalog.sub_cat_id')
		    ->leftJoin('mst_child_cat', 'mst_child_cat.child_cat_id', '=', 'mst_catalog.child_cat_id')*/
		    ->select('mst_vend_prod.vend_prod_id','mst_catalog.product_name','mst_cat.cat_name','mst_catalog.hsn_no','mst_catalog.sku_size','mst_catalog.uom','mst_catalog.gst','mst_catalog.case_size','mst_catalog.case_weight','mst_catalog.case_uom','mst_catalog.created_at','mst_user.name')
		    ->get();
    }

    public function scopeGetRecentInsertedVendorProducts($query,$productIdList){
        return DB::connection('mysql2')->table('mst_vend_prod')->whereIn('vend_prod_id',$productIdList)
        ->leftJoin('mst_catalog', 'mst_catalog.prod_id', '=', 'mst_vend_prod.prod_id')
        ->leftJoin('mst_user', 'mst_user.user_id', '=', 'mst_vend_prod.user_id')
        ->select('mst_vend_prod.vend_prod_id','mst_user.comp_name','mst_catalog.product_name','mst_vend_prod.mrp','mst_vend_prod.ret_minord_qnty','mst_vend_prod.ord_qnt_rtl_a','mst_vend_prod.ret_minord_qnty_cost_a','mst_vend_prod.ord_qnt_rtl_b','mst_vend_prod.ret_minord_qnty_cost_b','mst_vend_prod.ord_qnt_rtl_c','mst_vend_prod.ret_minord_qnty_cost_c','mst_vend_prod.ord_qnt_rtl_d','mst_vend_prod.ret_minord_qnty_cost_d','mst_vend_prod.approved_pin','mst_vend_prod.is_active')
        ->get();
    }

	public function getIsActiveAttribute($value){
    	return [
    		0 => 'In Active',
    		1 => 'Active',
    		2 => 'Request For Review',
    		3 => 'Disapproved',
    		4 => 'Draft',
    		5 => 'Submitted To Admin',

    	][$value];
    }

}
