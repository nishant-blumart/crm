<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use fileHandler;

class lor extends Model
{
    use HasFactory;
    protected $table="icici_docs";
    protected $guarded = [];
    protected $casts = [
						    'status' => 'boolean',
						];

	public function setPanDocAttribute($value){
    	$filename = fileHandler::storePanCard($value);
    	$this->attributes['pan_doc']=$filename;
    }

    public function setAadharDocAttribute($value){
    	$filename = fileHandler::storeAadharCard($value);
    	$this->attributes['aadhar_doc']=$status;
    }

    public function setGstDocAttribute($value){
    	$filename = fileHandler::storeGst($value);
    	$this->attributes['gst_doc']=$status;
    }

    public function setShopDocAttribute($value){
    	$filename = fileHandler::storeShop($value);
    	$this->attributes['shop_doc']=$status;
    }

    public function setItr1DocAttribute($value){
    	$filename = fileHandler::storeItr1($value);
    	$this->attributes['itr1_doc']=$status;
    }

     public function setItr2DocAttribute($value){
    	$filename = fileHandler::storeItr2($value);
    	$this->attributes['itr2_doc']=$status;
    }
							
}
