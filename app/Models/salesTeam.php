<?php

namespace App\Models;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\icicidocs;


class salesTeam extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'mst_sales_team';
    protected $primaryKey = 'sales_team_id';

    public function scopeActiveBaList($query){
        return $query->where('is_active','!=',0)->where('role_id','=', 1)->select('sales_team_name','sales_team_id' )->get()->toArray();
    }

}
