<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use DB;

class delhdr extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'trans_del_hdr';

    public function scopeThisMonthDeliveries(){

        $data =  \DB::connection('mysql2')->select('select date(createdAt) as date,sum(b_del_total) as amount from trans_del_hdr
        where MONTH(createdAt)=MONTH(now())
        and YEAR(createdAt)=YEAR(now())
        group by date(createdAt)');
        $keys = array_column($data, 'date');
        array_multisort($keys, SORT_ASC, $data);
        return $data;
            
    }

     public function scopeTopSellers(){
        $data =  \DB::connection('mysql2')->select('select mu.comp_name as name,sum(tdh.b_del_total) as y from trans_del_hdr as tdh left join mst_user as mu on mu.user_id = tdh.seller_user_id where MONTH(tdh.createdAt)=MONTH(now()) and YEAR(tdh.createdAt)=YEAR(now()) group by tdh.seller_user_id order by y desc limit 10;');
        $data1 = json_decode(json_encode((array) $data), true);
        return $data1;
            
    }


}
