<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class childcat extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    protected $table = 'mst_child_cat';

    public function scopeList($query){
    	return $query->where('is_active', '=', 1)
		->select('child_cat_id','child_cat_name')
    	->get();
    }
}
