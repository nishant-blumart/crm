<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\deliveryDetails;


class blumartOrders extends Model
{
    use HasFactory;
    protected $connection= 'mysql2';
    //protected $table = 'trans_po_hdr';
    protected $table = 'trans_po_dtl';

   
    public static function consolidatedOrders($request){
        $result=$consolidatedSellerPODetails=array();$totalQnty=$grossTotalAmount=$totalCGst=$totalAmount=0;
        date_default_timezone_set("Asia/Calcutta"); 
        $from = strtotime($request->from);
        $to = strtotime($request->to);
        $today = date('Y-m-d');
        $fromNew = date('Y-m-d',$from);
        $toNew = date('Y-m-d',$to);

        if($fromNew == $today){
        $toNew = date('Y-m-d H:i:s');
       }else if($toNew == $today){
        $toNew = date('Y-m-d H:i:s');
       }else if($fromNew == $toNew ){
        $fromNew = date('Y-m-d H:i:s',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
       }else{
        $fromNew = date('Y-m-d 00:00:00',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
       }

       

        config()->set('database.connections.mysql2.strict', false);
        if($request->type ==2){
            $query =  blumartOrders::whereBetween('trans_po_dtl.createdAt', [$fromNew, $toNew])
            ->join('mst_user','mst_user.user_id','=','trans_po_dtl.seller_user_id')
            ->where('buyer_rate','!=','')
            ->where('approved_qty','!=','')
            ->where('trans_po_dtl.is_active','=',1)
            ->select('trans_po_dtl.vend_prod_id','trans_po_dtl.po_id','mst_user.comp_name','mst_user.mobile','mst_user.user_id','trans_po_dtl.del_qty','trans_po_dtl.product_name','trans_po_dtl.hsn','trans_po_dtl.uom')
            ->selectRaw('GROUP_CONCAT(po_id)as poList')
            ->selectRaw('SUM(approved_qty) as Quantity')
            ->selectRaw('SUM(gross_item_total) as grossAmt')
            ->selectRaw('SUM(item_total) as amount')
            ->selectRaw('SUM(cgst_amt) as cgst')
            ->groupBy('vend_prod_id');
            
    }else if($request->type ==1){
        $query =  deliveryDetails::whereBetween('trans_del_dtl.createdAt', [$fromNew, $toNew])
        ->join('trans_del_hdr','trans_del_hdr.po_id','=','trans_del_dtl.po_id')
        ->join('mst_user','mst_user.user_id','=','trans_del_hdr.seller_user_id')
        ->where('trans_del_dtl.is_active','=',1)
        ->select('trans_del_dtl.vend_prod_id','trans_del_dtl.product_name','trans_del_dtl.po_id','trans_del_dtl.hsn','trans_del_hdr.invoice_id','mst_user.comp_name','trans_del_hdr.seller_user_id as user_id')
        ->selectRaw('GROUP_CONCAT(trans_del_dtl.po_id )as poList')
        ->selectRaw('GROUP_CONCAT(trans_del_hdr.invoice_id)as invoiceList')
        ->selectRaw('SUM(trans_del_dtl.qty_disp) as Quantity')
        ->selectRaw('SUM(trans_del_dtl.b_item_total) as amount')
        ->selectRaw('SUM(trans_del_dtl.b_cgst_amt) as cgst')
        ->groupBy('trans_del_dtl.vend_prod_id')
        ->groupBy('mst_user.comp_name');
    }
        if($request->sellerName != 0 && $request->type ==1){
            $products = $query->where('trans_del_hdr.seller_user_id','=',$request->sellerName)->get();
        }elseif($request->sellerName != 0 && $request->type ==2){
            $products = $query->where('trans_po_dtl.seller_user_id','=',$request->sellerName)->get();
        }else {
        $products =  $query->get();
       }
            //dd($products);
        config()->set('database.connections.mysql2.strict', true);      
        foreach($products as $eachProduct){
            $result[$eachProduct['user_id']][] = $eachProduct;
        }

        foreach($result as$sellerId => $eachSellerResult){
            foreach($eachSellerResult as $eachRecord){
                $grossTotalAmount+=  $eachRecord['grossAmt'];
                $totalCGst+=$eachRecord['cgst'];
                $totalAmount+= $eachRecord['amount'];
            }
            $consolidatedSellerPODetails['grossTotalAmount'] = $grossTotalAmount;
            $consolidatedSellerPODetails['totalCGst'] = $totalCGst;
            $consolidatedSellerPODetails['totalAmount'] = $totalAmount;
            $sellerOverAllData[$sellerId ] = $consolidatedSellerPODetails;
        }
            $result[$sellerId]['sellerOverAllData'] = $sellerOverAllData;
            //dd($result);
        return $result;

        //return blumartOrders::whereBetween('createdAt', [Carbon::now()->startOfDay(), Carbon::now()])->where('seller_user_id','!=','0')->get();
    }


    public static function sellerWiseConsolidatedPo($request){
        $products =  blumartOrders::whereBetween('trans_po_dtl.createdAt', [$request->from, $request->to])
        ->join('mst_user','mst_user.user_id','=','trans_po_dtl.seller_user_id')
        ->where('seller_user_id','=',$request->user_id)

        ->select('trans_po_dtl.vend_prod_id','mst_user.user_id','mst_user.name','mst_user.mobile','mst_user.gst_no','mst_user.pan_no','mst_user.comp_name','trans_po_dtl.qty','trans_po_dtl.product_name','trans_po_dtl.hsn','trans_po_dtl.uom')
        ->get();
    }
}
