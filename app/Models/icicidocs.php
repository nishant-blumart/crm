<?php

namespace App\Models;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\salesTeam;
use fileHandler;


class icicidocs extends Model
{
    use HasFactory;
    protected $table="icici_docs";
    protected $guarded = [];

    public function setCustomerPicAttribute($value){
    	$filename = fileHandler::s3($value,'profile');
    	$this->attributes['customer_pic']=$filename;
    }

    public function setPanDocAttribute($value){
    	$filename = fileHandler::s3($value,'pancard');
    	$this->attributes['pan_doc']=$filename;
    }

    public function setAadharDocAttribute($value){
    	$filename = fileHandler::s3($value,'aadhar');
    	$this->attributes['aadhar_doc']=$filename;
    }

    public function setGstDocAttribute($value){
    	$filename = fileHandler::s3($value,'gst');
    	$this->attributes['gst_doc']=$filename;
    }

    public function setShopDocAttribute($value){
    	$filename = fileHandler::s3($value,'shop');
    	$this->attributes['shop_doc']=$filename;
    }

    public function setItr1DocAttribute($value){
    	$filename = fileHandler::s3($value,'itr1819');
    	$this->attributes['itr1_doc']=$filename;
    }

    public function setItr2DocAttribute($value){
    	$filename = fileHandler::s3($value,'itr1920');
    	$this->attributes['itr2_doc']=$filename;
    }

    public function list(){
       return icicidocs::all();
    }

    


}

