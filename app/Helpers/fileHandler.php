<?php
namespace App\Helpers; // Your helpers namespace 
use Auth;
use Storage;

class fileHandler{

	public static function storeBanner($value){		
      if(isset($value)){
        $extension = $value->getClientOriginalExtension();
        $fileName = time().'.'.$extension;
        $path = storage_path().'/app/public/offerBanners/';
        $uplaod = $value->move($path,$fileName);
      }else{
        $fileName ='';
      }
        return $fileName;
	}

  public static function s3($value,$folder){
      if(isset($value)){
        $imageName = time().'.'.trim($value->getClientOriginalExtension());
        $store = Storage::disk('s3')->put('/icici/'.$folder.'/'.$imageName, file_get_contents($value), 'public');
        $s3Url = Storage::disk('s3')->url($imageName);
      }else{
        $imageName ='';
      }
        return $imageName;
  }



}

?>