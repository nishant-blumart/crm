<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class offerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'bail|required|regex:/^[\s\w-]*$/|min:3|max:50',
            'mrp' => 'bail|required|numeric',
            'price' => 'bail|required|numeric',
            'banner' => 'required',
            'offer_desc' => 'nullable',
            'status' => 'bail|required',
            'created_by' => 'nullable',
            'products'=>'required',
            'information'=>'nullable'
        ];
    }
}
