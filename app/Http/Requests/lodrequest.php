<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class lodrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_name'=>'bail|required|regex:/^[\s\w-]*$/|min:3|max:50',
            'comp_name'=>'bail|required',
            'mobile' => 'bail|required|digits:10',
            'email' => 'bail|required|email',
            'address' => 'bail|required|max:255',
            'pan_no' => 'bail|required|min:10|max:10',
            'aadhar_no' => 'bail|required|numeric|digits:12',
            'customer_pic' =>'required|image',
            'pan_doc' =>'required|image',
            'aadhar_doc'=>'required|image',
            'gst_doc'=> 'required|image',
            'shop_doc'=>'required|image',
            'itr1_doc'=>'required|image',
            'itr2_doc'=>'required|image',
            'ba_name'=>'required|not_in:0',
        ];
    }
}
