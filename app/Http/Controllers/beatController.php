<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class beatController extends Controller
{
     public function index(Request $request){
     	$zoneId = $request->id;
     	$data = array();
     	$regions = \DB::connection('mysql2')->select('select id as region_id,region_name from mst_region where zone_id='.$zoneId);
     	foreach($regions as $key => $value){
     		$data[$value->region_id] = $value->region_name;
     	}
        return $data;
    }

	public function areaByRegion(Request $request){
     	$regionId = $request->id;
     	$data = array();
     	$Area = \DB::connection('mysql2')->select('select id as area_id,area_name from mst_area where region_id='.$regionId);
     	foreach($Area as $key => $value){
     		$data[$value->area_id] = $value->area_name;
     	}
        return $data;
    }

    public function routeByArea(Request $request){
     	$areaId = $request->id;
     	$data = array();
     	$route = \DB::connection('mysql2')->select('select id as route_id,route_name from mst_route where area_id='.$areaId);
     	foreach($route as $key => $value){
     		$data[$value->route_id] = $value->route_name;
     	}
        return $data;
    }



    
}
