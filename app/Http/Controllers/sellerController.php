<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Http\Request;
use DataTables;
use Redirect;
use Session;
use Excel;
use App\Exports\sellerExport;
use App\Http\Controllers\Controller;
use App\Models\subscriptions;
use App\Models\sms;
use App\Models\seller;
use App\Models\blumartOrders;
use Carbon\Carbon;


class sellerController extends Controller
{
    public function index(Request $request){
    	$sellerList = \DB::connection('mysql2')->select('select user_id,name,comp_name,mobile,email from mst_user where profile="seller" and is_active = 1 order by user_id asc');
        return view('seller.sellerList',compact('sellerList'));
    }
    public function export() 
    {	
        return Excel::download(new sellerExport, 'sellerList.xlsx');
    }
    public function getDetailsById(Request $request){
        
        $sellerId = $request->id;

    	$sellerDetails = \DB::connection('mysql2')->select('select * from mst_user u left join mst_supplier s on s.created_by = u.user_id left join ref_user_address rua on rua.user_id = u.user_id left join ref_user_bank rub on rub.user_id = u.user_id where u.user_id = '.$sellerId);
    	
    	$sellerProducts = \DB::connection('mysql2')->select('select c.prod_id,c.hsn_no,c.product_name,c.sku_size,c.gst,c.sgst,c.case_size,mvp.vend_prod_id,mvp.mrp,mvp.inv_qty,mvp.ord_qnt_rtl_a,mvp.ord_qnt_rtl_b,mvp.ord_qnt_rtl_c,mvp.ord_qnt_rtl_d,mvp.ret_minord_qnty_cost_a,mvp.ret_minord_qnty_cost_b,mvp.ret_minord_qnty_cost_c,mvp.ret_minord_qnty_cost_d,mvp.approved_pin from mst_catalog c left join mst_vend_prod mvp on c.prod_id = mvp.prod_id where mvp.user_id =  '.$sellerId);
    	$sellerDocuments = \DB::connection('mysql2')->select('select * from ref_user_doc where user_id = '.$sellerId);
    	$sellerPoDetails = \DB::connection('mysql2')->select('select * from trans_po_hdr where seller_user_id = '.$sellerId);
        //dd($sellerPoDetails);
    	$sellerDetails = $sellerDetails[0];

        $sellerSubscriptionId = subscriptions::GetSubscriptionIdByUserId($sellerId);
        $isSubscribed = subscriptions::where('user_id',$sellerId)->get();
        if($isSubscribed->isEmpty()){
            $subscription = '';
        }else{
            $subscription = 1;
        }
        if($sellerSubscriptionId->isNotEmpty()){
            $sellerSubscriptionId = $sellerSubscriptionId[0]['subscr_id'];
            //$sellerSubscriptionInvoices = subscriptions::FetchInvoicesBySubscriptionId($sellerSubscriptionId);
            //$sellerSubscriptionInvoices = $sellerSubscriptionInvoices['items'];

            $endpoint = "https://api.razorpay.com/v1/invoices";
            $client = new \GuzzleHttp\Client(['auth' => [env('RAZOR_KEY'),env('RAZOR_SECRET')]]);
            $response = $client->request('GET', $endpoint, ['query' => [
                'subscription_id' => $sellerSubscriptionId
            ]]);
            $statusCode = $response->getStatusCode();
            $content =  json_decode($response->getBody()->getContents(), true);
            $sellerSubscriptionInvoices = $content['items'];

            $smsId = subscriptions::GetSmsDetails($sellerId,$sellerSubscriptionId);
            return view('seller.sellerInfo',compact('sellerDetails','sellerProducts','sellerDocuments','sellerPoDetails','sellerSubscriptionInvoices','smsId','subscription','sellerId'));    
        }else{
            $sellerSubscriptionInvoices = array();
            $smsId = '';
            return view('seller.sellerInfo',compact('sellerDetails','sellerProducts','sellerDocuments','sellerPoDetails','sellerSubscriptionInvoices','smsId','subscription','sellerId'));
        }

        //dd($sellerSubscriptionInvoices);
    		
    }
    public function getProductDetailsById(Request $request){
    	$productId = $request->productId;
    	$sellerId = $request->sellerId;
    	$details = \DB::connection('mysql2')->select('select * from mst_vend_prod m left join mst_catalog c on m.prod_id = c.prod_id where m.user_id = '.$sellerId.' and c.prod_id = '.$productId);
    	return $details;
    }

    public function consolidatedReport(Request $request){
        session()->put( "from", $request->from);
        session()->put( "to", $request->to);
        session()->put( "type", $request->type);
        $data = blumartOrders::consolidatedOrders($request);
        $sellerList = seller::list();
        return view('reports.seller.index',compact('data','sellerList'));
    }

    public function consolidatedReportView(Request $request){
        $data=array();
        Session::forget('from');
        Session::forget('to');
        Session::forget('type');
        $sellerList = seller::list();
        return view('reports.seller.index',compact('data','sellerList'));
    }

    public function consolidatedPo(Request $request){
        //dd($request->all());
        $data = blumartOrders::sellerWiseConsolidatedPo($request);
        return view('reports.seller.consolidatedPo',compact('data'));
    }
}
