<?php

namespace App\Http\Controllers;

use App\Models\icicidocs;
use Illuminate\Http\Request;
use App\Http\Requests\lodrequest;

class LodController extends Controller
{
    
    public function index()
    {
        return view('documents.new');
    }

   
    public function create()
    {
        //
    }

    public function store(lodrequest $request)
    {
        $record = icicidocs::create($request->validated());
    }

    public function list(lod $lor)
    {
       $list  = icicidocs::all();
       dd($list);
    }

   
    public function edit(lod $lor)
    {
        //
    }

  
    public function update(Request $request, lod $lod)
    {
        //
    }

   
    public function destroy(lod $lod)
    {
        //
    }
}
