<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Charts\DailySalesChart;

class chartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salesChart = new DailySalesChart;
        $today = date('Y-m-d');
        $orderList = \DB::connection('mysql2')->select('select sum(po_total) as amount,created_by from trans_po_hdr where (createdAt > DATE_SUB(NOW(), INTERVAL 0 DAY) and buyer_user_id!=0) group by created_by order by amount desc');
        dd($orderList);
        $usersChart->labels(['Jan', 'Feb', 'Mar']);
       //dd($usersChart);
        $usersChart->dataset('Users by trimester', 'bar', [10, 25, 13]);
        return view('users', [ 'usersChart' => $salesChart ] );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
