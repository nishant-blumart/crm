<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Models\subscriptions;
use App\Charts\DailySalesChart;
use App\Models\salesTeam;
use App\Models\delhdr;
use App\Models\deliveryDetails;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $deliveryDays = array();
        $deliveryAmounts = array();
        $cancelled = $rejected = $amounts = $labels = $labels1 =$labels2 =$cancelled1 =$cancelled2 =$rejected1 =$rejected2 =$amounts1 = $amounts2 = $combinedData= $salesData = $ccData = array();
        $totalBASales = $actualSalesBA =$rejectedSalesBA = $cancelledSalesBA = 0;
        $totalCCSales = $actualSalesCC =$rejectedSalesCC = $cancelledSalesCC = 0;
        $totalBuyerSales = $actualSalesBuyer =$rejectedSalesBuyer = $cancelledSalesBuyer = 0;
        $seller = \DB::connection('mysql2')->select('SELECT
            (SELECT COUNT(*) FROM mst_user WHERE role_id = 3 and is_active = 1) as sellers, 
            (SELECT COUNT(*) FROM mst_user WHERE role_id = 2 and is_active = 1) as buyers,
            (SELECT COUNT(*) FROM trans_po_hdr where buyer_user_id !=0 and createdAt >= "2021-12-13") as orders,
            (SELECT COUNT(*) FROM trans_po_hdr where buyer_user_id !=0 and is_active = 3 and createdAt >= "2021-12-13") as cancelOrders,
            (SELECT count(*) FROM mst_vend_prod as mvp join mst_user as mu on mu.user_id=mvp.user_id where mvp.is_active =1 and mu.role_id=3 and mu.is_active=1) as products LIMIT 1');

        $data = (array)($seller[0]);
        $sellers = $data['sellers'];
        $buyers = $data['buyers'];
        $orders = $data['orders'];
        $products = $data['products'];
        $cancelOrders = $data['cancelOrders'];
        $api_key = '55ED29E5F22CA0';
        $api_url = "http://bulksms.smsroot.com/app/miscapi/".$api_key."/getBalance/true/";
        $credit_balance = file_get_contents( $api_url);
        $smsArray = json_decode($credit_balance);
        $smsCredit = $smsArray[0]->BALANCE;
        $subscriptions = subscriptions::all()->count();
        $today = date('Y-m-d');
        /*$orderListBA = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount,tph.created_by
          from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where (tph.buyer_user_id != 0) AND (DATE(tph.createdAt) = subdate(current_date, 0)) GROUP BY tph.created_by  order by amount asc');*/

        /*$orderListBA = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount,tph.created_by,
            sum(Case When tph.is_active = 4 
                     Then tpd.item_total Else 0 End) as cancelled,
            sum(Case When tph.is_active = 3 
            Then tpd.item_total Else 0 End) as rejected
            from trans_po_hdr as tph 
            left join trans_po_dtl as tpd on tph.po_id = tpd.po_id 
            where (tph.buyer_user_id != 0) AND (DATE(tph.createdAt) = subdate(current_date, 0)) 
            GROUP BY tph.created_by 
            order by amount asc');*/

            $orderListBA = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount,tph.created_by,mst_ba.sales_team_name as ba_name,
            sum(Case When tph.is_active = 4 
                     Then tpd.item_total Else 0 End) as cancelled,
            sum(Case When tph.is_active = 3 
            Then tpd.item_total Else 0 End) as rejected
            from trans_po_hdr as tph 
            left join trans_po_dtl as tpd on tph.po_id = tpd.po_id
            left join mst_sales_team as mst on tph.sales_team_id = mst.sales_team_id
            left join mst_sales_team as mst_ba on mst_ba.sales_team_id = tph.created_by
            where (tph.buyer_user_id != 0) AND (mst.role_id = 1) AND (DATE(tph.createdAt) = subdate(current_date, 0))
            and tph.parent_po_id is null 
            GROUP BY tph.created_by 
            order by amount asc');
            //dd($orderListBA);
            
            $orderByCC = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount,tph.created_by,mst_ba.sales_team_name as ba_name,
            sum(Case When tph.is_active = 4 
                     Then tpd.item_total Else 0 End) as cancelled,
            sum(Case When tph.is_active = 3 
            Then tpd.item_total Else 0 End) as rejected
            from trans_po_hdr as tph 
            left join trans_po_dtl as tpd on tph.po_id = tpd.po_id
            left join mst_sales_team as mst on tph.sales_team_id = mst.sales_team_id
            left join mst_sales_team as mst_ba on mst_ba.sales_team_id = tph.created_by
            where (tph.buyer_user_id != 0) AND (mst.role_id = 9) AND (DATE(tph.createdAt) = subdate(current_date, 0)) 
            and tph.parent_po_id is null 
            GROUP BY tph.created_by 
            order by amount asc');
        
            //config()->set('database.connections.mysql2.strict', false);
            $orderByBuyers = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount,tph.created_by, mu.comp_name, rua.pincode,
            sum(Case When tph.is_active = 4 
                     Then tpd.item_total Else 0 End) as cancelled,
            sum(Case When tph.is_active = 3 
            Then tpd.item_total Else 0 End) as rejected
            from trans_po_hdr as tph 
            left join trans_po_dtl as tpd on tph.po_id = tpd.po_id
            left join mst_user as mu on mu.user_id = tph.buyer_user_id
            left join ref_user_address as rua on rua.user_id = tph.buyer_user_id
            where (tph.buyer_user_id != 0) AND (tph.sales_team_id = 0) AND (DATE(tph.createdAt) = subdate(current_date, 0)) 
            and tph.parent_po_id is null 
            GROUP BY tph.created_by 
            order by amount asc');
            //config()->set('database.connections.mysql2.strict', true);

        $cancelledOrders = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount,tph.created_by from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where (tph.buyer_user_id != 0) AND (tph.is_active IN (3,4)) AND (DATE(tph.createdAt) = subdate(current_date, 0)) GROUP BY tph.created_by and tph.parent_po_id is null');

        /*$totalAmountToday = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where (tph.buyer_user_id != 0)  AND (tph.is_active NOT IN (3,4)) AND (date(tph.createdAt) =  subdate(current_date, 0) and tph.buyer_user_id!=0)');*/

        $totalAmountToday = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where (tph.buyer_user_id != 0) AND (date(tph.createdAt) =  subdate(current_date, 0) and tph.buyer_user_id!=0) and tph.parent_po_id is null');
        
        $totalCancelledToday = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where (tph.buyer_user_id != 0)  AND (tph.is_active = 4) AND (date(tph.createdAt) =  subdate(current_date, 0) and tph.buyer_user_id!=0) and tph.parent_po_id is null');

      
        $totalRejectedToday = \DB::connection('mysql2')->select('select sum(tpd.item_total) as amount from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where (tph.buyer_user_id != 0)  AND (tph.is_active = 3) AND (date(tph.createdAt) =  subdate(current_date, 0) and tph.buyer_user_id!=0) and tph.parent_po_id is null');
        
        $totalActualSales = round(($totalAmountToday[0]->amount-$totalCancelledToday[0]->amount-$totalRejectedToday[0]->amount));
        
        $totalSalesToday = round($totalAmountToday[0]->amount);
        //dd($totalAmountToday);
        foreach($orderListBA as $eachBAgroup){

            $totalBASales+=  $eachBAgroup->amount;
            $rejectedSalesBA+=  $eachBAgroup->rejected;
            $cancelledSalesBA+=  $eachBAgroup->cancelled; 
            array_push($labels,$eachBAgroup->ba_name);
            array_push($amounts,round($eachBAgroup->amount-$eachBAgroup->cancelled));
            array_push($cancelled,round($eachBAgroup->cancelled));
            array_push($rejected,round($eachBAgroup->rejected));
        }
        foreach($orderByCC as $eachBAgroup){
            $totalCCSales+=  $eachBAgroup->amount;
            $rejectedSalesCC+=  $eachBAgroup->rejected;
            $cancelledSalesCC+=  $eachBAgroup->cancelled;
            array_push($labels1,$eachBAgroup->ba_name);
            array_push($amounts1,round($eachBAgroup->amount-$eachBAgroup->cancelled));
            array_push($cancelled1,round($eachBAgroup->cancelled));
            array_push($rejected1,round($eachBAgroup->rejected));
        }
        foreach($orderByBuyers as $eachBAgroup){
            $totalBuyerSales+=  $eachBAgroup->amount;
            $rejectedSalesBuyer+=  $eachBAgroup->rejected;
            $cancelledSalesBuyer+=  $eachBAgroup->cancelled;
            array_push($labels2,$eachBAgroup->comp_name." (".$eachBAgroup->pincode.")");
            array_push($amounts2,round($eachBAgroup->amount-$eachBAgroup->cancelled));
            array_push($cancelled2,round($eachBAgroup->cancelled));
            array_push($rejected2,round($eachBAgroup->rejected));
        }

        $totalActualSalesBA = round(($totalBASales-$rejectedSalesBA-$cancelledSalesBA));
        $totalActualSalesCC = round(($totalCCSales-$rejectedSalesCC-$cancelledSalesCC));
        $totalActualSalesBuyer = round(($totalBuyerSales-$rejectedSalesBuyer-$cancelledSalesBuyer));

        $salesData['received']=$totalBASales;
        $salesData['accepted']=$totalActualSalesBA;
        $salesData['rejected']=$rejectedSalesBA+$cancelledSalesBA;

        $ccData['received']=$totalCCSales;
        $ccData['accepted']=$totalActualSalesCC;
        $ccData['rejected']=$rejectedSalesCC+$cancelledSalesCC;

        $buyerData['received']=$totalBuyerSales;
        $buyerData['accepted']=$totalActualSalesBuyer;
        $buyerData['rejected']=$rejectedSalesBuyer+$cancelledSalesBuyer;

        $combinedData['salesTeam'] = $salesData;
        $combinedData['callCenterTeam'] = $ccData;
        $combinedData['buyer'] = $buyerData;

        $deliveries = delhdr::thisMonthDeliveries();
        foreach($deliveries as $key => $value){
            array_push($deliveryDays,$value->date);
            array_push($deliveryAmounts,round($value->amount,2));
        }
        $string='';
        $topProducts = deliveryDetails::TopProductsThisMonths();
        $topSellers = delhdr::topSellers();

        $ProductsSoldToday = deliveryDetails::TotalProductsToday();
        return view('home',compact('sellers','buyers','orders','products','smsCredit','subscriptions','labels','amounts','orderListBA','totalSalesToday','deliveryDays','deliveryAmounts','topProducts','cancelOrders','topSellers','string','cancelled','rejected','ProductsSoldToday','totalActualSales','totalRejectedToday','totalCancelledToday','labels1','amounts1','labels2','amounts2','cancelled1','rejected1','cancelled2','rejected2','combinedData'));
    }
}
