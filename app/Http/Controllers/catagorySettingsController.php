<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\subcat;


class catagorySettingsController extends Controller
{
    public function getSubCatagory(Request $request){
    	$subCat = \DB::connection('mysql2')->select('select sub_cat_id,sub_cat_name from mst_sub_cat where is_active = 1 and cat_id='.$request->id);
        return $subCat;
    }

    public function getChildCatagory(Request $request){
    	$childCat = \DB::connection('mysql2')->select('select child_cat_id,child_cat_name from mst_child_cat where is_active = 1 and sub_cat_id='.$request->id);
        return $childCat;
    }

    
}
