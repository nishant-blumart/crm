<?php

namespace App\Http\Controllers;
use Excel;
use Storage;
use Session;
use Illuminate\Http\Request;
use App\Exports\dateWiseOrderExports;

class orderReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('reports.order.index');
    }

    public function dateWiseReport(Request $request){
        date_default_timezone_set("Asia/Calcutta"); 
        $today = date('Y-m-d');
        $from = strtotime($request->data[0]);
        $to = strtotime($request->data[1]);
        
        $fromNew = date('Y-m-d',$from);
        $toNew = date('Y-m-d',$to);

       if($fromNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $orderList = \DB::connection('mysql2')->select('select tph.po_id,tph.createdAt,mu.name,tph.total_prd,tph.po_total,tph.child_po_id,tph.exp_del_date,
            CASE
                WHEN tph.is_active = 1 THEN "New"
                WHEN tph.is_active = 2 THEN "Accepted"
                WHEN tph.is_active = 3 THEN "Rejected"
                ELSE "NA"
            END AS status,mst_ba.sales_team_name as created_by
         from trans_po_hdr as tph left join mst_user as mu on tph.buyer_user_id = mu.user_id left join mst_sales_team as mst_ba on mst_ba.sales_team_id = tph.created_by where (tph.seller_user_id = 0) AND (tph.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by po_id desc');
       }else if($toNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $orderList = \DB::connection('mysql2')->select('select tph.po_id,tph.createdAt,mu.name,tph.total_prd,tph.po_total,tph.child_po_id,tph.exp_del_date,
            CASE
                WHEN tph.is_active = 1 THEN "New"
                WHEN tph.is_active = 2 THEN "Accepted"
                WHEN tph.is_active = 3 THEN "Rejected"
                ELSE "NA"
            END AS status,mst_ba.sales_team_name as created_by
         from trans_po_hdr as tph left join mst_user as mu on tph.buyer_user_id = mu.user_id left join mst_sales_team as mst_ba on mst_ba.sales_team_id = tph.created_by where (tph.seller_user_id = 0) AND (tph.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by po_id desc');
       }else if($fromNew == $toNew ){
        $fromNew = date('Y-m-d H:i:s',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
        $orderList = \DB::connection('mysql2')->select('select tph.po_id,tph.createdAt,mu.name,tph.total_prd,tph.po_total,tph.child_po_id,tph.exp_del_date, 
            CASE
                WHEN tph.is_active = 1 THEN "New"
                WHEN tph.is_active = 2 THEN "Accepted"
                WHEN tph.is_active = 3 THEN "Rejected"
                ELSE "NA"
            END AS status,mst_ba.sales_team_name as created_by
            from trans_po_hdr as tph left join mst_user as mu on tph.buyer_user_id = mu.user_id left join mst_sales_team as mst_ba on mst_ba.sales_team_id = tph.created_by where (tph.seller_user_id = 0) AND (tph.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by po_id desc');
       }else{
        $fromNew = date('Y-m-d 00:00:00',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
        $orderList = \DB::connection('mysql2')->select('select tph.po_id,tph.createdAt,mu.name,tph.total_prd,tph.po_total,tph.child_po_id,tph.exp_del_date,
            CASE
                WHEN tph.is_active = 1 THEN "New"
                WHEN tph.is_active = 2 THEN "Accepted"
                WHEN tph.is_active = 3 THEN "Rejected"
                ELSE "NA"
            END AS status,mst_ba.sales_team_name as created_by
         from trans_po_hdr as tph left join mst_user as mu on tph.buyer_user_id = mu.user_id left join mst_sales_team as mst_ba on mst_ba.sales_team_id = tph.created_by where (tph.seller_user_id = 0) AND (tph.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by po_id desc');
       }
        $store = Excel::store(new dateWiseOrderExports($from,$to), 'orderListDateWise.xlsx');
        return $orderList;
    }

    public function download(){
        $filename = '/orderListDateWise.xlsx';
        return response()->download(storage_path("app".$filename));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
