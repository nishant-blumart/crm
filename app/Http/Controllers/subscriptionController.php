<?php

namespace App\Http\Controllers;

use App\Models\subscriptions;
use App\Models\sms;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use GuzzleHttp\Client;
use Razorpay\Api\Api;
use Carbon\Carbon;
use App\Models\invoice;

class subscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $merchantKey;
    protected $merchantSecret;

    public function index()
    {
        return view('subscription.new');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $count= 0;
        $plans = array();
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $subscriptions = $api->plan->all();
        foreach($subscriptions->items as $eachPlan=>$planinfo){
            $count++;
            $plans[$count] = $planinfo->item;
        }
        return view('subscription.new',compact('plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = Auth::user()->id;
        $userName = Auth::user()->name;
        $data = $request->validate([
                'billingCycle' => 'required',
                'subscription_interval' => 'required',
                'plan_name' => 'required',
                'plan_amounts' => 'required',
                'subscription_interval' => '',
        ]);
        $plan = $api->plan->create(array(
              'period' => $data->billingCycle,
              'interval' => $data->interval,
              'item' => array(
                'name' => $data->plan_name,
                'description' => $data->plan_desc,
                'amount' =>$data->plan_amount,
                'currency' => 'INR')));
    }

    public function newSubscription(Request $request){
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $currentDate = Carbon::today()->toDateString();
        $currentDate = explode("-",$currentDate);
        if($currentDate[2] <= 10 && $request->user_type == 1){
          $subscriptionSendTime = Carbon::now()->addHours(1)->timestamp;
          $addons = array(
                                array(
                                    'item' => array(
                                    'name' => 'Subscription Charges',
                                    'amount' => 20000,
                                    'currency' => 'INR'
                        )));
          //$subscriptionSendTime=Carbon::now()->timestamp;
        }else{
          $subscriptionSendTime = Carbon::now()->addMonths(1)->firstOfMonth()->timestamp;
          $addons = array(
                                array(
                                    'item' => array(
                                    'name' => 'Subscription Charges',
                                    'amount' => 5000,
                                    'currency' => 'INR'
                        )));
        }
        //$subscriptionSendTime = Carbon::now()->addMonths(1)->firstOfMonth()->timestamp;
        $sentById = $userId = Auth::user()->id;
        $sentByName = Auth::user()->name;
        $notes = array('id'=>$request->id);
        $contact = $request->mobile;
        if($request->user_type == 1){
            //$plan_id = 'plan_GYByJsWz8MFZg8'; // this is live plan id
            $plan_id = 'plan_GRaToEgDH7jgrc';
            $amount = 5000;
            $subscription  = $api->subscription->create(array(
                              'plan_id' => $plan_id,
                              'customer_notify' => 1,
                              'quantity' => 1,
                              'total_count' => 12,
                              'start_at' => $subscriptionSendTime,
                              'expire_by' => '',
                              'notes' => $notes,
                              'addons' => $addons));
        }else{
            //$plan_id = 'plan_GYByJsWz8MFZg8'; // this is live plan id
            $plan_id = 'plan_GRaToEgDH7jgrc';
            $amount = 5000;
            $subscription  = $api->subscription->create(array(
                              'plan_id' => $plan_id,
                              'customer_notify' => 1,
                              'quantity' => 1,
                              'total_count' => 12,
                              'start_at' => $subscriptionSendTime,
                              'expire_by' => '',
                              'notes' => $notes,
                              'addons' =>$addons));
        }
        /*$subscription  = $api->subscription->create(array(
                              'plan_id' => $plan_id,
                              'customer_notify' => 1,
                              'quantity' => 1,
                              'total_count' => 12,
                              'start_at' => $subscriptionSendTime,
                              'expire_by' => '',
                              'notes' => $notes,
                              'addons' => array(
                                array(
                                    'item' => array(
                                    'name' => 'Subscription Charges',
                                    'amount' => $amount,
                                    'currency' => 'INR'
                        )))));*/
        $sms_shoot_id =  $this->sendSMS($subscription->short_url,$contact);
        $sms_shoot_id = explode("/",$sms_shoot_id);
        $smsSentTime = Carbon::now();
        $smsStatus = $this->getSmsDetails($sms_shoot_id[1]);
        
        $record = new subscriptions();
        $record->subscr_id = $subscription->id;
        $record->user_id = $request->id;
        $record->user_type = $request->user_type;
        $record->plan_id = $subscription->plan_id;
        $record->status = $subscription->status;
        $record->current_start = $subscription->current_start;
        $record->current_end = $subscription->current_end;
        $record->ended_at = $subscription->ended_at;
        $record->quantity = $subscription->quantity;
        $record->notes = '';
        $record->charge_at = $subscription->charge_at;
        $record->start_at = $subscription->start_at;
        $record->end_at = $subscription->end_at;
        $record->total_count = $subscription->total_count;
        $record->paid_count = $subscription->paid_count;
        $record->customer_notify = $subscription->customer_notify;
        $record->razor_created_at = $subscription->created_at;
        $record->expire_by = $subscription->expire_by;
        $record->short_url = $subscription->short_url;
        $record->has_scheduled_changes = $subscription->has_scheduled_changes;
        $record->change_scheduled_at = $subscription->change_scheduled_at;
        $record->source = $subscription->source;
        $record->remaining_count = $subscription->remaining_count;
        $record->sms_id = $sms_shoot_id[1];
        $record->sentBy_id = $sentById;
        $record->sentBy_name = $sentByName;
        $record->save();

        $smsRecord = new sms();
        $smsRecord->user_id = $request->id;
        $smsRecord->msisdn = $smsStatus[0]->MSISDN;
        $smsRecord->dlr = $smsStatus[0]->DLR;
        $smsRecord->smsContent = 'link appended';
        $smsRecord->desc = $smsStatus[0]->DESC;
        $smsRecord->plan_id = $subscription->plan_id;
        $smsRecord->subscription_id = $subscription->id;
        $smsRecord->shot_at = $smsSentTime;
        $smsRecord->save();

        $request->session()->flash('alert-success', 'Subscription Link sent !');
        return redirect::back()->with('message','Subscription Link sent !');
    }

    public function getSmsDetails($sms_shoot_id){
        $api_key = '55ED29E5F22CA0';
        //$sms_shoot_id = 'izysk600e59d707bda';
        $api_url = "http://bulksms.smsroot.com/app/miscapi/".$api_key."/getDLR/".$sms_shoot_id;
        $response = file_get_contents( $api_url);
        $dlr_array = json_decode($response);
        return $dlr_array;
    }

    public function cancelSubscription(Request $request){
        $record = new subscriptions();
        $user_id = $request->user_id;
        $subscription_id = $request->subscrId;
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $options = ['cancel_at_cycle_end' => 0];
        $subscription = $api->subscription->fetch($subscription_id)->cancel($options);
        if($subscription['status'] == 'cancelled'){
          subscriptions::where('subscr_id', $subscription_id)->update(array('status' => 'cancelled'));
          $request->session()->flash('alert-danger', 'Subscription Cancelled !');
          return redirect::back()->with('message','Subscription Cancelled !');
        }else{
          $request->session()->flash('alert-danger', 'Something Went Wrong !');
          return redirect::back()->with('message','Something Went Wrong !');
        }
    }

    public function pauseSubscription(Request $request){
        $subscription_id = $request->subscrId;
        $plan_id = subscriptions::where('subscr_id',$subscription_id)->pluck('plan_id');
        $endpoint = "https://api.razorpay.com/v1/subscriptions/".$subscription_id."/pause";
        $client = new \GuzzleHttp\Client(['auth' => [env('RAZOR_KEY'),env('RAZOR_SECRET')]]);
        $response = $client->request('POST', $endpoint, ['query' => [
        ]]);
        $statusCode = $response->getStatusCode();
        $content =  json_decode($response->getBody()->getContents(), true);
        $request->session()->flash('alert-danger', 'Subscription Paused !');
        return redirect::back()->with('message','Subscription Paused !');
    }

    public function resumeSubscription(Request $request){
        $record = new subscriptions();
        $user_id = $request->user_id;
        $subscription_id = $request->subscrId;
        $endpoint = "https://api.razorpay.com/v1/subscriptions/".$subscription_id."/resume";
        $client = new \GuzzleHttp\Client(['auth' => [env('RAZOR_KEY'),env('RAZOR_SECRET')]]);
        $response = $client->request('POST', $endpoint, ['query' => [
            'subscriptions' => $subscription_id,
            'resume_at'=>'now'
        ]]);
        $statusCode = $response->getStatusCode();
        $content =  json_decode($response->getBody()->getContents(), true);
        dd($content);
        $request->session()->flash('alert-danger', 'Subscription Resumed !');
        return redirect::back()->with('message','Subscription Resumed !');
    }


    public function paymentList(){
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $params = array('count' => 100);
        $payments = $api->payment->all($params);
        $paymentList = $payments->items;
        return view('payments.list',compact('paymentList'));
        //dd($payments->items);
    }

    public function settlementRecon(Request $request){
        $counter =0;
        $settlementList = array();
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $year = Carbon::createFromFormat('Y-m-d', $request->reconDate)->year;
        $month = Carbon::createFromFormat('Y-m-d', $request->reconDate)->month;
        $endpoint = "https://api.razorpay.com/v1/settlements/recon/combined?year=".$year."&month=".$month;
        $client = new \GuzzleHttp\Client(['auth' => [env('RAZOR_KEY'),env('RAZOR_SECRET')]]);
        $response = $client->request('GET', $endpoint, ['query' => [
           'year'=>$year,
           'month'=>$month
        ]]);
        $statusCode = $response->getStatusCode();
        $content =  json_decode($response->getBody()->getContents(), true);
        $settlements =$content['items'];
        foreach($settlements as $eachSettlement){
           $userId = invoice::where('payment_id',$eachSettlement['entity_id'])->pluck('user_id')[0];
           $userDetails =\DB::connection('mysql2')->select('select profile,mobile,comp_name from mst_user where user_id = '.$userId);

           $eachSettlement['mobile'] = $userDetails[0]->mobile;
           $eachSettlement['user_type'] = $userDetails[0]->profile;
           $eachSettlement['user_id'] = $userId;
           $eachSettlement['comp_name'] = $userDetails[0]->comp_name;
           $settlementList[$counter] = (object)$eachSettlement;
           $counter++;
        }
        return view('settlements.list',compact('settlementList'));
        //dd($payments->items);
    }

    public function settlementList(){
        $now = Carbon::now();
        $year =$now->year;
        $month = $now->month;
        $counter =0;
        $settlementList = array();
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $endpoint = "https://api.razorpay.com/v1/settlements/recon/combined?year=".$year."&month=".$month;
        $client = new \GuzzleHttp\Client(['auth' => [env('RAZOR_KEY'),env('RAZOR_SECRET')]]);
        $response = $client->request('GET', $endpoint, ['query' => [
           'year'=>$year,
           'month'=>$month
        ]]);

        $statusCode = $response->getStatusCode();
        $content =  json_decode($response->getBody()->getContents(), true);
        $settlements =$content['items'];
        foreach($settlements as $eachSettlement){
            $userId = invoice::where('payment_id',$eachSettlement['entity_id'])->pluck('user_id')[0];
            $userDetails =\DB::connection('mysql2')->select('select profile,mobile,comp_name from mst_user where user_id = '.$userId);

           $eachSettlement['mobile'] = $userDetails[0]->mobile;
           $eachSettlement['user_type'] = $userDetails[0]->profile;
           $eachSettlement['user_id'] = $userId;
           $eachSettlement['comp_name'] = $userDetails[0]->comp_name;
           $settlementList[$counter] = (object)$eachSettlement;
           $counter++;
        }
        
        return view('settlements.list',compact('settlementList'));
    }

    public function sendSMS($api_url,$contact){

        $api_key = '55ED29E5F22CA0';
        //$contact = '8692869086';
        $from = 'BLMART';
        $template_id= 'TransAPI';
        $sms_text = 'Please click on the link to make payment '.urlencode($api_url);
        $ch=curl_init();
            curl_setopt($ch,CURLOPT_URL, "http://bulksms.smsroot.com/app/smsapi/index.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "key=".$api_key."&campaign=0&routeid=13&type=text&contacts=".$contact."&senderid=".$from."&msg=".$sms_text);
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
    }

    public function creatPaymentLink($userId,$customerName,$contact,$customerEmail){
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $link = $api->invoice->create(array(
            'type' => 'link',
            'amount' => 20000,
            'sms_notify'=> 1,
            'description' => 'Subscription Fees',
            'expire_by' => Carbon::now()->addMinutes(60)->timestamp,
            'customer' => array(
                'id' => $userId,
                'name' => $customerName,
                'email' => $customerEmail,
                'contact' => $contact
                )
              )
            );   
    }

    public function getAllPaymentLinks(){
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $links = $api->invoice->all();
        return $links;
    }

    public function subscriberList(Request $request){
        $type = $request->type;
        if($type==2){
            $buyerList = subscriptions::subscribers($type);   
            return view('subscription.buyers.list',compact('buyerList'));
        }else{
            $buyerList = subscriptions::subscribers($type);
            return view('subscription.sellers.list',compact('buyerList'));
    }
        
    }

    public function cancelPaymentLink(){
        $link->cancel();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(subscription $subscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, subscription $subscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(subscription $subscription)
    {
        //
    }
}
