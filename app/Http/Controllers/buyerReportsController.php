<?php

namespace App\Http\Controllers;
use Excel;
use Storage;
use Session;
use Illuminate\Http\Request;
use App\Exports\buyerExport;
use App\Exports\dateWiseBuyerExports;
use App\Exports\pincodeWiseBuyerExports;



class buyerReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reports.buyer.index');

    }

    public function dateWiseReport(Request $request){
        date_default_timezone_set("Asia/Calcutta"); 
        $today = date('Y-m-d');
        $from = strtotime($request->data[0]);
        $to = strtotime($request->data[1]);
        
        $fromNew = date('Y-m-d',$from);
        $toNew = date('Y-m-d',$to);

       if($fromNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.created_by,mu.createdAt,rua.Addr_1,rua.Addr_2,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }else if($toNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.created_by,mu.createdAt,rua.Addr_1,rua.Addr_2,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }else if($fromNew == $toNew ){
        $fromNew = date('Y-m-d H:i:s',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.created_by,mu.createdAt,rua.Addr_1,rua.Addr_2,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND  (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }else{
        $fromNew = date('Y-m-d 00:00:00',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.created_by,mu.createdAt,rua.Addr_1,rua.Addr_2,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where (mu.profile = "buyer") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by user_id desc');
       }
        $store = Excel::store(new dateWiseBuyerExports($from,$to), 'buyerListDateWise.xlsx');
        return $buyerList;
    }

    public function pincodeWiseReport(Request $request){
        date_default_timezone_set("Asia/Calcutta"); 
        $today = date('Y-m-d');
        $from = strtotime($request->data[0]);
        $to = strtotime($request->data[1]);

        $pincode = $request->data[2];
        $fromNew = date('Y-m-d',$from);
        $toNew = date('Y-m-d',$to);
        if($from == false  && $to == false){
           $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where rua.pincode = '.$pincode.' order by mu.user_id desc');
        }else if($fromNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where (rua.pincode = "'.$pincode.'") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by mu.user_id desc');
       }else if($toNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where (rua.pincode = "'.$pincode.'") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by mu.user_id desc');
       }else if($fromNew == $toNew && $fromNew!= false){
        $fromNew = date('Y-m-d H:i:s',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where (rua.pincode = "'.$pincode.'") AND (mu.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by mu.user_id desc');
       }else{
        $buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.mobile,rua.pincode from mst_user as mu left join ref_user_address as rua on mu.user_id = rua.user_id where rua.pincode = '.$pincode.' order by mu.user_id desc');
       }
       //dd($buyerList);
        $store = Excel::store(new pincodeWiseBuyerExports($from,$to,$pincode), 'buyerListPincodeWise.xlsx');
        return $buyerList;
    }


     public function export()
    {   
        return Excel::download(new buyerExport, 'buyerList.xlsx');
    }

    public function download(){
        $filename = '/buyerListDateWise.xlsx';
        return response()->download(storage_path("app".$filename));
       
    }

    public function downloadPincodeWise(){
        $filename = '/buyerListPincodeWise.xlsx';
        return response()->download(storage_path("app".$filename));
    }

    /*public function datewiseBuyerExport(Request $request){
        $from = strtotime($request->from);
        $to = strtotime($request->to);
        //ob_end_clean();
        //$store = Excel::store(new dateWiseBuyerExports($from,$to), 'buyerListDateWise.xlsx');
        return Excel::download(new dateWiseBuyerExports($from,$to), 'buyerListDateWise.xlsx',null, [\Maatwebsite\Excel\Excel::XLS]);
    }*/



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
