<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;

class orderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderList = \DB::connection('mysql2')->select('select tph.po_id,tph.buyer_user_id,tph.createdAt,mu.name,tph.total_prd,tph.po_total,tph.child_po_id,tph.exp_del_date,tph.payment_method,tph.commission_amt,tph.created_by,mst_ba.sales_team_name as ba_name from trans_po_hdr as tph left join mst_user as mu on tph.buyer_user_id = mu.user_id left join mst_sales_team as mst_ba on mst_ba.sales_team_id = tph.created_by where seller_user_id = 0 order by po_id desc');
        $orderList = (object)$orderList;
        return view('orders.list',compact('orderList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $parentPoId = (int)$id;
        $orderDetails = array();

        $childOrder = \DB::connection('mysql2')->select('select *,mstST.sales_team_name from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id left join mst_sales_team as mstST on tph.created_by = mstST.sales_team_id where tph.po_id ='.$id);
       $orderDetails["po_id"]=$childOrder[0]->po_id;
       $orderDetails["po_total"]=$childOrder[0]->po_total;
       $orderDetails["cgst_amt"]=$childOrder[0]->cgst_amt;
       $orderDetails["sgst_amt"]=$childOrder[0]->sgst_amt;
       $orderDetails["exp_del_date"]=$childOrder[0]->exp_del_date;
       $orderDetails["total_prd"]=$childOrder[0]->total_prd;
       $orderDetails["parent_po_id"]=$childOrder[0]->parent_po_id;
       $orderDetails["bill_to_addr"]=$childOrder[0]->bill_to_addr;
       $orderDetails["ship_to_addr"]=$childOrder[0]->ship_to_addr;
       $orderDetails["bill_from_addr"]=$childOrder[0]->bill_from_addr;
       $orderDetails["po_exp_dt"]=$childOrder[0]->po_exp_dt;
       $orderDetails["po_accept_exp_dt"]=$childOrder[0]->po_accept_exp_dt;
       $orderDetails["commission_amt"]=$childOrder[0]->commission_amt;
       $orderDetails["payment_method"]=$childOrder[0]->payment_method;
       $orderDetails["created_by"]=$childOrder[0]->sales_team_name;
       $orderDetails["createdAt"]=$childOrder[0]->createdAt;
       $orderDetails["is_active"]=$childOrder[0]->is_active;
       $orderDetails = (object)$orderDetails;
       $orderStatus = \DB::connection('mysql2')->select('select is_active from trans_po_hdr where po_id='.$id);
       $orderStatus = $orderStatus[0]->is_active;
       //dd($orderStatus);
       $parentOrder = \DB::connection('mysql2')->select('select * from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where tph.po_id ='.$parentPoId);
       
       $products = \DB::connection('mysql2')->select('select * from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where tph.po_id ='.$id);

       $sellerDetails = \DB::connection('mysql2')->select('select u.user_id,u.name,u.mobile,u.email,u.gst_no,u.comp_name,s.ss_pincode,rua.Addr_1,rua.Addr_2,rua.area_name,rua.city_name,rua.state_name,rua.pincode from mst_user u left join mst_supplier s on s.created_by = u.user_id left join ref_user_address rua on rua.user_id = u.user_id where u.user_id = '.$parentOrder[0]->seller_user_id);
      
        $buyerDetails = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.gst_no,mu.pan_no,rua.pincode, rua.Addr_1,rua.Addr_2,rua.area_name,rua.city_name,rua.state_name from mst_user mu left join ref_user_address rua on mu.user_id = rua.user_id where mu.user_id ='.$parentOrder[0]->buyer_user_id);

        $buyerDetails = (object)$buyerDetails[0];
        $parentOrder = (object)$parentOrder[0];
        $sellerDetails = (object)$sellerDetails[0];
        //dd($orderStatus);
        if($orderStatus == 3){
            $orderStatusArray = array('cancelOrder'=>'completed','NewOrder'=>'completed','orderAccepted'=>'','ReadyToDispatch'=>'','dispatched'=>'','pendingForPayment'=>'','delivered'=>'');
        }elseif($orderStatus == 1){
            $orderStatusArray = array('cancelOrder'=>'','NewOrder'=>'completed','orderAccepted'=>'','ReadyToDispatch'=>'','dispatched'=>'','pendingForPayment'=>'','delivered'=>'');
        }elseif($orderStatus == 2){
            $orderStatusArray = array('cancelOrder'=>'','NewOrder'=>'completed','orderAccepted'=>'completed','ReadyToDispatch'=>'','dispatched'=>'','pendingForPayment'=>'','delivered'=>'');
        }elseif($orderStatus == 8){
            $orderStatusArray = array('cancelOrder'=>'','NewOrder'=>'completed','orderAccepted'=>'completed','ReadyToDispatch'=>'completed','dispatched'=>'','pendingForPayment'=>'completed','delivered'=>'');
        }
        
        return view('orders.info',compact('orderDetails','sellerDetails','orderStatusArray','buyerDetails','orderStatus','parentOrder','parentPoId','products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
