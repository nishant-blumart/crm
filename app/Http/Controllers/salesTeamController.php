<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class salesTeamController extends Controller
{
   public function index(Request $request){
    	$salesTeamList = \DB::connection('mysql2')->select('select * from mst_sales_team  where role_id = 1 order by sales_team_id asc');

    	$salesTeamList = \DB::connection('mysql2')->select('select mst.sales_team_id,mst.sales_team_name,mst.mobile, count(mst.sales_team_id) as totalBuyers, mst.route_id from mst_sales_team mst left join mst_user mu on mst.sales_team_id=mu.created_by GROUP BY mst.sales_team_id HAVING COUNT(mst.sales_team_id) > 0 order by totalBuyers desc');
        return view('salesTeam.list',compact('salesTeamList'));
    }
}