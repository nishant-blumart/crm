<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\delhdr;
class deliveryController extends Controller
{
    
    public function index(Request $request)
    {
        if ($request->isMethod('get')) {
        
             $orderList = '';
            return view('orders.list',compact('orderList'));
        }
        else
        {
            $deliveryType = $request->post('deliveryType');
            if($deliveryType == 2){
               $delTypeCondition = "1,2,3";
            }elseif($deliveryType == 1){
               $delTypeCondition = "4";
            }

            $orderList = \DB::connection('mysql2')->select('select tph.po_id,tph.buyer_user_id,tph.createdAt,mu.name,tph.total_prd,tph.po_total,tph.child_po_id,tph.exp_del_date,tph.payment_method,tph.commission_amt,tph.created_by from trans_po_hdr as tph left join mst_user as mu on tph.buyer_user_id = mu.user_id left join trans_del_hdr as tdh on tdh.parent_po_id=tph.po_id where tdh.is_active IN ('.$delTypeCondition.') AND tph.seller_user_id=0 order by po_id desc');
            $orderList = (object)$orderList;
            return view('orders.list',compact('orderList'));
        }
        
    }

    public function create()
    {
        //
    }

  
    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
