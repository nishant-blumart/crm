<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\blucartOrders;
use App\Http\Controllers\numberToWordConverterController;

class blucartController extends Controller
{
   
    public $sellerDetails = array('id'=>'1','name'=>'I-zysk Solutions Pv. Ltd.','mobile'=>'9089084242','pincode'=>'400078','gst_no'=>'27AAFCI5469R1ZN','email'=>'izysksolutions@gmail.com','Addr_1'=>'F-204, Eastern Business District,L.B.S. Marg,Bhandup (West)-400078','area_name'=>'Bhandup','city_name'=>'Mumbai','state_name'=>'Maharashtra','pan'=>'AAFCI5469R','cin'=>'U74990MH2020PTC336062');

    public function index()
    {
        $list = blucartOrders::all();
        return view('blucart.orders.list', compact('list'));
    }

    public function invoice(blucartOrders $invoice){
        $products = unserialize($invoice->products);
        $masterHsnArray = array();
        foreach($products as $value) {
            if(array_key_exists($value['hsn'],$masterHsnArray)){
                 $masterHsnArray[$value['hsn']]['total']+= $value['total'];
                $masterHsnArray[$value['hsn']]['qty']+= $value['qty'];
                $masterHsnArray[$value['hsn']]['gross']+=  $value['gross'];
                $masterHsnArray[$value['hsn']]['gst_value']+=  $value['gst_value'];
            }else{
                $masterHsnArray[$value['hsn']] = array('gross'=>0,'gst'=>$value['gst'],'gst_value'=>0,'total'=>0,'qty'=>0);
                $masterHsnArray[$value['hsn']]['total']+= $value['total'];
                $masterHsnArray[$value['hsn']]['qty']+= $value['qty'];
                $masterHsnArray[$value['hsn']]['gross']+=  $value['gross'];
                $masterHsnArray[$value['hsn']]['gst_value']+=  $value['gst_value'];
            }
        }
        $wordConverter = new numberToWordConverterController;
        $inWords =  $wordConverter->convert_number_to_words((string)round($invoice->total,0));
        $from = (object)$this->sellerDetails;
        return view('blucart.orders.invoice',compact('from','invoice','inWords','masterHsnArray'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(blucartOrders $order)
    {
        $sellerDetails = (object)$this->sellerDetails;
        $products = unserialize($order->products);
       return view('blucart.orders.info',compact('order','sellerDetails'));
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
