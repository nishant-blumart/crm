<?php

namespace App\Http\Controllers;
use Excel;
use Storage;
use Session;
use Illuminate\Http\Request;
use App\Exports\deliveryExports;

class deliveryReportsController extends Controller
{
    public function index()
    {
         return view('reports.delivery.index');
    }


    public function dateWiseReport(Request $request){
        date_default_timezone_set("Asia/Calcutta"); 
        $today = date('Y-m-d');
        $from = strtotime($request->data[0]);
        $to = strtotime($request->data[1]);
        $status = $request->data[2]==3 ? "= 3" : "!= 3";
        //dd($status);
        $fromNew = date('Y-m-d',$from);
        $toNew = date('Y-m-d',$to);
        
       if($fromNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $orderList = \DB::connection('mysql2')->select('select tdh.parent_po_id,tdh.invoice_id,tdh.createdAt,tdh.b_del_total,mu.comp_name,tph.created_by,rua.pincode,
            CASE
                WHEN tdh.is_active = 1 THEN "New"
                WHEN tdh.is_active = 2 THEN "Ready to Dispatch"
                WHEN tdh.is_active = 3 THEN "Dispatched"
                WHEN tdh.is_active = 4 THEN "Buyer Approved"
                WHEN tdh.is_active = 8 THEN "Cancelled"
                ELSE "NA"
            END AS del_status
           from trans_del_hdr as tdh left join trans_po_hdr as tph on tdh.parent_po_id = tph.po_id left join mst_user as mu on mu.user_id = tph.buyer_user_id left join ref_user_address as rua on mu.user_id = rua.user_id where (tdh.is_active '.$status.') AND (tdh.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by tdh.createdAt desc');
       }else if($toNew == $today){
        $toNew = date('Y-m-d H:i:s');
        $orderList = \DB::connection('mysql2')->select('select tdh.parent_po_id,tdh.invoice_id,tdh.createdAt,tdh.b_del_total,mu.comp_name,tph.created_by,rua.pincode,
            CASE
                WHEN tdh.is_active = 1 THEN "New"
                WHEN tdh.is_active = 2 THEN "Ready to Dispatch"
                WHEN tdh.is_active = 3 THEN "Dispatched"
                WHEN tdh.is_active = 4 THEN "Buyer Approved"
                WHEN tdh.is_active = 8 THEN "Cancelled"
                ELSE "NA"
            END AS del_status
           from trans_del_hdr as tdh left join trans_po_hdr as tph on tdh.parent_po_id = tph.po_id left join mst_user as mu on mu.user_id = tph.buyer_user_id left join ref_user_address as rua on mu.user_id = rua.user_id where (tdh.is_active '.$status.') AND (tdh.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by tdh.createdAt desc');
       }else if($fromNew == $toNew ){
        $fromNew = date('Y-m-d H:i:s',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
        $orderList = \DB::connection('mysql2')->select('select tdh.parent_po_id,tdh.invoice_id,tdh.createdAt,tdh.b_del_total,mu.comp_name,tph.created_by,rua.pincode,
            CASE
                WHEN tdh.is_active = 1 THEN "New"
                WHEN tdh.is_active = 2 THEN "Ready to Dispatch"
                WHEN tdh.is_active = 3 THEN "Dispatched"
                WHEN tdh.is_active = 4 THEN "Buyer Approved"
                WHEN tdh.is_active = 8 THEN "Cancelled"
                ELSE "NA"
            END AS del_status
           from trans_del_hdr as tdh left join trans_po_hdr as tph on tdh.parent_po_id = tph.po_id left join mst_user as mu on mu.user_id = tph.buyer_user_id left join ref_user_address as rua on mu.user_id = rua.user_id where (tdh.is_active '.$status.') AND (tdh.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by tdh.createdAt desc');
       }else{
        $fromNew = date('Y-m-d 00:00:00',$from);
        $toNew = date('Y-m-d 23:59:59',$to);
        $orderList = \DB::connection('mysql2')->select('select tdh.parent_po_id,tdh.invoice_id,tdh.createdAt,tdh.b_del_total,mu.comp_name,tph.created_by,rua.pincode,
            CASE
                WHEN tdh.is_active = 1 THEN "New"
                WHEN tdh.is_active = 2 THEN "Ready to Dispatch"
                WHEN tdh.is_active = 3 THEN "Dispatched"
                WHEN tdh.is_active = 4 THEN "Buyer Approved"
                WHEN tdh.is_active = 8 THEN "Cancelled"
                ELSE "NA"
            END AS del_status
           from trans_del_hdr as tdh left join trans_po_hdr as tph on tdh.parent_po_id = tph.po_id left join mst_user as mu on mu.user_id = tph.buyer_user_id left join ref_user_address as rua on mu.user_id = rua.user_id where (tdh.is_active '.$status.') AND (tdh.createdAt BETWEEN "'.$fromNew.'" AND "'.$toNew.'") order by tdh.createdAt desc');
       }
        $store = Excel::store(new deliveryExports($orderList), 'deliveryListDateWise.xlsx');
        return $orderList;
    }

    public function download(){
        $filename = '/deliveryListDateWise.xlsx';
        return response()->download(storage_path("app".$filename));
    }



}
