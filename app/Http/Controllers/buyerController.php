<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Concerns\FromCollection;
use Excel;
use DataTables;
use Redirect;
use App\Exports\buyerExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\subscriptions;
use App\Models\sms;
use Session;

class buyerController extends Controller
{
    public function index(Request $request){
    	//$buyerList = \DB::connection('mysql2')->select('select user_id,name,comp_name,mobile,email,is_active,created_by from mst_user where profile="buyer" order by user_id asc');

		$buyerList = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.is_active,mu.created_by,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id where mu.profile = "buyer" and mu.is_active = 1 order by mu.user_id asc');
        return view('buyer.list',compact('buyerList'));
    }

    public function export() 
    {	
        return Excel::download(new buyerExport, 'buyerList.xlsx');
    }
    public function new(Request $request){
        $zones = \DB::connection('mysql2')->select('select * from mst_zone where is_deleted = 0');
        //$region = \DB::connection('mysql2')->select('select * from mst_zone where is_deleted = 0');
        //$area = \DB::connection('mysql2')->select('select * from mst_zone where is_deleted = 0');
        
        return view('buyer.new',compact('zones'));
    }

    public function getDetailsById(Request $request){
        
    	$buyerId = $request->id;
    	$buyerInfo= \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.altr_mob_no,mu.whatsapp_mob_no,mu.gst_no,mu.pan_no,mu.is_active,mu.created_by,mu.aadhar_no,mst.sales_team_name,mst.mobile as sales_mob,rua.pincode, rua.Addr_1,rua.Addr_2,rua.Addr_1,rua.area_name,rua.city_name,rua.state_name,ma.area_name, mr.region_name, mz.zone_name from mst_user mu left join mst_sales_team mst on mu.created_by = mst.sales_team_id left join ref_user_address rua on mu.user_id = rua.user_id left join mst_area ma on mu.area_id = ma.id left join mst_region mr on mu.region_id = mr.id left join mst_zone mz on mu.zone_id = mz.id where mu.profile = "buyer" and mu.user_id = '.$buyerId);
        $buyerDetails = $buyerInfo[0];

        $buyerOrders = \DB::connection('mysql2')->select('select tph.po_id,tph.createdAt,tph.total_prd,tph.po_total,tph.cgst_amt,tph.sgst_amt,tph.payment_method,tph.created_by from trans_po_hdr as tph left join  trans_po_dtl as tpd on tph.po_id= tpd.po_id where buyer_user_id ='.$buyerId);
        
        $isSubscribed = subscriptions::where('user_id',$buyerId)->pluck('status');
        if($isSubscribed->isEmpty()){
            $subscription = 'Inactive';
        }else{
            switch($isSubscribed[0]) {
        
        case 'created': $subscription = 'Active';
        break;
        case 'cancelled': $subscription = 'Cancelled';
            break;
        case 'paused': $subscription = 'Paused';
        break;
        default: $subscription = 'NA';
        }
    }
/*
        dd($isSubscribed[0]);
        if($isSubscribed->isEmpty()){
            $subscription = 0;
        }else{
            $subscription = 1;
        }*/
        $buyerSubscriptionId = subscriptions::GetSubscriptionIdByUserId($buyerId);
        //dd($buyerSubscriptionId);

        if($buyerSubscriptionId->isNotEmpty()){
            $buyerSubscriptionId = $buyerSubscriptionId[0]['subscr_id'];
            $endpoint = "https://api.razorpay.com/v1/invoices";
            $client = new \GuzzleHttp\Client(['auth' => [env('RAZOR_KEY'),env('RAZOR_SECRET')]]);
            $response = $client->request('GET', $endpoint, ['query' => [
                'subscription_id' => $buyerSubscriptionId
            ]]);
            $statusCode = $response->getStatusCode();
            $content =  json_decode($response->getBody()->getContents(), true);
            $buyerSubscriptionInvoices = $content['items'];
            $smsId = subscriptions::GetSmsDetails($buyerId,$buyerSubscriptionId);
            return view('buyer.info',compact('buyerDetails','buyerId','subscription','smsId','buyerSubscriptionInvoices','buyerSubscriptionId')); 

        }else{
            $buyerSubscriptionInvoices = array();
            $smsId = '';
            return view('buyer.info',compact('buyerDetails','buyerId','subscription','smsId','buyerSubscriptionInvoices','buyerSubscriptionId','buyerOrders')); 
        }
    	/*$sellerProducts = \DB::connection('mysql2')->select('select c.prod_id,c.hsn_no,c.product_name,c.sku_size,c.gst,c.sgst,c.case_size,mvp.vend_prod_id,mvp.mrp,mvp.inv_qty,mvp.ord_qnt_rtl_a,mvp.ord_qnt_rtl_b,mvp.ord_qnt_rtl_c,mvp.ord_qnt_rtl_d,mvp.ret_minord_qnty_cost_a,mvp.ret_minord_qnty_cost_b,mvp.ret_minord_qnty_cost_c,mvp.ret_minord_qnty_cost_d,mvp.approved_pin from mst_catalog c left join mst_vend_prod mvp on c.prod_id = mvp.prod_id where mvp.user_id =  '.$buyerId);
    	$buyerDocuments = \DB::connection('mysql2')->select('select * from ref_user_doc where user_id = '.$buyerId);
    	$buyerPoDetails = \DB::connection('mysql2')->select('select* from trans_po_hdr where seller_user_id = '.$buyerId);
    	$buyerDetails = $sellerDetails[0];*/
        
    }

    public function edit(Request $request){
        $buyerId = $request->id;
        $buyerInfo= \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.altr_mob_no,mu.whatsapp_mob_no,mu.gst_no,mu.pan_no,mu.is_active,mu.aadhar_no,mu.zone_id,mu.region_id,mu.area_id,mu.route_id,rua.pincode, rua.Addr_1,rua.Addr_2 from mst_user mu left join ref_user_address rua on mu.user_id = rua.user_id where mu.profile = "buyer" and mu.user_id = '.$buyerId);
        $buyerDetails = $buyerInfo[0];
        $zones = \DB::connection('mysql2')->select('select id as zone_id,zone_name from mst_zone where is_deleted="0"');
        $regions = \DB::connection('mysql2')->select('select id as region_id,zone_id,region_name from mst_region');
        $area = \DB::connection('mysql2')->select('select id as area_id,region_id,zone_id,area_name from mst_area');
        $route = \DB::connection('mysql2')->select('select id as route_id,area_id,region_id,zone_id,route_name from mst_route');
        return view('buyer.edit',compact('buyerDetails','zones','regions','area','route'));
    }

    public function update(Request $request){
        //$records = \DB::connection('mysql2')->table('mst_user')->orderBy('user_id', 'desc')->take(5)->get();
        //dd($request->all());
        $data = $request->validate([
            'buyer_id'=>'required|numeric',
            'buyer_name'=>'required|min:5|regex:/^[\pL\s\-]+$/u',
            'company_name'=>'required|min:5|regex:/^[ A-Za-z0-9_@.,#& ]*$/',
            'buyer_mobile'=>'required|digits:10',
            'buyer_email'=>'nullable|email',
            'buyer_whatsapp'=>'nullable|digits:10',
            'altr_mob_no'=>'nullable|digits:10',
            'buyer_address1'=>'required',
            'buyer_address2'=>'nullable',
            'buyer_pincode'=>'required|digits:6',
            'pan_no'=>'required|alpha_num|size:10',
            'gst_no'=>'nullable|alpha_num|size:15',
            'aadhar_no'=>'nullable|numeric',
            'zone_id'=>'required|numeric',
            'region_id'=>'required|numeric',
            'area_id'=>'required|numeric',
            'route_id'=>'required|numeric',
            'updated_by'=>'',
        ]);

       $updateBuyer =  \DB::connection('mysql2')->table('mst_user')->where('user_id', $request->buyer_id)->update([
        'name' =>$request->buyer_name,
        'comp_name' =>$request->company_name,
        'mobile' =>$request->buyer_mobile,
        'email' =>$request->buyer_email,
        'whatsapp_mob_no' =>$request->buyer_whatsapp,
        'altr_mob_no' =>$request->altr_mob_no,
        'pan_no' =>$request->pan_no,
        'gst_no' =>$request->gst_no,
        'aadhar_no' =>$request->aadhar_no,
        'zone_id' =>$request->zone_id,
        'region_id' =>$request->region_id,
        'area_id' =>$request->area_id,
        'route_id' =>$request->route_id,
        'updated_by' =>$request->buyer_id,
        'updatedAt' =>date('Y-m-d H:i:s'),
    ]);
        $updateAddress =  \DB::connection('mysql2')->table('ref_user_address')->where('user_id', $request->buyer_id)->update([
        'Addr_1' =>$request->buyer_address1,
        'Addr_2' =>$request->buyer_address2,
        'pincode' =>$request->buyer_pincode,
        ]);

       if($updateBuyer ==1 || $updateAddress ==1){ 
            $request->session()->flash('alert-success', 'Buyer Details Updated Successfully !');
            return redirect::back()->with('message','Buyer Details Updated Successfully !'); 

       }else{
            $request->session()->flash('alert-danger', 'Something Went Wrong !');
            return redirect::back()->with('message','Something Went Wrong !'); 
       }
       

    }
}
