<?php

namespace App\Http\Controllers;

use App\Models\cart;
use App\Models\mstuser;
use Illuminate\Http\Request;

class CartController extends Controller
{
    
    public function index()
    {
        return view('otp.cart.index');
    }

    public function show(Request $request)
    {
        $id = mstuser::where('mobile', $request->mobile)->get('user_id');
        if($id->isEmpty()) {
            $data = 'Mobile Number Not Found';
        }else{
            $record = cart::where('user_id', $id[0]->user_id)->orderBy('createdAt','desc')->first('otp');
            $data =  $record->otp;
        }
        return $data;
    }

}
