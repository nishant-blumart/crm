<?php

namespace App\Http\Controllers;

use Storage;
use Aws\S3\S3Client;
use Illuminate\Http\Request;
use League\Flysystem\Filesystem;
use League\Flysystem\AwsS3v3\AwsS3Adapter;

class searchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /*  https://www.webslesson.info/2018/04/live-search-in-laravel-using-ajax.html  */
    function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = \DB::connection('mysql2')->table('mst_user')
         ->where('name', 'like', '%'.$query.'%')
         ->orWhere('mobile', 'like', '%'.$query.'%')
         ->orWhere('comp_name', 'like', '%'.$query.'%')
         ->orWhere('gst_no', 'like', '%'.$query.'%')
         ->orderBy('user_id', 'desc')
         ->get();
      }
      else
      {
       $data =\DB::connection('mysql2')->table('mst_user')
         ->orderBy('user_id', 'desc')
         ->get();
      }
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        $output .= '
        <tr id="'.$row->user_id.'">
        <td>'.$row->name.'</td>
        <td>'.$row->comp_name.'</td>
        <td>'.$row->mobile.'</td>
        <td><button type="button" data-id="'.$row->user_id.'" data-mobile="'.$row->mobile.'" id="SubscriptionBtn" class="btn btn-success">Pay</button></td>
        </tr>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

      echo json_encode($data);
     }
    }

function productSearch(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = \DB::connection('mysql2')->table('mst_catalog')
       ->select('mst_catalog.product_name','mst_catalog.gst','mst_catalog.sgst','mst_catalog.hsn_no','mst_catalog.uom','mst_catalog_media.media','mst_catalog.prod_id','mst_vend_prod.mrp','mst_vend_prod.ret_minord_qnty_cost_a','mst_vend_prod.vend_prod_id','mst_cat.cat_name')
       ->join("mst_vend_prod","mst_vend_prod.prod_id","=","mst_catalog.prod_id")
       ->join("mst_catalog_media","mst_catalog_media.prod_id","=","mst_catalog.prod_id")
       ->join("mst_cat","mst_cat.cat_id","=","mst_catalog.cat_id")
        ->where('product_name', 'like', '%'.$query.'%')
        ->where('mst_vend_prod.user_id', '=', '179')
        ->where('mst_vend_prod.is_active', '=', '1')
         ->get();
      }
      else
      {
       $data =\DB::connection('mysql2')->table('mst_catalog')
         ->get();
      }

      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        $imgpath = Storage::disk("s3")->url($row->media);
        $output .= '
        <div class="card mb-3" id="'.$row->prod_id.'">
          <div class="card-header text-center" style="background-color:#fbb848;font-size:1.5rem;" data-name="'.$row->product_name.'" id="prodName_'.$row->vend_prod_id.'" ><b>'.$row->cat_name.'
          </b></div>
          <div class="card-body">
            <div class="row" style="font-size:1.5rem;">
              <div class="col-4" style="border-radius:1px solid black;border-right:1px solid black">
                  <img class="prodImg" id="img_'.$row->vend_prod_id.'" src="'.$imgpath.'" style="width: 100px;height: 120px;">
              </div>
              <div class="col-8">
                <div class="row">
                  <div class="col-12 pb-1">
                          <div class="row mb-2">
                            <div class="col-12">
                              <p><b>'.$row->product_name.'</b></p>
                            </div>
                            <div class="col-12">
                              <p>MRP : '.$row->mrp.' Rs.</p>
                            </div>
                           
                             <input type="hidden" id="'.$row->gst.'" class="cgst_'.$row->vend_prod_id.'" >
                             <input type="hidden" id="'.$row->hsn_no.'" class="hsn_'.$row->vend_prod_id.'" >
                             <input type="hidden" id="'.$row->uom.'" class="uom_'.$row->vend_prod_id.'" >
                            
                          <div class="col-12">
                            <p class="rate_'.$row->vend_prod_id.'" id="'.$row->ret_minord_qnty_cost_a.'"><b>Selling Price : '.$row->ret_minord_qnty_cost_a.' Rs.</b></p>
                          </div>
                          <div class="col-12">
                            <label class="qnty" style="font-size:1.5rem;"><b>Qty : </b></label>
                            <input type="number" name="qty"  data-id="'.$row->vend_prod_id.'" class="qtyMain" id="qty_'.$row->vend_prod_id.'" style="width:60%;">
                          </div>

                        </div>
                  </div>
                </div>
              </div>
             </div> 
            
            </div>

            <hr>

            <div class="row mb-4 ml-1">
              <div class="col-6">
                <button class="btn btn-success addToCartBtn" id="'.$row->vend_prod_id.'"><i class="fa fa-cart-arrow-down"></i> Add to Cart</button>
              </div>
              <div class="col-6">
                 <p><b style="font-size:1.5rem;" id="totalAmount_'.$row->vend_prod_id.'"></b></p>
              </div>
            </div>

          </div>
          </div>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $data = array(
       'product_data'  => $output
      );
      //dd($output);
      echo json_encode($data);
     }
    }


    function catagorySearch(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('catagory');
      if($query != '')
      {
       $data = \DB::connection('mysql2')->table('mst_catalog')
       ->select('mst_catalog.product_name','mst_catalog.gst','mst_catalog.sgst','mst_catalog_media.media','mst_catalog.hsn_no','mst_catalog.uom','mst_catalog.prod_id','mst_vend_prod.mrp','mst_vend_prod.ret_minord_qnty_cost_a','mst_vend_prod.vend_prod_id','mst_cat.cat_name')
       ->join("mst_vend_prod","mst_vend_prod.prod_id","=","mst_catalog.prod_id")
       ->join("mst_catalog_media","mst_catalog_media.prod_id","=","mst_catalog.prod_id")
       ->join("mst_cat","mst_cat.cat_id","=","mst_catalog.cat_id")
        ->where('mst_vend_prod.user_id', '=', '2346')
        ->where('mst_catalog.cat_id', '=', $query )
        ->where('mst_vend_prod.is_active', '=', '1')
         ->get();
      }
      else
      {
       $data =\DB::connection('mysql2')->table('mst_catalog')
         ->get();
      }
      //dd($data);
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        $imgpath = Storage::disk("s3")->url($row->media);
        $output .= '
        <div class="card mb-3" id="'.$row->prod_id.'">
          <div class="card-header text-center" style="background-color:#fbb848;font-size:1.5rem;" data-name="'.$row->product_name.'" id="prodName_'.$row->vend_prod_id.'" ><b>'.$row->cat_name.'
          </b></div>
          <div class="card-body">
            <div class="row" style="font-size:1.5rem;">
              <div class="col-4" style="border-radius:1px solid black;border-right:1px solid black">
                  <img class="prodImg" id="img_'.$row->vend_prod_id.'" src="'.$imgpath.'" style="width: 100px;height: 120px;">
              </div>
              <div class="col-8">
                <div class="row">
                  <div class="col-12 pb-1">
                          <div class="row mb-2">
                            <div class="col-12">
                              <p><b>'.$row->product_name.'</b></p>
                            </div>
                            <div class="col-12">
                              <p>MRP : '.$row->mrp.' Rs.</p>
                            </div>
                           
                             <input type="hidden" id="'.$row->gst.'" class="cgst_'.$row->vend_prod_id.'" >
                             <input type="hidden" id="'.$row->hsn_no.'" class="hsn_'.$row->vend_prod_id.'" >
                             <input type="hidden" id="'.$row->uom.'" class="uom_'.$row->vend_prod_id.'" >
                            
                          <div class="col-12">
                            <p class="rate_'.$row->vend_prod_id.'" id="'.$row->ret_minord_qnty_cost_a.'"><b>Selling Price : '.$row->ret_minord_qnty_cost_a.' Rs.</b></p>
                          </div>
                          <div class="col-12">
                            <label class="qnty" style="font-size:1.5rem;"><b>Qty : </b></label>
                            <input type="number" name="qty"  data-id="'.$row->vend_prod_id.'" class="qtyMain" id="qty_'.$row->vend_prod_id.'" style="width:60%;">
                          </div>

                        </div>
                  </div>
                </div>
              </div>
             </div> 
            
            </div>

            <hr>

            <div class="row mb-4 ml-1">
              <div class="col-6">
                <button class="btn btn-success addToCartBtn" id="'.$row->vend_prod_id.'"><i class="fa fa-cart-arrow-down"></i> Add to Cart</button>
              </div>
              <div class="col-6">
                 <p><b style="font-size:1.5rem;" id="totalAmount_'.$row->vend_prod_id.'"></b></p>
              </div>
            </div>

          </div>
          </div>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $data = array(
       'product_data'  => $output
      );
      echo json_encode($data);
     }
    }

    public function offerProductSearch(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $data = \DB::connection('mysql2')->table('mst_catalog')
       ->select('mst_catalog.product_name','mst_catalog.gst','mst_catalog.sgst','mst_catalog.hsn_no','mst_catalog.uom','mst_catalog_media.media','mst_catalog.prod_id','mst_vend_prod.mrp','mst_vend_prod.ret_minord_qnty_cost_a','mst_vend_prod.vend_prod_id','mst_cat.cat_name')
       ->join("mst_vend_prod","mst_vend_prod.prod_id","=","mst_catalog.prod_id")
       ->join("mst_catalog_media","mst_catalog_media.prod_id","=","mst_catalog.prod_id")
       ->join("mst_cat","mst_cat.cat_id","=","mst_catalog.cat_id")
        ->where('product_name', 'like', '%'.$query.'%')
        ->where('mst_vend_prod.user_id', '=', '179')
        ->where('mst_vend_prod.is_active', '=', '1')
         ->get();
      }
      else
      {
       $data =\DB::connection('mysql2')->table('mst_catalog')
         ->get();
      }

      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        $imgpath = Storage::disk("s3")->url($row->media);
        $output .= '
        
        <div class="card" id="'.$row->prod_id.' style="width: 18rem;">
            <div class="card-body">
                <div class="row">
                  <div class="col-3">
                    <img class="prodImg" id="img_'.$row->prod_id.'" src="'.$imgpath.'" style="width: 80px;height: 80px;">
                  </div>
                  <div class="col-9">
                    <div class="row">
                        <div class="col-12">
                          <h5 class="card-title"> <b>'.$row->product_name.'</b></h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4 col-xs-2">
                            <label class="qnty" >Mrp : </label>
                            <input type="number" name="mrp" class="mrp_'.$row->prod_id.'" style="width:70px;">
                        </div>
                        <div class="col-4 col-xs-2">
                            <label class="qnty" >Price : </label>
                            <input type="number" name="price" class="price_'.$row->prod_id.'" style="width:70px;">
                        </div>
                         <div class="col-4 col-xs-2 mt-4">
                            <input type="button" id="add_'.$row->prod_id.'" data-id="'.$row->prod_id.'" class="btn btn-primary addProductBtn" value="Add">
                        </div>
                    </div>     
                  </div>
                </div>
            </div>
        </div>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $data = array('product_data'  => $output);
      echo json_encode($data);
     }
  }
}
