<?php


namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Excel;
use DB;
use Session;
use Storage;
use Redirect;
use Response;
use Carbon\Carbon;
use App\Exports\catalogRecentProductsDownload;
use App\Exports\vendorRecentProductsDownload;
use App\Models\subcat;
use App\Models\childcat;
use App\Models\seller;
use App\Models\deliveryDetails;
use App\Models\vendorProducts;
use App\Imports\productsImport;
use App\Imports\catalogImport;
use App\Models\products;
use App\Models\catagorySettings;
use App\Models\brands;


class productController extends Controller
{
    protected $connection= 'mysql2';
    protected $table = 'mst_catalog';
    
    public function index()
    {
        return view('products.search');
    }

    public function uploadView(){
        $products =array();
        return view('products.uploadView')->with(['products' => $products]); 
    }

    public function catalogUploadView(){
        $products =array();
        return view('products.catalogUploadView')->with(['products' => $products]);
    }

    public function upload(Request $request){

        if($request->hasFile('productsCsvFile')){
            //$rows = Excel::toArray(new productsImport, $request->file('productsCsvFile'));
            //dd($rows);
            /*foreach($rows as $eachRow=>$eachData){
                    dd($eachData);
            }*/
        }
    }

    public function import(Request $request){
        $products =Excel::toArray(new productsImport(), request()->file('productsCsvFile'));
        $errors = productsImport::validate($products[0]);
        //dd($errors);
        foreach($errors as $key => $eachError){
            if($eachError == false){
               unset($errors[$key]);
            }
        }
        if(empty($errors)){
            $productIdList = productsImport::store($products[0]);
            //dd($productIdList);
            if(Session::has('vendor_products')){
                Session::forget('vendor_products');
            }    
                Session::put('vendor_products', $productIdList);
                $products = vendorProducts::getRecentInsertedVendorProducts($productIdList);
                session::flash('success','All the Products Uploaded Successfully. Please check and verify the same');
            return view('products.uploadView')->with(['products' => $products]);
        }else{
            session::flash('errors',$errors);
            return Redirect::back();
        }
        
    }

    public function CatalogImport(Request $request){
        $catalog =Excel::toArray(new catalogImport(), request()->file('productsCsvFile'));
        //dd($catalog);
        $errors = catalogImport::validate($catalog[0]);

        foreach($errors as $key => $eachError){
            if($eachError == false){
               unset($errors[$key]);
            }
        }
        if(empty($errors)){
            $productIdList = catalogImport::store($catalog[0]);
            if(Session::has('products')){
                Session::forget('products');
            }    
                Session::put('products', $productIdList);
            
            $products = products::getRecentInsertedProducts($productIdList);
            session::flash('success','All the Products Uploaded Successfully. Please check and verify the same');
            return view('products.catalogUploadView')->with(['products' => $products]);
        }else{
            session::flash('errors',$errors);
            return Redirect::back();
        }
        
    }

    public function downloadProductCsv(){
        $file = Storage::path('products_upload.xlsx');
        return Response::download($file);
    }

    public function downloadCatalogCsv(){
        $file = Storage::path('catalog_products_upload.xlsx');
        return Response::download($file);
    }

    public function downloadRecentCatalogCsv(){
        return Excel::download(new catalogRecentProductsDownload, 'recent_products.xlsx');
    }

    public function downloadRecentVendorProductsCsv(){
        return Excel::download(new vendorRecentProductsDownload, 'recent_vendor_products.xlsx');
    }

    public function list(){
        $catagories = catagorySettings::list();
        $brands = brands::list();
        $products = array();
        return view('products.list',compact('catagories','brands','products'));
    }

    public function vendorlist(){
        $sellerList = seller::list();
        return view('products.vendor.list',compact('sellerList'));
    }

    public function vendorfilterProducts(Request $request){
        if($request->product_Search == null && $request->status == '10'){
            //search all products
            $products = vendorProducts::getvendorproducts($request->seller);
            return $this->productCardGeneratorVendor($products);
        }elseif($request->product_Search == null && $request->status != '10'){
            $products = vendorProducts::getvendorproductsByStatus($request->seller,$request->status);
            return $this->productCardGeneratorVendor($products);
        }else{
            $products = vendorProducts::getvendorproductsByName($request->seller,$request->product_Search);
            return $this->productCardGeneratorVendor($products);
        }
    }

    public function filterProducts(Request $request){
        if($request->product_Search == null && $request->brand == '0'){
            if($request->child_catagory == '0' && $request->sub_catagory == '0'){
                $products = products::catlist($request->catagory);
                return $this->productCardGenerator($products);
            }
            if($request->child_catagory == '0' && $request->sub_catagory != '0'){
                 $products = products::subcatlist($request->sub_catagory);
                return $this->productCardGenerator($products);
            }
            if($request->child_catagory != '0' && $request->sub_catagory != '0'){
                 $products = products::childcatlist($request->child_catagory);
                return $this->productCardGenerator($products);
            }
        }else if($request->product_Search != null && $request->brand == '0'){
                $products = products::productbyname($request->product_Search);
                return $this->productCardGenerator($products);
        }
    }

    public function catalogProductEdit(products $product){
        //$catagories = catagorySettings::list();
        //$subcat = subcat::list();
        //$childcat = childcat::list();
        $vendors = vendorProducts::Productvendors($product->prod_id);
        $buyers = deliveryDetails::Productbuyers($product->prod_id);

        return view('products.edit',compact('product','vendors','buyers'));
    }

    public function catalogProductUpdate(Request $request){
        $status = products::where('prod_id', $request->prod_id)->update(array('hsn_no' => $request->hsn,'updated_at'=>date('Y-m-d H:i:s')));
        $msg = ($status == 1) ? 'Product Updated Successfully !' : 'Something Went Wrong !';
        $msgType = ($status == 1) ? 'alert-success' : 'alert-danger';
        $request->session()->flash($msgType, $msg);
        return redirect::back()->with('message','Product Updated Successfully !');
    }

    public function productCardGenerator($products){
       
        $output = '';
        foreach($products as $row){
            $imgpath = Storage::disk("s3")->url($row->media);
            $output .= '
            <div class="col-md-6">     
            <div class="card">
              <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-fluid img-responsive rounded product-image" style="width:150px;height:150px;" src="'.$imgpath.'"><br>
                        <div class="text-center mt-1">ID : '.$row->prod_id.'</div>
                    </div>
                    <div class="col-md-8">
                        <h6>'.$row->product_name.'</h6>
                        <div class="row">
                            <div class="col-6">
                                <span>SKU : '.$row->sku_size.'</span>
                            </div>
                            <div class="col-6">
                                <span>UOM : '.$row->uom.'</span>
                            </div>
                            <div class="col-6">
                                <span>CGST : '.$row->gst.' %</span>
                            </div>
                            <div class="col-6">
                                <span>SGST : '.$row->gst.' %</span>
                            </div>
                        </div>
                        <hr>
                        <h6 class="text-center">Case Details</h6>
                        <div class="row">    
                            <div class="col-4">
                                <span>SIZE : '.$row->case_size.'</span>
                            </div>
                            <div class="col-4">
                                <span>UOM : '.$row->case_uom.'</span>
                            </div>
                            <div class="col-4">
                                <span>WEIGHT : '.$row->case_weight.'</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">    
                    <div class="col-3">
                        <span>HSN : '.$row->hsn_no.'</span>
                    </div>
                    <div class="col-5">
                         <span>Updated At : '.date('d-m-Y', strtotime($row->updated_at)).'</span>
                    </div>
                    <div class="col-4">
                        <a href="#" class="btn btn-outline-dark">View</a>
                        <a href="/catalog/product/edit/'.$row->prod_id.'" class="btn btn-outline-dark">Edit</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

            ';
        }
        return $output;
    }


    public function productCardGeneratorVendor($products){
       //dd($products[10]);
        $output = '';
        foreach($products as $row){

            $imgpath = Storage::disk("s3")->url($row->media);
            $output .= '
            <div class="col-md-6">     
            <div class="card">
              <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-fluid img-responsive rounded product-image" style="width:150px;height:150px;" src="'.$imgpath.'"><br>
                        <div class="text-center mt-1">ID : '.$row->prod_id.'</div>
                    </div>
                    <div class="col-md-8">
                        <span><b>'.$row->product_name.'</b></span>
                        <hr>
                         <span><b>STATUS : '.$row->is_active.'</b></span>&nbsp;
                         <a href="#" class="btn btn-outline-danger">Edit</a>
                         <a href="#" class="btn btn-outline-success">View</a>
                         <hr>
                        <div class="row">
                            <div class="col-6">
                                <span>SKU : '.$row->sku_size.'</span>
                            </div>
                            <div class="col-6">
                                <span>UOM : '.$row->uom.'</span>
                            </div>
                            <div class="col-6">
                                <span>CGST : '.$row->gst.' %</span>
                            </div>
                            <div class="col-6">
                                <span>SGST : '.$row->gst.' %</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <h6 class="text-center">Case Details</h6>
                        <div class="row">    
                            <div class="col-4">
                                <span>SIZE : '.$row->case_size.'</span>
                            </div>
                            <div class="col-4">
                                <span>UOM : '.$row->case_uom.'</span>
                            </div>
                            <div class="col-4">
                                <span>WEIGHT : '.$row->case_weight.'</span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">    
                            <div class="col-3">
                                <span>HSN : '.$row->hsn_no.'</span>
                            </div>
                            <div class="col-5">
                                <span>Updated At : '.date('d-m-Y  H:i', strtotime('+5 hour +30 minutes',strtotime($row->updatedAt))).'</span>
                            </div>
                            <div class="col-4">
                                 <span>MOQ : '.$row->ret_minord_qnty.'</span>
                            </div>
                            
                        </div>

                        <div class="row mt-2">
                            <div class="col-12">
                                <table class="table table-bordered">
                                  <thead>
                                    <tr>
                                      <th scope="col">#</th>
                                      <th scope="col">QUANTITY</th>
                                      <th scope="col">PRICE (Rs.)</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <th scope="row">LEVEL 1</th>
                                      <td>'.$row->ord_qnt_rtl_a.'</td>
                                      <td>'.$row->ret_minord_qnty_cost_a.'</td>
                                    </tr>
                                    <tr>
                                      <th scope="row">LEVEL 2</th>
                                      <td>'.$row->ord_qnt_rtl_b.'</td>
                                      <td>'.$row->ret_minord_qnty_cost_b.'</td>
                                    </tr>
                                    <tr>
                                      <th scope="row">LEVEL 3</th>
                                      <td>'.$row->ord_qnt_rtl_c.'</td>
                                      <td>'.$row->ret_minord_qnty_cost_c.'</td>
                                    </tr>
                                     <tr>
                                      <th scope="row">LEVEL 4</th>
                                      <td>'.$row->ord_qnt_rtl_d.'</td>
                                      <td>'.$row->ret_minord_qnty_cost_d.'</td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    <div class="row mt-2">
                        <div class="col-12">
                            <span>Approved Pincodes :<br> '.$row->approved_pin.'</span>
                        </div>
                    </div>
            </div>
        </div>
    </div>

            ';
        }
        return $output;
    }


    public function vendorPincodeUpload(){
        $sellerList = seller::list();
        return view('products.vendor.pincode',compact('sellerList'));
    }

    public function vendorPincodeUploadSave(Request $request){
        $pincodes = $request->pincodes;
        $sellerList = seller::list();
        $status = vendorProducts::whereIn('user_id', [$request->seller])->update(['approved_pin' => $pincodes]);
        $msg = ($status != 0) ? 'Vendor Products Pincodes Updated Successfully !' : 'Something Went Wrong !';
        $msgType = ($status != 0) ? 'alert-success' : 'alert-danger';
        $request->session()->flash($msgType, $msg);
        return redirect()->route('vendorPincodeUpload', ['sellerList' => $sellerList]);

        //return view('products.vendor.pincode',compact('sellerList'));
    }

    public function activeProductsList(){
        $products = vendorProducts::vendorActiveProducts();
        //$products = $productList->groupBy('vend_prod_id');
        //dd($products);
        return view('products.activelist',compact('products'));
    }
    
}
