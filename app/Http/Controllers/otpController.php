<?php

namespace App\Http\Controllers;
use App\Models\mstuser;

use Illuminate\Http\Request;

class otpController extends Controller
{
    public function index()
    {
        return view('otp.onboarding.index');
    }

    public function show(Request $request)
    {
        $otp = mstuser::where('mobile', $request->mobile)->get('otp');
        if($otp->isEmpty()) {
            $data = 'OTP not found';
        }else{
        	return $otp[0]->otp;
        }
    }
}
