<?php

namespace App\Http\Controllers;

use App\Models\invoice;
use Illuminate\Http\Request;
use App\Models\subscriptions;
use App\Models\sms;
use Auth;
use Redirect;
use App\Models\delhdr;
use GuzzleHttp\Client;
use Razorpay\Api\Api;
use Carbon\Carbon;
use App\Http\Controllers\numberToWordConverterController;


class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request,$invoice_id,$userId)
    {
        $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
        $invoice = $api->invoice->fetch($invoice_id);
        $name = 'I-ZYSK Solutions Pvt. Ltd';
        $address='F-204, Eastern Business District,L.B.S. Marg, Bhandup (West) -400078';
        $cin = 'U74990MH2020PTC336062';
        $email = 'izysksolutions@gmail.com';
        $gst = '27AAFCI5469R1ZN';
        $mobile = '9089084242';
        $from = array('name'=>$name,'address'=>$address,'cin'=>$cin,'email'=>$email,'gst'=>$gst,'mobile'=>$mobile);
        $to = \DB::connection('mysql2')->select('select * from mst_user u left join mst_supplier s on s.created_by = u.user_id left join ref_user_address rua on rua.user_id = u.user_id left join ref_user_bank rub on rub.user_id = u.user_id where u.user_id = '.$userId);
            $to = $to[0];
        if(invoice::where('invoice_id', $invoice->id )->exists()){
            $invoice_no = invoice::where('invoice_id',$invoice->id)->pluck('id')[0];
            $userType = subscriptions::where('user_id',$userId)->first('user_type')['user_type'];
            if($userType == 1){
                $subTotal = 169.49;
                $gst = 15.25;
                $total = 200;
                $amountInWords = 'Two Hundred Rupees Only';
            }else{
                $subTotal = 42.37;
                $gst = 3.81;
                $total = 50;
                $amountInWords = 'Fifty Rupees Only';
            }
            return view('subscription.invoice',compact('to','from','invoice','invoice_no','subTotal','gst','total','amountInWords'));
        }else{
                $userType = subscriptions::where('user_id',$userId)->first('user_type')['user_type'];
                $record = new invoice();
                $record->invoice_id = $invoice->id;
                $record->user_id = $userId;
                $record->payment_id = $invoice->payment_id;
                $record->order_id = $invoice->order_id;
                $record->customer_id = $invoice->customer_id;
                $record->status = $invoice->status;
                $record->subscription_id = $invoice->subscription_id;
                $record->invoice_amt = $invoice->amount_paid;
                $record->user_type = $userType;
                $record->invoice_date = $invoice->date;
                $record->invoice_notes = '';
                $record->save();
                $invoice_no=$record->id;
                if($userType == 1){
                    $subTotal = 169.49;
                    $gst = 15.25;
                    $total = 200;
                    $amountInWords = 'Two Hundred Rupees Only';
                }else{
                    $subTotal = 42.37;
                    $gst = 3.81;
                    $total = 50;
                    $amountInWords = 'Fifty Rupees Only';
                }
                return view('subscription.invoice',compact('to','from','invoice','invoice_no','subTotal','gst','total','amountInWords'));
        }
    }

    public function buyerInvoice($invoiceId,$supplierStatus){
        $sellers = array();
        $parentPoId = (int)$invoiceId;
        $from = array('name'=>'I-ZYSK Solutions Pvt. Ltd','address'=>'F-204, Eastern Business District,L.B.S. Marg, Bhandup (West) -400078','cin'=>'U74990MH2020PTC336062','email'=>'izysksolutions@gmail.com','gst'=>'27AAFCI5469R1ZN','pan'=>'AAFCI5469R','mobile'=>'9089084242');
        $from = (object)$from;
        $isCancelled = \DB::connection('mysql2')->select('select is_active,seller_user_id from trans_po_hdr where po_id ='.$invoiceId);
        $parentOrder = \DB::connection('mysql2')->select('select * from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where tph.po_id ='.$parentPoId);
        $buyerOrder =  $parentOrder;
        //dd($buyerOrder);
       foreach( $buyerOrder as $key => $eachOrder){
        if($eachOrder->seller_user_id !== $isCancelled[0]->seller_user_id){
            unset($buyerOrder[$key]);
        }
       }

        
       /* foreach($parentOrder as $eachOrder){
                array_push($sellers,$eachOrder->seller_user_id);
        }
        if(count(array_unique($sellers)) >1){
            $parentOrder = \DB::connection('mysql2')->select('select * from trans_po_hdr as tph left join trans_po_dtl as tpd on tph.po_id = tpd.po_id where tph.po_id ='.$invoiceId);
        }*/
        if($supplierStatus == 1){
            $sellerDetails = \DB::connection('mysql2')->select('select u.user_id,u.name,u.mobile,u.email,u.gst_no,u.pan_no,u.comp_name,s.ss_pincode,rua.Addr_1,rua.Addr_2,rua.area_name,rua.city_name,rua.state_name,rua.pincode from mst_user u left join mst_supplier s on s.created_by = u.user_id left join ref_user_address rua on rua.user_id = u.user_id where u.user_id = '.$parentOrder[0]->seller_user_id);
            $sellerDetails = (object)$sellerDetails[0];
        }else{
            $sellerDetails = array();
        }
        
        $totalQnty = array_sum(array_column($parentOrder,'qty'));

        $totalApprovedQty = array_sum(array_column($parentOrder,'approved_qty'));
        if($totalQnty == $totalApprovedQty && $isCancelled[0]->is_active == 2){
            $orderStatus = 2;//order accepted
        }else if($totalQnty != $totalApprovedQty && $isCancelled[0]->is_active == 2){
            $orderStatus = 3; //partial order
        }else if($isCancelled[0]->is_active == 1){
            $orderStatus = 1;//new order
        }else{
            $orderStatus = 4;//cancelled order
        }
        
        $buyerDetails = \DB::connection('mysql2')->select('select mu.user_id,mu.name,mu.comp_name,mu.mobile,mu.email,mu.gst_no,mu.pan_no,rua.pincode, rua.Addr_1,rua.Addr_2,rua.area_name,rua.city_name,rua.state_name from mst_user mu left join ref_user_address rua on mu.user_id = rua.user_id where mu.user_id ='.$parentOrder[0]->buyer_user_id);
        $delivery = \DB::connection('mysql2')->select('select * from trans_del_hdr where po_id = '.$invoiceId);
        $delivery =$delivery[0];
        //dd($delivery);
        $buyerDetails = (object)$buyerDetails[0];
        if($buyerDetails->state_name == 'Maharashtra' || $buyerDetails->state_name == 'Maharashtra ' || $buyerDetails->state_name == 'Maharastra'){
            $gstStatus = 1;
            $parentOrder = (object)$parentOrder[0];
            $digit = new numberToWordConverterController;
            $inWords =  $digit->convert_number_to_words((string)round($delivery->b_baseamt+$delivery->b_sgst_amt+$delivery->b_cgst_amt,0));
            $tst = round($delivery->b_del_total, 0);
            $roundOff = number_format(round($delivery->b_baseamt+$delivery->b_sgst_amt+$delivery->b_cgst_amt, 0) - ($delivery->b_baseamt+$delivery->b_sgst_amt+$delivery->b_cgst_amt), 2, '.', '');
        }else{
            $gstStatus = 2;
            $parentOrder = (object)$parentOrder[0];
            $digit = new numberToWordConverterController;
            $inWords =  $digit->convert_number_to_words((string)round($delivery->b_baseamt+$delivery->b_cgst_amt+$delivery->b_sgst_amt,0));
            $tst = round($delivery->b_del_total, 0);
            $roundOff = number_format(round($delivery->b_baseamt+$delivery->b_cgst_amt+$delivery->b_sgst_amt, 0) - ($delivery->b_baseamt+$delivery->b_cgst_amt+$delivery->b_sgst_amt), 2, '.', '');
        }
            
         if($orderStatus == 3){
                $products = \DB::connection('mysql2')->select('select * from trans_po_hdr as tph left join trans_del_dtl as tdd on tph.po_id = tdd.po_id left join trans_po_dtl as tpd on tpd.po_dtl_id=tdd.po_dtl_id where tph.po_id ='.$invoiceId);
                foreach($products as $key => $eachProducts){
                    if($eachProducts->qty != $eachProducts->approved_qty && $eachProducts->approved_qty==0 && $eachProducts->del_qty == null){
                        unset($products[$key]);
                    }
               }
            $buyerOrder = $products;
            
        }
        return view('orders.invoice',compact('sellerDetails','roundOff','buyerDetails','inWords','buyerOrder','parentOrder','from','delivery','orderStatus','supplierStatus','gstStatus')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(invoice $invoice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(invoice $invoice)
    {
        //
    }
}
