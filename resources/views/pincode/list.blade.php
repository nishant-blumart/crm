@extends('adminlte::page')

@section('title', 'BLUMART | Pincode List')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Pincode List</h4>
        </div>
        <div class="col-md-4">

        </div>
        <div class="col-md-4">
           <a href="#" class="export btn btn-warning btn-sm mr-3">Import Format</a>
            <a href="#" class="export btn btn-primary btn-sm mr-3">Import Pincode</a>
          <a href="{{ url('pincodeExport') }}" class="export btn btn-success btn-sm ">Export</a>
        </div>
        <hr>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        
                        <th class="text-left">statename</th>
                        <th class="text-left">Region Name</th>
                        <th class="text-left">pincode</th>
                        <th class="text-left">View</th>

                    </tr>
                </thead>
                <tbody>
                  @foreach($maharashtraPincodes as $eachRow => $eachPincode)
                    <tr class="pincodeRows" id="{{$eachPincode->id}}" >
                      <td>{{$eachPincode->id}}</td>
                      <td>{{$eachPincode->statename}}</td>
                      <td>{{$eachPincode->regionname}}</td>
                      <td>{{$eachPincode->pincode}}</td>
                      <td>
                        <a href="{{ url('') }}" target="_blank" class="viewSeller btn btn-success btn-sm ">View</a>
                        
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>

      </div>          
    </div>
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      .sellerRows > td { text-align: left; }

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    
    <!-- <script type="text/javascript">
      $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('sellerList') }}",
        columns: [
            {data: 'user_id', name: 'Sr.No'},
            {data: 'name', name: 'name'},
            {data: 'comp_name', name: 'comp_name'},
            {data: 'mobile', name: 'mobile'},
            {data: 'email', name: 'email'},

            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]
    });
    
      });
      </script> -->
      <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
  });
</script>

@stop

