<html>
 <head>
  <title>Blumart | Search Buyer</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
 </head>
 <body>
  <br />

   <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
  
  <div class="container box" style="margin-top: 7%;overflow-x:auto;">
   <div class="panel panel-default">
    <div class="panel-heading"><b>Search Buyer</b></div>
    <div class="panel-body">
     <div class="form-group">
      <input type="text" name="search" id="search" class="form-control" placeholder="Search Customer Data" />
     </div>
     <div class="table-responsive">
      <h3 align="center">Total Count : <span id="total_records"></span></h3>
      <table class="table table-striped table-bordered">
       <thead>
        <tr>
         <th>Buyer Name</th>
         <th>Company Name</th>
         <th>Mobile</th>
         <th>Auto Pay</th>
        </tr>
       </thead>
       <tbody>
       </tbody>
      </table>
     </div>
    </div>    
   </div>
  </div>

  <div class="modal fade" tabindex="-1" id="subscriptionMobile" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog" style="width: 90%;">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Subscription Mobile Number </h4>
              <button type="button" class="close" style="margin-top: -36px;color: black;font-size: 36px;font-weight: 900;" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-6">
                  <form method="post" action="/subscription/new" style="margin-left: 6%;margin-right: 6%;">
                    @csrf
                    <div class="form-group">
                      <label for="Mobile">Mobile Numer : </label>
                      <input type="text" name="mobile" placeholder="Enter Your Mobile Number" class="form-control" id="mobile" maxlength="10">
                      <input type="hidden" name="id" value="" id="id">
                      <input type="hidden" name="user_type" value="2" id="user_type">
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-success" value="Send">
                    </div>
                  </form>
                </div>
                
              </div>
            </div>
           
          </div>
        </div>
    </div>


 </body>
</html>

<script>
$(document).ready(function(){

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('tbody').html(data.table_data);
    $('#total_records').text(data.total_data);
   }
  })
 }

 $(document).on('keyup', '#search', function(){
  var query = $(this).val();
  if(query !== ''){
    fetch_customer_data(query);
  }
 });

 $(document).on('click', '#SubscriptionBtn', function(event) {
    var userId = $(this).attr('data-id');
    var mobile = $(this).attr('data-mobile');
         $('#id').val(userId);
          $('#mobile').val(mobile);  
         $('#subscriptionMobile').modal('show');
    });
});
</script>