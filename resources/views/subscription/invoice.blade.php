@extends('adminlte::page')

<title>BLUMART | INV-{{ $invoice_no }}</title>

@section('content')
<!DOCTYPE html>
<html>
      <div class="row">
        <!-- <div class="col-md-6">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Invoice</h4>
        </div> -->
        <div class="col-md-2">
        </div>
        <div class="col-md-4 float-right">
        </div>
      </div>

            <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content" >
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Main content -->
            <div class="invoice p-3 mb-3" style="border: 1px solid black;">
              <!-- title row -->
              <div class="row p-2">
                
                <div class="col-9 text-center">
                   <h2 class="pl-5"> Invoice</h2>
                </div>
                <div class="col-3">
                    <img class="" src="{{asset('img/blumart-logo.png')}}">
                </div>               
                <!-- /.col -->
              </div>
              <hr>
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col" style="border-right: 1px solid black;">
                  <p class="mb-2"><b>FROM :</b></p>
                  <address>
                    <b>{{$from['name']}}</b><br>
                    Address : {{$from['address']}}<br>
                    CIN : {{$from['cin']}}<br>
                    Email : {{$from['email']}}<br>
                    GST :  {{$from['gst']}}
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col" style="border-right: 1px solid black;">
                  <p class="mb-2"><b>TO :</b></p>
                  <address>
                    <b>{{$to->comp_name}}</b><br>
                    <!--strong>Name : {{$to->name}}</strong><br-->
                    Address : {{$to->Addr_1}}<br>
                    {{$to->Addr_2}},{{$to->area_name}}<br>
                    {{$to->city_name}},{{$to->state_name}},{{$to->pincode}}<br>
                    PAN : {{$to->pan_no}}
                    Phone: {{$to->ss_mobile}}<br>
                    Email: {{$to->ss_email}}<br>
                    GST : {{$to->gst_no}}<br>
                    
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <b>Invoice No :</b> INV-{{$invoice_no}}<br>
                  <hr>
                  <b>Invoice Date :</b> <?php echo date('d-m-Y', $invoice['issued_at']);?><hr>
                  <b>Reference ID :</b> {{$invoice['id']}}<hr>
                  <b>Payment Status :</b> {{$invoice['status']}}<hr>
                  
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Sr.No</th>
                      <th>Particulars</th>
                      <th>HSN/SAC</th>
                      <th>Description</th>
                      <th>Quantity</th>
                      <!-- <th>CGST</th>
                      <th>SGST</th> -->
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td>BLUMART SUBSCRIPTION</td>
                      <td>9995</td>
                      <td>Subscription Fees towards Izysk solutions Pvt. Ltd</td>
                      <td>01</td>
                      <!-- <td>9%</td>
                      <td>9%</td> -->
                      <td>{{$total}}</td>
                    </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <hr>
              <br>
              <!-- /.row -->
              <div class="row">
                <!-- accepted payments column -->
                <div class="col-3">
                <h4 class="lead"></h4>
                 

                  <p class="text-muted well well-sm shadow-none" style="margin-top: 10px;">
                    
                  </p>
                </div>
                <!-- /.col -->
                <div class="col-9">
                 <!--  <p class="lead"><b>Amount Paid</b></p> -->
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th style="width:50%">Subscription Fees :</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>{{$subTotal}}</td>
                      </tr>
                      <tr>
                        <th>CGST (9%) :</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>{{$gst}}</td>
                      </tr>
                      <tr>
                        <th>SGST (9%) :</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>{{$gst}}</td>
                      </tr>
                      <tr>
                        <th>Round Off (~) :</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>(+) 0.01</td>
                      </tr>
                      <tr>
                        <th>Total :</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>{{$total}}</td>
                      </tr>
                      <tr>
                        <th>Amount Chargable(in Words):</th>
                        <td><i class="fa fa-inr" aria-hidden="true"></i>{{$amountInWords}}</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

              <div class="row mt-5">
                <div class="col-6" style="float:left;margin-top:170px">
                  <b>E. & O.E.</b>
                </div>
                <div class="col-6 border border-secondary" style="height:200px;border-radius:20px;">
                  
                  <div clas="izyskStampUpper"style="position: relative;margin-left: 240px;margin-top: 3px;" >
                    <p style="float: right;position: absolute;">For I-ZYSK Solutions Pvt. Ltd</p>
                     <p class="izyskStamp" style="float: right;margin-top:166px;position: relative;">Authorised Signatory</p>
                  </div>
                  
                </div>
              </div>
              <hr>

              <div class="row mt-5">
                <div class="col-6 mb-4" style="border-right: 1px solid black;">
                  <p><b>Declaration : </b></p>
                  <ol style="list-style-type: inherit;">
                    <li>Company's PAN : <b>AAFCI5469R</b></li>
                    <li>We Declare that this invoice shows the actual price of the services described and that all particulars are true and correct.</li>
                    <li>This is Computer generated Invoice. It does not require Signature.</li>                    
                    <!-- <li>This is Computer generated Invoice. it does not require Signature.</li> -->
                  </ol>
                </div>
                <div class="col-6 mb-4">
                 
                  
                  <p><b>Terms and Conditions : </b></p>
                  <ol style="list-style-type: inherit;">
                    <!-- <li>Arvind sir's terms and Conditions will come here.arvind sir's terms and Conditions will come here</li> -->
                    <li>Subject to Mumbai Jurisdiction.</li>
                    
                  </ol>
                </div>
              </div>

              <hr>
              <!-- this row will not appear when printing -->
              <div class="row no-print">
                <div class="col-12">
                  <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fas fa-print"></i> Print</a>
                  <!-- <button type="button" class="btn btn-success float-right"><i class="far fa-credit-card"></i> Submit
                    Payment
                  </button> -->
                  <button type="button" class="btn btn-primary float-right" style="margin-right: 5px;">
                    <i class="fas fa-download"></i> Generate PDF
                  </button>
                </div>
              </div>
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
    
        
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      hr{
        border-top: 1px solid black;
      }
    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/subscription.js')}}"></script>

@stop

