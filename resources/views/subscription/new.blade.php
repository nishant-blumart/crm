@extends('adminlte::page')

@section('title', 'BLUMART | Create Subscription Plan')



@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Create Subscription Plan</h4>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-4 float-right">
        </div>
      </div>
<hr>
      <div class="row">
        <div class="col-md-6">
        <div class="card">
          <div class="card-header text-center">
            <b>General Details</b>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <form action="{{ url('/subscription/store') }}" method="post" class="was-validated">
                  @csrf
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="zone">Select Billing Cycle</label>
                        <select name="billingCycle" class="form-control" id="billingCycle">
                              <!-- <option value="weekly">daily</option>
                              <option value="weekly">weekly</option> -->
                              <option value="1">Monthly</option>
                              <option value="3">Quarterly</option>
                              <option value="6">Half Yearly</option>
                              <option value="12">Yearly</option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="subscription_interval">Interval:</label>
                        <select name="interval" class="form-control" id="interval">
                              <!-- <option value="weekly">daily</option>
                              <option value="weekly">weekly</option> -->
                              <option value="1">1</option>
                              <option value="2">2</option>
                              <option value="3">3</option>
                              <option value="4">4</option>
                              <option value="5">5</option>
                              <option value="6">6</option>
                              <option value="7">7</option>
                              <option value="8">8</option>
                              <option value="9">9</option>
                              <option value="10">10</option>
                          </select>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="zone">Plan Name</label>
                        <input type="text" class="form-control" id="plan_name" placeholder="Enter Buyer Name" name="plan_name" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="plan_amount">Amount :</label>
                        <input type="text" class="form-control" id="plan_amount" placeholder="Enter Plan Amount" name="plan_amount" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="plan_desc">Plan Description :</label>
                        <textarea id="plan_desc" class="form-control" placeholder="Enter Plan Description" name="plan_desc" rows="3" cols="42" ></textarea>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="plan_desc">Created By :</label>
                        <input type="text" value="{{ Auth::user()->name }}" data-id="{{ Auth::user()->id }}" class="form-control" id="created_by" placeholder="Enter Plan Amount" name="created_by" disabled>
                         
                      </div>
                    </div>
                    <div class="col-md-12 text-center">
                      <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Submit">
                      </div>
                    </div>
                     </div>
                     </form> 
                </div>
            </div>
              </div>
        </div>
      </div>

      <div class="col-md-6">
          <div class="card">
          <div class="card-header text-center">
            <b>Existing Plans</b>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="planBox col-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Plan Name</th>
                      <th>Amount</th>
                      <th>Plan Key</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                      @foreach ($plans as $eachRow => $EachPlan)
                      <tr>
                        <td value="{{ $EachPlan->name}}">{{ $EachPlan->name}}</td>
                        <td value="{{ $EachPlan->amount}}">@php echo $EachPlan->amount/100 @endphp</td>
                        <td value="{{ $EachPlan->id}}">{{ $EachPlan->id}}</td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer p-0">
            <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
          </div>
      </div>

</div>
        
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/subscription.js')}}"></script>

@stop

