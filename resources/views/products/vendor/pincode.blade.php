@extends('adminlte::page')

@section('title', 'BLUMART | Products List')

@section('content')
   <div class="container-fluid">

    <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>
    
    
      <div class="row">
          <div class="col-md-12">
            <form name="vendorproductPincodeForm" method="post" action="{{route('vendorPincodeUploadSave')}}">
              @csrf
              <div class="row">
                 <div class="form-group col-4">
                  <label for="email">Vendor:</label>
                  <select name="seller" class="form-control seller">
                     @foreach($sellerList as $eachSeller)
                       <option value="{{$eachSeller->user_id}}">{{$eachSeller->comp_name}}</option>
                     @endforeach
                  </select>
                </div>
                
                <div class="form-group col-8">
                  <label for="reconDate">Pincodes to Upload:</label>
                    <!-- <input type="text" name="pincodes" class="form-control pincodes" id="pincodes" style="width: 100%;">   -->
                    <textarea id="pincodes" name="pincodes" rows="4" cols="80"></textarea>
                </div>
              </div>

                <div class="row">
                  <div class="form-group col-2" style="margin-top: 30px;">
                    <!-- <a href="#" id="vendorSearchProductsBtn" class="btn btn-success">Upload</a> -->
                    <input type="submit" class="btn btn-success UploadForm" value="Upload">
                    <a href="{{route('vendorPincodeUpload')}}" class="btn btn-danger">Reset</a>
                  </div>
                </div>
            </form>
        </div>
      </div>
      </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/seller.js')}}"></script>

@stop

