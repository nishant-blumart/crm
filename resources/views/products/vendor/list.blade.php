@extends('adminlte::page')

@section('title', 'BLUMART | Products List')

@section('content')
   <div class="container-fluid">
      <div class="row">
          <div class="col-md-12">
              <div class="row">
                 <div class="form-group col-2">
                  <label for="email">Vendor:</label>
                  <select name="seller" class="form-control seller">
                     @foreach($sellerList as $eachSeller)
                       <option value="{{$eachSeller->user_id}}">{{$eachSeller->comp_name}}</option>
                     @endforeach
                  </select>
                </div>
                <div class="form-group col-2">
                  <label for="status">Product Status:</label>
                  <select name="status" class="form-control status">
                    <option value="10">Select</option>
                    <option value="1">Active</option>
                    <option value="0">In Active</option>
                    <option value="2">Request For Review</option>
                    <option value="3">Disapproved</option>
                    <option value="4">Draft</option>
                    <option value="5">Submitted To Admin</option>
                  </select>
                </div>
                
                <div class="form-group col-2">
                  <label for="reconDate">Search By Name:</label>
                    <input type="text" name="product_Search" class="form-control product_Search" id="product_Search" style="width: 100%;">  
                </div>

                <div class="form-group col-2" style="margin-top: 30px;">
                  <a href="#" id="vendorSearchProductsBtn" class="btn btn-success">Search</a>
                  <a href="{{route('vendorproductsList')}}" class="btn btn-danger">Reset</a>
                </div>
              </div>
        </div>
      </div>
      <hr>
      <div class="container">
        <div class="row p-2 bg-white border rounded" id="productsData">
          </div>
        </div>
      </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/seller.js')}}"></script>

@stop

