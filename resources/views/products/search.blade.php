

<html>
 <head>
  <title>Blumart | Search Buyer</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>    
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>
    <style>
      .swal2-popup {
        font-size: 1.25rem;
        margin-top: 70% !important;
      }
      .loader{
        position: absolute;
        height: 70px;
        width: 70px;
        top: 96%;
        left: 54%;
        margin-left: -50px;
        margin-top: -50px;
        background-size: 100%;
    }
    .carousel-control-next-icon, .carousel-control-prev-icon{
      width: 15px;
      height: 15px;
    }
    .carousel-control-next-icon, .carousel-control-next-icon{
      width: 15px;
      height: 15px;
    }

    @media only screen and (min-width: 300px) and (max-width: 400px)  {
    #cartCount {
          height: 18px;
          width: 18px;
          border-radius: 50%;
          display: inline-block;
          background-color: red;
          margin-left: 313px;
        }
        .cartImg{
          width: 24%;
          margin-top: -26px;
    }
   
    }

    @media only screen and (min-width: 401px) and (max-width: 768px)  {
    #cartCount {
          height: 18px;
          width: 18px;
          border-radius: 50%;
          display: inline-block;
          background-color: red;
          margin-left: 168px;
        }
        .cartImg{
          width: 24%;
          margin-top: -26px;
    }
   
    }

     @media only screen and (min-width: 768px) and (max-width: 1024px)  {
          #cartCount {
          height: 18px;
          width: 18px;
          border-radius: 50%;
          display: inline-block;
          background-color: red;
          margin-left: 168px;
        }
        .cartImg{
          width: 35%;
          margin-top: 0px;
        
  }
</style>

<script>
$(document).ready(function(){
  $('#cartCount').css('display','none'); 
  $('.loader').css('display','none');
 function fetch_product_data(query = '')
 {
  $.ajax({
   url:"{{ route('product_search.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('.productListing').html(data.product_data);
   }
  })
 }

 $(document).on('keyup', '#search', function(){

        var query = $(this).val();
        if(query !== ''){
          fetch_product_data(query);
        }
       });

        $(document).on('keyup', '.qtyMain', function(){
          var qty = $(this).val();

          var prodId = $(this).attr('data-id');
          var rate = $('.rate_'+prodId).attr('id');
          var gst = $('.cgst_'+prodId).attr('id');
          var gross = (rate*qty);
          //var total_gst = 2*gst;
          //var total =gross + (gross*total_gst)/100;
          $('#totalAmount_'+prodId).text('Total : '+gross.toFixed(2));
       });

      $(document).on('click', '.addToCartBtn', function(e){
          var prodId = $(this).attr('id');
          var qty_val = $("#qty_"+prodId).val();
          var result = (qty_val - Math.floor(qty_val)) !== 0;
          if (result){
            alert('Decimals are not allowed');
             e.preventDefault();
           }else if(qty_val ==''){
            alert('Please provide Quantity');
            e.preventDefault();
          }else if(qty_val == 0 || qty_val < 0 || qty_val == 'undefined'){
            alert('Zero and Negative values are not allowed');
             e.preventDefault();
          }else{
             var productDetails = {};
          
          var img = $("#img_"+prodId).attr('src');
          var prodName = $("#prodName_"+prodId).attr('data-name');
          var rate = $('.rate_'+prodId).attr('id');
          var gst = $('.cgst_'+prodId).attr('id');
          var hsn = $('.hsn_'+prodId).attr('id');
          var uom = $('.uom_'+prodId).attr('id');

          var gross = parseFloat((rate*qty_val));

          var total_gst = 2*gst;
          var baseAmount = (gross*100)/(100+total_gst);
          productDetails['id'] = prodId;
          productDetails['qty'] = qty_val;
          productDetails['rate'] = rate;
          productDetails['gst'] = total_gst;
          productDetails['gst_value'] = (gross-baseAmount).toFixed(2);
          productDetails['gross'] = baseAmount.toFixed(2);
          productDetails['total'] = gross;
          productDetails['img'] = img;
          productDetails['prodName'] = prodName;
          productDetails['hsn'] = hsn;
          productDetails['uom'] = uom;
           swal({
                title: "Product Added to the Cart",
                type:'success',
                showCancelButton: false,
                confirmButtonColor: "#1FAB45",
                confirmButtonText: "Ok",
                buttonsStyling: true,
                timer: 3000,
                width: '300px',
            }).then(function () {   
                    $.ajax({
                      headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                   url:"{{ route('cart.add') }}",
                   method:'POST',
                   data:{productDetails:productDetails},
                   dataType:'json',
                   success:function(data)
                   {
                    $('#cartItemCount').text('');
                    $('#cartItemCount').text(data);
                    $('#cartCount').show();
                   }
                  });
            });
          }
         

       })

    $(document).on('click', '.categorySearch', function(){
        var catagory = $(this).attr('id');
        $('.loader').css('display','block');
        $.ajax({
       url:"{{ route('catagory_search.action') }}",
       method:'GET',
       data:{catagory:catagory},
       dataType:'json',
       success:function(data)
       {
        $('.loader').css('display','none');
        $('.productListing').html(data.product_data);
       }
      })
     })

});
</script>
 </head>
 <body>
   <div class="container-fluid" style="background-color: #feffe0;">
    <div class="row">
      <div class="col-12 pr-5 pt-3">
        <img src="{{asset('img/Blumart_Logo-01.png')}}" style="width: 44%;margin-left: 15px;top:0;right:0;" placeholder="blumart_logo"> 
        <!-- <a href="{{ url('cart/show') }}" class="btn btn-success float-right"><i class="fa fa-cart-arrow-down"></i> Go To Cart</a> -->
        <div id="cartCount"><b id="cartItemCount" style="color: #fff;margin-left: 0.6rem!important;"></b></div>
         <a href="{{ url('cart/show') }}" class="float-right">
          <img src="{{asset('img/cart_icon.png')}}" class="float-right cartImg" placeholder="blumart_logo">
        </a>


      </div>
    </div>
      <div class="row">
        <div class="col-12">

          <div class="card m-4">
            <div class="card-header text-center">
              <b class="" style="font-size: 2rem;">Search Products</b>
            </div>
            <div class="card-body" style="font-size: 1.25rem;">
                <div class="form-group">
                  <input type="text" name="search" id="search" class="form-control" placeholder="Search Product" />
                </div>
                <hr>
                <div class="row">
                  <div class="col-3">
                    <a href="#" id="5" class="categorySearch">
                      <img src="{{asset('img/Category_icon_Grocery.png')}}" style="width: 80%;" class="ml-3 mb-1">
                    </a>
                    <p class="text-center">Grocery</p>
                  </div>
                  <div class="col-3">
                    <a href="#" id="6" class="categorySearch" >
                      <img src="{{asset('img/Category_icon_Home_kitchen.png')}}" style="width: 80%;" class="ml-3 mb-1">
                    </a>
                    <p class="text-center">Home & kitchen</p>
                  </div>
                  <div class="col-3">
                    <a href="#" id="7" class="categorySearch" >
                      <img src="{{asset('img/Category_icon_Packaged_food.png')}}" style="width: 80%;" class="ml-3 mb-1">
                    </a>
                    <p class="text-center">Packaged food</p>
                  </div>
                  <div class="col-3">
                    <a href="#" id="8" class="categorySearch">
                      <img src="{{asset('img/Category_icon_Personal_Care.png')}}" style="width: 80%;" class="ml-3 mb-1">
                    </a>
                    <p class="text-center">Personal Care</p>
                  </div>

                </div>

                <!-- <div class="form-group">
                  <label>Search By Catagory</label>
                 <select name="cars" id="cars" class="form-control">
                    <option value="0">All</option>
                    <option value="5">Grocery</option>
                    <option value="6">Home & Kitchen</option>
                    <option value="7">Packaged Food</option>
                    <option value="8">Personal Care</option>
                  </select>
                </div> -->


            </div>
            <!-- <div class="card-footer text-center">
              <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
            </div> -->
          </div>
          <!-- <div class="ajaxLoader">
                <img src="{{asset('img/loader.gif')}}" class="loader" placeholder="loader">
          </div> -->
          <img src="{{asset('img/loader.gif')}}" class="loader" placeholder="loader">
        </div>
      </div>

      <!-- Carousal starts here -->
      <div id="demo" class="carousel slide" data-ride="carousel">

          <!-- Indicators -->
         <!--  <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul> -->
          
          <!-- The slideshow -->
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="{{asset('img/Blucart_Combo_Mobile-01.jpg')}}" alt="Los Angeles" style="width:384px;height: 160px;">
            </div>
            <div class="carousel-item">
              <img src="{{asset('img/Blucart_Combo_Mobile-02.jpg')}}" alt="Chicago" style="width:384px;height: 160px;">
            </div>
            <div class="carousel-item">
              <img src="{{asset('img/Blucart_Combo_Mobile-03.jpg')}}" alt="New York" style="width:384px;height: 160px;">
            </div>
            <div class="carousel-item">
              <img src="{{asset('img/Blucart_Combo_Mobile-04.jpg')}}" alt="New York" style="width:384px;height: 160px;">
            </div>
          </div>
          
          <!-- Left and right controls -->
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
</div>





      <div class="productListing">
      </div>
    </div>

    
 </body>
</html>



