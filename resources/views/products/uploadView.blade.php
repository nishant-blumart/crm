@extends('adminlte::page')

@section('title', 'BLUMART | Products Upload')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header">
              <b>Bulk Product Upload</b>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <form name="productUploadForm" method="post" action="{{url('products/save')}}" id="productUploadForm" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleInputEmail1">Upload File</label>
                      <div class="custom-file">
                        <input type="file" name="productsCsvFile" class="custom-file-input" accept=".xlsx, .xls, .csv" id="productsCsvFile">
                        <label class="custom-file-label" for="customFile">Choose file</label>
                      </div>
                    </div>
                    <button type="submit" class="btn btn-success">Validate & Upload</button>
                    <!-- <button type="submit" class="btn btn-success float-right">Download Upload Excel Format</button> -->
                    <a href="/downloadSampleproductCsv" class="btn btn-primary float-right"><i class="icon-download-alt"> </i> Download Upload Excel Format </a>
                  </form>
                </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
            </div>
          </div>
        </div>
         <div class="col-6">
          <div class="card">
            <div class="card-header text-center">
              <b>Message Board for Errors</b>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <div class="flash-message">
                    @if (\Session::has('success'))
                          <p class="alert alert-success">
                            {{ Session::get('success')}}
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                     @endif
                    @if (\Session::has('errors'))
                      @foreach($errors as $key => $error)
                          <p class="alert alert-danger">
                            Row Number {{$key+2}} contains error at column {{$error}} Non numeric value found'
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                      @endforeach
                     @endif
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
            </div>
          </div>
         </div>
      </div>
      @if(!empty($products))
        <div class="col-6 mb-2">
            <a href="{{ route('downloadRecentVendorProductsCsv') }}" class="btn btn-info">Download Products</a>
        </div>
      @endif


      <div class="row">
        <div class="col-md-12">
          <div class="sellerList-table">
            <div class="row">
              <div class="col-md-12 mt-10">
                <table class="table table-hover" id="Product-table">
                  <thead>
                      <tr>
                          <th class="text-left">VPI</th>
                           <th class="text-left">Company Name</th>
                           <th class="text-left">Product Name</th>
                           <th class="text-left">MRP</th>
                          <th class="text-left">MOQ</th>
                          <th class="text-left">Q1</th>
                          <th class="text-left">P1</th>
                          <th class="text-left">Q2</th>
                          <th class="text-left">P2</th>
                          <th class="text-left">Q3</th>
                          <th class="text-left">P3</th>
                          <th class="text-left">Q4</th>
                          <th class="text-left">P4</th>
                          <th class="text-left">Approved Pin</th>
                      </tr>
                  </thead>
                  <tbody>
                    @foreach($products as $eachRow => $product)
                      <tr class="sellerRows" id="{{$product->vend_prod_id}}" >
                        <td>{{$product->vend_prod_id}}</td>
                        <td>{{$product->comp_name}}</td>
                        <td>{{$product->product_name}}</td>
                        <td>{{$product->mrp}}</td>
                        <td>{{$product->ret_minord_qnty}}</td>
                        <td>{{$product->ord_qnt_rtl_a}}</td>
                        <td>{{$product->ret_minord_qnty_cost_a}}</td>
                        <td>{{$product->ord_qnt_rtl_b}}</td>
                        <td>{{$product->ret_minord_qnty_cost_b}}</td>
                        <td>{{$product->ord_qnt_rtl_c}}</td>
                        <td>{{$product->ret_minord_qnty_cost_c}}</td>
                        <td>{{$product->ord_qnt_rtl_d}}</td>
                        <td>{{$product->ret_minord_qnty_cost_d}}</td>
                        <td>{{$product->approved_pin}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>


              
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
    $('#productsCsvFile').on('change',function(){
                var fileName = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
                $(this).next('.custom-file-label').html(fileName);
            })
  });
</script>
@stop

