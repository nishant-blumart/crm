@extends('adminlte::page')

@section('title', 'BLUMART | Active Products List')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Active Products List</h4>
        </div>
        <div class="col-md-2">
          
        </div>
        <div class="col-md-6 float-right">
          <a href="{{ url('buyerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a>
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div>
        <hr>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">Image</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Catagory</th>
                        <!-- <th class="text-left">Sub Cat</th>
                        <th class="text-left">Child Cat</th> -->
                        <th class="text-left">HSN</th>
                        <th class="text-left">SKU</th>
                        <th class="text-left">UOM</th>
                        <th class="text-left">GST</th>
                        <th class="text-left">CASE SIZE</th>
                        <th class="text-left">CASE WEIGHT</th>
                        <th class="text-left">CASE UOM</th>
                        <th class="text-left">Sold By</th>
                        <!-- <th class="text-left">Created At</th> -->
                    </tr>
                </thead>
                <tbody>
                  @foreach($products as $eachRow => $eachProduct)
                   
                    <tr class="sellerRows" id="{{$eachRow}}" >
                        <td>{{$eachProduct->vend_prod_id}}</td>
                        <td><!-- <img src='{{Storage::disk("s3")->url($eachProduct->media)}}' style="width: 100px;height: 100px;"> --></td>
                        <td>{{$eachProduct->product_name}}</td>
                        <td>{{$eachProduct->cat_name}}</td>
                        <!-- <td>{{$eachProduct->sub_cat_name}}</td>
                        <td>{{$eachProduct->child_cat_name}}</td> -->
                        <td>{{$eachProduct->hsn_no}}</td>
                        <td>{{$eachProduct->sku_size}}</td>
                        <td>{{$eachProduct->uom}}</td>
                        <td>{{$eachProduct->gst}}</td>
                        <td>{{$eachProduct->case_size}}</td>
                        <td>{{$eachProduct->case_weight}}</td>
                        <td>{{$eachProduct->case_uom}}</td>
                        <td>{{$eachProduct->name}}</td>
                        <!-- <td>{{$eachProduct->created_at}}</td> -->
                    </tr>
                  @endforeach
                
                </tbody>
            </table>
        </div>
    </div>

      </div>          
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      .sellerRows > td { text-align: left; }

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    
    <!-- <script type="text/javascript">
      $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('sellerList') }}",
        columns: [
            {data: 'user_id', name: 'Sr.No'},
            {data: 'name', name: 'name'},
            {data: 'comp_name', name: 'comp_name'},
            {data: 'mobile', name: 'mobile'},
            {data: 'email', name: 'email'},

            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]
    });
    
      });
      </script> -->
      <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
  });
</script>

@stop

