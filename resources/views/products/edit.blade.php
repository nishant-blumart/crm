@extends('adminlte::page')

@section('title', 'BLUMART | Product Information')

@section('content')
<meta name="_token" content="{{ csrf_token() }}" />
  

  <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))
      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>

    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header">
            <div class="d-flex">
              <div class="title">
               <h6>Product Name : {{$product->product_name}} ({{$product->prod_id}})</h6>
              </div>
              <div class="ml-auto">
                <a class="text-dark" href="https://elmah.io/?utm_source=codepen&utm_medium=social&utm_campaign=codepen" target="_blank">
                  <div class="elmahio-ad d-flex">
                    <div class="logo">
                      <img src="https://elmah.io/images/logo.png" />
                    </div>
                    <div class="motto d-none d-sm-block px-2"><a href="{{ url('buyerExport') }}" class="export btn btn-success btn-sm float-right">Export</a></div>
                  </div>
                </a>
              </div> 
            </div>
      
      <!-- START TABS DIV -->
      <div class="tabbable-responsive">
        <div class="tabbable">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">Basic Info</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="second-tab" data-toggle="tab" href="#second" role="tab" aria-controls="second" aria-selected="false">Vendors</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="third-tab" data-toggle="tab" href="#third" role="tab" aria-controls="third" aria-selected="false">buyer</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="fourth-tab" data-toggle="tab" href="#fourth" role="tab" aria-controls="fourth" aria-selected="false">Orders</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="fifth-tab" data-toggle="tab" href="#fifth" role="tab" aria-controls="fifth" aria-selected="false">Pincodes</a>
            </li>
          </ul>
        </div>
      </div>

    </div>
    <div class="card-body">
      
      <div class="tab-content">
        <div class="tab-pane fade show active" id="first" role="tabpanel" aria-labelledby="first-tab">
          <form name="product_edit_form" action="{{ route('catalogProductUpdate')}}" method="post">
            @csrf
              <div class="row">
                <div class="col-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Product Name</label>
                    <input type="text" class="form-control" id="product_name" name="product_name" value="{{$product->product_name}}" placeholder="Enter Product Name">
                    <input type="hidden" id="prod_id" name="prod_id" value="{{$product->prod_id}}">
                  </div>
                </div>
                <div class="col-6">
                  <label for="hsn">HSN CODE :</label>
                    <input type="text" class="form-control" id="hsn" name="hsn" value="{{$product->hsn_no}}" placeholder="Enter HSN">
                </div>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>

        <div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
          <div class="col-md-12 mt-10">
            <div class="row">
                  <div class="col-md-12 mt-10">
                      <table class="table table-hover" id="Product-table">
                          <thead>
                              <tr>
                                  <th class="">#</th>
                                  <th class="">ID</th>
                                  <th class="">Name</th>
                                  <th class="">Company Name</th>
                                  <th class="">Mobile</th>
                                  <th class="">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                           @foreach($vendors as $eachRow => $eachVendor)
                              <tr class="sellerRows" id="{{$eachVendor->user_id}}" >
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$eachVendor->user_id}}</td>
                                <td>{{$eachVendor->name}}</td>
                                <td>{{$eachVendor->comp_name}}</td>
                                <td>{{$eachVendor->mobile}}</td>
                                <td>
                                  <a href="/seller/{{$eachVendor->user_id}}" class="viewProduct btn btn-success btn-sm">View</a>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
        </div>
        </div>

        <div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
          <div class="col-md-12 mt-10">
            <div class="row">
                  <div class="col-md-12 mt-10">
                      <table class="table table-hover" id="buyer-table">
                          <thead>
                              <tr>
                                  <th class="">#</th>
                                  <th class="">ID</th>
                                  <th class="">Name</th>
                                  <th class="">Company Name</th>
                                  <th class="">Mobile</th>
                                  <th class="">Total Qnty</th>
                                  <th class="">Total Amount</th>
                                  <th class="">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                           @foreach($buyers as $eachRow => $eachBuyer)
                              <tr class="sellerRows" id="{{$eachVendor->user_id}}" >
                                <td>{{ $loop->iteration }}</td>
                                <td>{{$eachBuyer->user_id}}</td>
                                <td>{{$eachBuyer->name}}</td>
                                <td>{{$eachBuyer->comp_name}}</td>
                                <td>{{$eachBuyer->mobile}}</td>
                                <td>{{$eachBuyer->total_qty}}</td>
                                <td>{{round($eachBuyer->total_amount, 2)}}</td>
                                <td>
                                  <a href="/buyer/{{$eachBuyer->user_id}}" class="viewProduct btn btn-success btn-sm">View</a>
                                </td>
                              </tr>
                            @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
        </div>
        </div>

        <div class="tab-pane fade" id="fourth" role="tabpanel" aria-labelledby="fourth-tab">
          <div class="col-md-12 mt-10">
            <div class="row">
                  <div class="col-md-12 mt-10">
                      <table class="table table-hover" id="order-table">
                          <thead>
                              <tr>
                                  <th class="">#</th>
                                  <th class="">Date</th>
                                  <th class="">Products</th>
                                  <th class="">Amount</th>
                                  <th class="">CGST</th>
                                  <th class="">SGST</th>
                                  <th class="text-center">PAY TYPE</th>
                                  <th class="">BA</th>
                                  <th class="">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                           
                          </tbody>
                      </table>
                  </div>
              </div>
        </div>
        </div>

        <div class="tab-pane fade" id="fifth" role="tabpanel" aria-labelledby="fifth-tab">
          <div class="row">
            <div class="col-12">
              <div id="pincodeContainer"></div>
            </div>
          </div>
        </div>

  
        </div>
      </div>
      <!-- END TABS DIV -->
      
    </div>
  </div>
        </div>
      </div>



     <!-- Extra large modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <div class="modal-body">
              <table class="table table-dark">
                  <thead>
                    <tr>
                      <th>Product ID</th>
                      <th>MRP</th>
                      <th>Rates</th>
                      <th>inventory</th>
                      <th>Min Order quantity</th>
                      <th>Pincodes</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td id="prod_id"></td>
                      <td id="mrp"></td>
                      <td id="rates"></td>
                      <td id="inventory"></td>
                      <td id="moq"></td>
                      <td id="pincodes"></td>
                    </tr>
                  </tbody>
              </table>
             
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

   <div class="modal fade" tabindex="-1" id="subscriptionMobile" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Subscription Mobile Number </h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-12">
                  <form method="post" action="/subscription/new">
                    @csrf
                    <div class="form-group">
                      <label for="Mobile">Mobile Numer : </label>
                      <input type="text" name="mobile" placeholder="Enter Your Mobile Number" class="form-control" id="mobile" maxlength="10">
                      <input type="hidden" name="id" value="" id="id">
                      <input type="hidden" name="user_type" value="2" id="user_type">
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-success" value="Send">
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>

@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/bootstrap_tabs.css')}}" type="text/css">
 <!--style>
      .sellerRows > td { text-align: center; }
    </style-->

@stop
@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/bootstrap-tabs.js')}}"></script>
    <script src="{{asset('js/buyer.js')}}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/full-screen.js"></script>
<script type="text/javascript">
   // Radialize the colors
Highcharts.setOptions({
  colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
    return {
      radialGradient: {
        cx: 0.5,
        cy: 0.3,
        r: 0.7
      },
      stops: [
        [0, color],
        [1, Highcharts.color(color).brighten(-0.3).get('rgb')] // darken
      ]
    };
  })
});

// Build the chart
Highcharts.chart('pincodeContainer', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
   text: '<b> '+<?php echo json_encode($product->product_name) ?>+'</b>',
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
        connectorColor: 'silver'
      }
    }
  },
  series: [{
    name: 'Share',
    data: [
      { name: 'Chrome', y: 61.41 },
      { name: 'Internet Explorer', y: 11.84 },
      { name: 'Firefox', y: 10.85 },
      { name: 'Edge', y: 4.67 },
      { name: 'Safari', y: 4.18 },
      { name: 'Other', y: 7.05 }
    ]
  }]
});

</script>

    <script type="text/javascript">
      $(document).ready(function() {
        var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
        var table2 = $('#order-table').DataTable({order:[[0,"desc"]]});
        var table3 = $('#buyer-table').DataTable({order:[[0,"desc"]]});
      });
    </script>
@stop

