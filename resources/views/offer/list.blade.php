@extends('adminlte::page')

@section('title', 'BLUCART | Offer List')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Order List</h4>
        </div>
        <div class="col-md-2">
          
        </div>
        <div class="col-md-6 float-right">
          <a href="{{ url('buyerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a>
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div>
        <hr>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">Date</th>
                        <th class="text-left">oFFER Name</th>
                        <th class="text-left">MRP</th>
                        <th class="text-left">PRICE</th>
                        <th class="text-left">Created By</th>
                        <th class="text-left">GST</th>
                        <th class="text-left">Total Amount</th>
                        <th class="text-left">Payment Id</th>
                        <th class="text-left">Payment Status</th>
                        <th class="text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($list as $eachRow => $order)
                    <tr class="sellerRows" id="{{$order->id}}" >
                      <td>{{$order->id}}</td>
                      <td>{{$order->created_at}}</td>
                      <td>{{$order->name}}</td>
                       <td>{{$order->mobile}}</td>
                      <td>{{$order->qty}}</td>
                      <td>{{$order->gross_amt}}</td>
                      <td>{{$order->gst}}</td>
                      <td>{{$order->total}}</td>
                      <td>{{$order->payment_id}}</td>
                      <td>{{$order->payment_status}}</td>
                      <td>
                         <a href="{{ url('/blucart/order', [$order->id]) }}" target="_blank" class="viewOrder"><i class="fa fa-eye text-green" title="View Order"></i></a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>

      </div>          
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
      .sellerRows > td { text-align: left; }

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

      <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
  });
</script>

@stop

