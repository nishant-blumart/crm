@extends('adminlte::page')

@section('title', 'BLUMART | Create Subscription Plan')



@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
        <div class="card">
          <div class="card-header text-center">
            <b>General Details</b>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <form action="{{ url('/offer/save') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="zone">Offer Number :</label>
                        <input type="text" class="form-control" id="offer_id" name="offer_id" value="{{$id}}" disabled>
                      </div>
                      <div class="form-group">
                        <label for="zone">Offer Name :</label><span style="color:red;">{{$errors->first('name')}}</span>
                        <input type="text" class="form-control" id="name" value="{{old('name')}}" placeholder="Enter Offer Name" name="name" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="plan_amount">Offer MRP :</label><span style="color:red;">{{$errors->first('mrp')}}</span>
                        <input type="text" class="form-control" id="mrp" value="{{old('mrp')}}"  placeholder="Enter Offer MRP" name="mrp" required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="plan_amount">Offer Price :</label><span style="color:red;">{{$errors->first('price')}}</span>
                        <input type="text" class="form-control" id="price" value="{{old('price')}}"  placeholder="Enter Offer Price" name="price" required>
                      </div>
                    </div>
                    <div class="col-md-12 my-2">
                      <div class="form-group">
                        <label for="plan_amount">Offer Banner :</label><span style="color:red;">{{$errors->first('banner')}}</span>
                            <input type="file" name="banner" accept="image/*" / required>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="offer_desc">Offer Information :</label>
                        <textarea id="information" class="form-control" placeholder="Enter Offer Description" name="information" rows="3" cols="42" ></textarea>
                      </div>
                    </div>
                    <div class="col-md-12 my-2">
                      <div class="form-group">
                        <label for="plan_desc">Offer Status :</label>
                        <label class="switch">
                          <input type="checkbox" name="status" checked>
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="plan_desc">Created By :</label>
                        <input type="text" value="{{ Auth::user()->id }}" data-id="{{ Auth::user()->id }}" class="form-control" id="created_by" placeholder="Enter Plan Amount" name="created_by" readonly>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="plan_desc">Added Products :</label>
                        <div class="row addedProducts">
                          
                        </div>
                      </div>
                    </div>
                    <input type="hidden" id="products" name="products" value="">
                    <div class="col-md-12 text-center">
                      <div class="form-group">
                        <input type="submit" class="btn btn-success" value="Submit">
                      </div>
                    </div>
                     </div>
                </form> 
                </div>
            </div>
              </div>
        </div>
      </div>

      <div class="col-md-6">
        <div class="card">
          <div class="card-header text-center">
            <b>Products under this Offer</b>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="planBox col-12">

                <div class="col-md-12">
                      <div class="row">
                        <div class="col-9">
                          <div class="form-group">
                          <label for="plan_amount">Search Product :</label>
                          <input type="text" class="form-control" id="prod_search" placeholder="Enter Plan Amount" name="prod_search" required>
                          </div>
                        </div>
                        <div class="col-3">
                          <div class="form-group">
                            <input type="button" class="btn btn-primary" style="margin-top: 30px;" id="prod_search_btn" value="search" name="prod_search_btn" required>
                          </div>  
                        </div>
                      </div>
                      <hr>
                      <div class="productListing">
                      </div>
                </div>
              </div>
            </div>
          </div>
          <div class="card-footer p-0">
            <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
          </div>
      </div>
</div>
        
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function(){
      function fetch_product_data(query = ''){
      $.ajax({
       url:"{{ route('offer_product_search.action') }}",
       method:'GET',
       data:{query:query},
       dataType:'json',
       success:function(data)
       {
        $('.productListing').html(data.product_data);
       }
      });
     }

      $(document).on('click', '#prod_search_btn', function(){
        var query = $('#prod_search').val();
        if(query !== ''){
          fetch_product_data(query);
        }
       });

       $(document).on('click', '.addProductBtn', function(){
            var obj = {};
            var elems = {};
            var products = $('#products').val();
            var id = $(this).attr('data-id');
            var mrp = $('.mrp_'+id).val();
            var price = $('.price_'+id).val();
            obj['id'] = id;
            obj['mrp'] = mrp;
            obj['price'] = price;
            elems[id] = obj;
            appendDivtoRow(id);
            if(products == ''){
                $('#products').val(JSON.stringify(elems));
                //console.log($('#products').val());
            }else{
                var products = $('#products').val();
                var raw = JSON.parse(products);
                var NewId = $(this).attr('data-id');
                raw[NewId] = obj;
                $('#products').val();
                $('#products').val(JSON.stringify(raw));
                console.log($('#products').val());
            }
            
       });


       function appendDivtoRow(id){
          var imgPath = $('#img_'+id).attr('src');
           $('.addedProducts').append('<div class="col-3"><img id="id_'+id+'" src="'+imgPath+'" placeholder="Product Image" style="width: 100px;height: 100px;"></div>');
       }

    });

    </script>
@stop

