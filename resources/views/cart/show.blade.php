

<html>
 <head>
  <title>Blumart | Search Buyer</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>
    <script>
$(document).ready(function(){


    

 function fetch_product_data(query = '')
 {
  $.ajax({
   url:"{{ route('product_search.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('.productListing').html(data.product_data);
   }
  })
 }

 $(document).on('keyup', '#search', function(){
  var query = $(this).val();
  if(query !== ''){
    fetch_product_data(query);
  }
 });

  $(document).on('keyup', '.qtyMain', function(){
    var qty = $(this).val();
    var prodId = $(this).attr('data-id');
    var rate = $('.rate_'+prodId).attr('id');
    var gst = $('.cgst_'+prodId).attr('id');
    var gross = (rate*qty);
    var total_gst = 2*gst;
    var total =gross + (gross*total_gst)/100;
    $('#totalAmount_'+prodId).text('Total : '+total.toFixed(2));
 });

$(document).on('click', '.addToCartBtn', function(){
    var productDetails = {};
    var prodId = $(this).attr('id');
    var qty_val = $("#qty_"+prodId).val();
    var img = $("#img_"+prodId).attr('src');
    var rate = $('.rate_'+prodId).attr('id');
    var gst = $('.cgst_'+prodId).attr('id');
    var gross = (rate*qty_val);
    var total_gst = 2*gst;
    var total =gross + (gross*total_gst)/100;
    productDetails['id'] = prodId;
    productDetails['qty'] = qty_val;
    productDetails['rate'] = rate;
    productDetails['gst'] = total_gst;
    productDetails['gst_value'] = total-gross;
    productDetails['gross'] = gross;
    productDetails['total'] = total;
    productDetails['img'] = img;
    
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
   url:"{{ route('cart.add') }}",
   method:'POST',
   data:{productDetails:productDetails},
   dataType:'json',
   success:function(data)
   {
    $('.productListing').html(data.product_data);
   }
  })


 })

$(document).on('click', '.categorySearch', function(){
    var catagory = $(this).attr('id');
    $.ajax({
   url:"{{ route('catagory_search.action') }}",
   method:'GET',
   data:{catagory:catagory},
   dataType:'json',
   success:function(data)
   {
    $('.productListing').html(data.product_data);
   }
  })
 });

$(document).on('click', '.removeItemBtn', function(){
        var productId = $(this).closest('div').attr('id');
        swal({
          title: "Are you sure Want to Remove ?",
          showCancelButton: true,
          cancelButtonText: 'Cancel',
          confirmButtonColor: "red",
          confirmButtonText: "remove",
          buttonsStyling: true,
          width: '300px',
      }).then(function (result) {
           if (result.value) {
                          $.ajax({
                            headers: {
                              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
                         url:'/cart/item/remove',
                         method:'post',
                         data:{productId:productId},
                         dataType:'json',
                         success:function(response)
                         {
                          if(response ==1){
                             window.location.href = '/cart/show';
                          }
                         }
                        });
            }else if(result.dismiss == 'cancel'){
           console.log('cancel');
        }
      });
 });


$(document).on('click', '.clearCartBtn', function(){
    swal({
          title: "Your Cart is Empty Now",
          type:"success",
          confirmButtonColor: "green",
          confirmButtonText: "ok",
          buttonsStyling: true,
          width: '300px',
          timer: 3000
      }).then(function (isconfirm) {
           if (isconfirm) {
                $.ajax({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
               url:'/cart/clear',
               method:'get',
               data:'',
               dataType:'json',
               success:function(response)
               {
                if(response ==1){
                   window.location.href = '/products/search';
                }
               }
              });
            }else{
              e.preventDefault();
            }
      });
 });




});
</script>
<style>
  .swal2-popup {
        font-size: 1.25rem;
        margin-top: 70% !important;
      }
     .blackiconcolor {color:black;}
</style>
 </head>
 <body>
   <div class="container-fluid">
    <div class="row">
      <div class="col-12 pr-5 pt-3">
        <img src="{{asset('img/Blumart_Logo-01.png')}}" style="width: 44%;margin-left: 15px;" placeholder="blumart_logo">
      </div>
    </div>
      <div class="row">
        <div class="col-12">
          <div class="card m-4">
            <div class="card-header text-center">
              <b>Cart Details</b>
            </div>
            <div class="card-body">
               @if($cartItems =='')
               @else
              <div class="row text-center">
                <div class="col-4">
                  <p> <b>Total Quantity: {{$cartDetails['totalQnty']}}</b></p>
                </div>
                <div class="col-4">
                  <p><b>Amount (excluding GST) : {{$cartDetails['grossTotalAmount']}}</b> </p>
                </div>
                <div class="col-4">
                  <p><b>Total GST : {{$cartDetails['totalGst']}}</b> </p>
                </div>
              </div>
              

          <div class="row text-center">
              <div class="col-4">
                  <p><b data-amount="{{$cartDetails['totalAmount']}}" class="totalAmount">Final Amount : {{$cartDetails['totalAmount']}} </b></p>
                </div>
                <div class="col-4">
                  <p><a href="{{ url('cart/buyerDetails') }}" class="btn btn-primary">Place Order</a></p>
                </div>
                <div class="col-4">
                  <p><a href="#" class="btn btn-danger clearCartBtn">Clear Cart</a></p>
                </div>
          </div>
@endif
                <div class="row">
                  <div class="col-12">
                    @if($cartItems =='')
                       <div class="card mb-3">
                        <div class="card-header text-center" style="background-color:#fbb848;font-size:1.5rem;">
                          <b>Your Cart is Empty</b>

                        </div>

                        <div class="card-body text-center">
                            <div class="row">
                              <div class="col-12" style="border-radius:1px solid black;">
                                <a href="/products/search" class="btn btn-primary">Shop Now</a>
                              </div>
                            </div>
                        </div>
                      </div>
                    @else
                       @foreach($cartItems as $eachItem)
                      <div class="card mb-3">

                        <div class="card-header text-center" style="background-color:#fbb848;font-size:1.5rem;">
                          <div class="removeItemBtn float-right" id="{{$eachItem['id']}}"><i class="fa fa-times-circle fa-lg blackiconcolor" aria-hidden="true"></i></div>
                          <b>{{ $eachItem['prodName']}}</b>
                        </div>

                        <div class="card-body">
                            <div class="row">
                              <div class="col-4" style="border-radius:1px solid black;">
                                  <img class="prodImg" src="{{ $eachItem['img']}}" style="width: 80px;height: 80px;">
                              </div>
                              <div class="col-8">
                                <div class="row">
                                  <div class="col-12 pb-1">
                                    <div class="row mb-2">
                                      <!-- <div class="col-6">
                                        MRP: {{$eachItem['rate']}}
                                      </div> -->
                                      <div class="col-6">
                                       <p class=""><b>Quantity : {{$eachItem['qty']}}</b></p>
                                      </div>
                                      
                                      <div class=col-6>
                                       <p><b>Rate: {{$eachItem['rate']}}</b></p>
                                      </div>
                                    </div>
                                    <div class="row mb-2">
                                      <div class="col-6">
                                       <p class=""><b>Gross : {{$eachItem['gross']}}</b></p>
                                      </div>
                                      <div class="col-6">
                                       <p class=""><b>GST : {{round($eachItem['gst_value'],2)}} ({{$eachItem['gst']}}%)</b></p>
                                      </div>
                                    </div>
                                    <hr>
                                     <div class="row mb-2">
                                      <div class="col-6">
                                       <p class=""><b>Total : {{$eachItem['total']}}</b></p>
                                      </div>
                                      
                                    </div>

                                  </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endforeach
                    @endif
                      </div>
                  </div>
                </div>
            </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="productListing">
      </div>
    </div>

<script>

  $(document).on('click', '.placeOrderBtn', function(){
          var amount = $('.totalAmount').attr('data-amount');
          var options = {
          "key": "rzp_test_XBePz643U27Uu9",
          "amount": amount*100,
          "currency": "INR",
          "name": "Izysk Solutions Pvt. Ltd",
          "description": "Order Details",
          "image": "https://blumart.co.in/logos/izysk.png",
          "handler": function (response){
              alert(response.razorpay_payment_id);
              alert(response.razorpay_order_id);
              alert(response.razorpay_signature)
          },
          "prefill": {
              "name": "Gaurav Kumar",
              "email": "gaurav.kumar@example.com",
              "contact": "9999999999"
          },
          "notes": {
              "address": "Razorpay Corporate Office"
          },
          "theme": {
              "color": "#3399cc"
          }
      };
      var rzp1 = new Razorpay(options);
      rzp1.on('payment.failed', function (response){
              alert(response.error.code);
              alert(response.error.description);
              alert(response.error.source);
              alert(response.error.step);
              alert(response.error.reason);
              alert(response.error.metadata.order_id);
              alert(response.error.metadata.payment_id);
      });
      document.getElementById('rzp-button1').onclick = function(e){
          rzp1.open();
          e.preventDefault();
      }
  });

</script>



 </body>
</html>



