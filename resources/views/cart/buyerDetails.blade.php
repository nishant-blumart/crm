

<html>
 <head>
  <title>Blumart | Search Buyer</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>

    <script>
$(document).ready(function(){

});
</script>
 </head>
 <body>
   <div class="container-fluid">
    <div class="row">
      <div class="col-12 pr-5 pt-3">
        <img src="{{asset('img/Blumart_Logo-01.png')}}" style="width: 44%;margin-left: 15px;" placeholder="blumart_logo">
      </div>
    </div>
      <div class="row">
        <div class="col-12">
          <div class="card m-4">
            <div class="card-header text-center">
              <b>Provide Your Contact Details</b>
            </div>
            <div class="card-body">
                <form name="buyerDetails" method="post" action="{{ route('cart.submitBuyerDetails') }}">
                  @csrf
                    <div class="form-group">
                      <label for="Name">Name</label>
                      <input type="text" class="form-control" name="name" id="name" aria-describedby="" placeholder="Enter Your Name" required>
                      <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Mobile</label>
                      <input type="number" name="mobile" class="form-control" name="" id="mobile" placeholder="Mobile Number" required>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Email</label>
                      <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Address</label>
                      <input type="text" class="form-control" name="address" id="address" placeholder="Enter Your Address" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Proceed to Pay</button>
                </form>

            </div>
            </div>
            
          </div>
        </div>
      </div>
      <div class="productListing">
      </div>
    </div>

      <script>

      </script>
 </body>
</html>



