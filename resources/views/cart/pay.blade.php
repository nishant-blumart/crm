

<html>
 <head>
  <title>Blumart | Search Buyer</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="{{asset('js/session.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>

 </head>
 <body>
   <div class="container-fluid">
    <div class="row">
      <div class="col-12 pr-5 pt-3">
        <img src="{{asset('img/Blumart_Logo-01.png')}}" style="width: 44%;margin-left: 15px;" placeholder="blumart_logo">
      </div>
    </div>
      <div class="row">
        <div class="col-12">
          <div class="card m-4">
            <div class="card-header text-center">
              <b>Pay to Place Your Order</b>
            </div>
            <div class="card-body text-center">
              <input type="hidden" name="amount" id="amount" value="{{ $cartDetails['totalAmount'] }}">
              <input type="hidden" name="name" id="name" value="{{ $buyerDetails['name'] }}">
              <input type="hidden" name="email" id="email" value="{{ $buyerDetails['email'] }}">
              <input type="hidden" name="mobile" id="mobile" value="{{ $buyerDetails['mobile'] }}">
              <input type="hidden" name="address" id="address" value="{{ $buyerDetails['address'] }}">
              <input type="hidden" name="grossTotalAmount" id="grossTotalAmount" value="{{ $cartDetails['grossTotalAmount'] }}">
              <input type="hidden" name="totalGst" id="totalGst" value="{{ $cartDetails['totalGst'] }}">
              <input type="hidden" name="totalQnty" id="totalQnty" value="{{ $cartDetails['totalQnty'] }}">
              <input type="hidden" name="allProducts" id="allProducts" value="{{ serialize($allProducts) }}">
              
              <button class="btn btn-success placeOrderBtn"  id="rzp-button1">Place Order</button>
            </div>
            </div>
            
          </div>
        </div>
      </div>
<script>



  $(document).on('click', '.placeOrderBtn', function(){
    var orderDetails = {};
          var amount =parseFloat($('#amount').val()).toFixed(2);
          var name = $('#name').val();
          var email = $('#email').val();
          var mobile = $('#mobile').val();
          var address = $('#address').val();
          var allProducts = $('#allProducts').val();
          var grossTotalAmount = $('#grossTotalAmount').val();
          var totalGst = $('#totalGst').val();
          var totalQnty = $('#totalQnty').val();

          orderDetails['amount'] = amount;
          orderDetails['name'] = name;
          orderDetails['email'] = email;
          orderDetails['mobile'] = mobile;
          orderDetails['address'] = address;
          orderDetails['allProducts'] = allProducts;
          orderDetails['grossTotalAmount'] = grossTotalAmount;
          orderDetails['totalGst'] = totalGst;
          orderDetails['totalQnty'] = totalQnty;
          var options = {
          "key": "rzp_test_XBePz643U27Uu9",
          "amount": Math.floor(amount*100),
          "currency": "INR",
          "name": "Izysk Solutions Pvt. Ltd",
          "description": "Order Details",
          "image": "https://blumart.co.in/logos/izysk.png",
          "order_id": "",
          "handler": function (response){
              orderDetails['payment_id'] = response.razorpay_payment_id;
              if (typeof response.razorpay_payment_id == 'undefined' ||  response.razorpay_payment_id < 1) {
              } else {
                sessionStorage.setItem("payment_id", response.razorpay_payment_id);
                $.ajaxSetup({
                   headers: {
                       'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                   }
                 });
                $.ajax({
                        type:'POST',
                        url:"{{ route('orderSave') }}",
                        data:{razorpay_payment_id:response.razorpay_payment_id,orderDetails:orderDetails,payment_status:1},
                        success:function(data){

                            /*swal({
                                icon: 'success',
                                title: 'Your order is Booked. Order Number is :'+data,
                                showConfirmButton: true,
                                confirmButtonText: "Ok",
                                timer: 7000
                              });

                            setTimeout(function(){ window.location = "/products/search"; }, 8000);
                            location.href="/products/search";
*/
                            swal({
                                  title: "Your order is Booked. Order Number is :"+data,
                                  type:"success",
                                  showCancelButton: false,
                                  confirmButtonColor: "green",
                                  confirmButtonText: "Ok",
                                  buttonsStyling: true,
                                  width: '300px',
                                  timer: 8000
                              }).then(function (result) {
                                   if (result.value) {
                                      location.href="/products/search";   
                                    }
                              });
                             setTimeout(function(){ window.location = "/products/search"; }, 9000);
                        }
                    });
              }
          },
          "prefill": {
              "name": name,
              "email": email,
              "contact": mobile,
          },
          "notes": {
              "address": address
          },
          "theme": {
              "color": "#3399cc"
          }
      };
      var rzp1 = new Razorpay(options);
      rzp1.on('payment.failed', function (response){

            swal({
                  title: "Payment Processing Failed ! Please try Again",
                  type:"warning",
                  showCancelButton: false,
                  confirmButtonColor: "red",
                  confirmButtonText: "Ok",
                  buttonsStyling: true,
                  width: '300px',
                  timer: 10000
              }).then(function (result) {
              });

             /* alert(response.error.code);
              alert(response.error.description);
              alert(response.error.source);
              alert(response.error.step);
              alert(response.error.reason);
              alert(response.error.metadata.order_id);
              alert(response.error.metadata.payment_id);*/
      });
      document.getElementById('rzp-button1').onclick = function(e){
          rzp1.open();
          //e.preventDefault();
      }
  });

</script>
 </body>
</html>



