@extends('adminlte::page')

@section('title', 'BLUMART | Products Upload')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-6">
          <div class="card">
            <div class="card-header text-center">
              <b>Get OTP by Mobile Number</b>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-6">
                    <div class="form-group">
                      <label for="mobile">Enter Mobile Number</label>
                      <div class="custom-file">
                        <input type="text" class="form-control" name="mobile" id="mobile">
                      </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                      <div class="custom-file">
                        <a href="#" id="otpBtn" class="btn btn-success">Get OTP</a>
                      </div>
                    </div>
                </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
            </div>
          </div>
        </div>
        <div class="col-6">
          <div class="card">
            <div class="card-header text-center">
              <b>OTP</b>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-12">
                  <h3 id="otp" class="text-center"></h3>
                </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
            </div>
          </div>
         </div>
         
      </div>
      <div class="ajaxLoader">
        <img src="{{asset('img/1480.gif')}}" class="loader" placeholder="loader">
      </div>
              
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
     <style>
      .sellerRows > td { text-align: left; }
      .ajaxLoader{position:relative; height:100px;width:1000px;}
      .loader{position:absolute;left:0;right:0;top:0;bottom:0;margin:auto}
    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script>
      
      $(document).ready(function() {
      $('.loader').css('display','none');
        $('#otpBtn').click(function(e){
          $('#otp').text('');
          $('.loader').css('display','block');
          var mobile = $('#mobile').val();
                  $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "/get/onboarding/otp",
                    method: "POST",
                    data: {mobile:mobile},
                    cache: false,
                    success: function(data){
                        if(data == null || data == ''){
                          $('#otp').text('OTP Not Found');
                        }else if(isNaN(data)){
                          $('#otp').text(data);
                        }else{
                          $('#otp').text('OTP is :'+data);
                        }
                        $('.loader').css('display','none');
                      }
              }); 
        });

      });
    </script>
@stop

