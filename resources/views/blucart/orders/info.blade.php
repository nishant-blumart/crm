@extends('adminlte::page')

@section('title', 'BLUMART | Orders Information')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Order {{$order->id}}</h4>
        </div>
        <div class="col-md-2">
          
        </div>
        <!-- <div class="col-md-6 float-right">
          <a href="{{ url('buyerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a>
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div> -->
        <hr>
      </div>
      <hr>
      <div class="card">
        <div class="card-header bg-success">
          <div class="row">
            <div class="col-4">
             <!--  <b>Order Details for : {{$order->id}}</b> -->
            </div>
            <div class="col-4">
              <b>Order Details for : {{$order->id}}</b>
            </div>
            <div class="col-4">
              <b>Order Date : {{$order->created_at}}</b>
            </div>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-6">
                <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-4">
                   <p class="buyerHeading" style="font-size: 25px;"> Buyer Details</p>
                  </div>
                  <div class="col-8" >
                    <div class="row mt-2">
                        <div class="col-6">
                         
                        </div>
                        <div class="col-6">
                        <a href="{{ url('/blucart/invoice', [$order->id]) }}" class="btn btn-outline-danger btn-rounded waves-effect0 invoiceBtn" target="_blank"><i class="fa fa-print fa-1x" aria-hidden="true"></i>&nbsp;&nbsp;View Invoice</a>
                        </div>
                    </div>
                  </div>
                </div>
                
                <hr>
                <div class="row">
                  <div class="col-3" style="border-right: 1px solid black;">
                    <img src="{{asset('img/buyer.jpg')}}" style="width: 70%;" class="ml-3 mb-1"><br>
                    <a href="{{ url('/buyer', [$order->id]) }}" class="btn btn-success ml-2" style="margin-top: 25px;margin-left:35px !important;" target="_blank" class="viewBuyer">View</a>
                  </div>
                  <div class="col-9">
                    <p class="name mb-2"><b>Name :</b> {{$order->name}}</p>
                    <p class="comp_name"><b>Mobile :</b> {{$order->mobile}}</p>
                    <p class="pan_no"><b>Email Address :</b> {{$order->email}}</p>
                    <p class="pan_no"><b>Address :</b> {{$order->address}}</p>
                  </div>
                </div>
              </div>
            </div>
            </div>
            <div class="col-6">
                <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-6" >
                    <p class="buyerHeading" style="font-size: 25px;"> Seller Details</p>    
                  </div>
                  <div class="col-6" >
                     <a href="{{ url('/blucart/buyer', [$order->mobile]) }}" class="btn btn-outline-danger btn-rounded waves-effect0 poBtn"  target="_blank">View Purchase Order</a>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-3" style="border-right: 1px solid black;">
                    <img src="{{asset('img/buyer.jpg')}}" style="width: 70%;" class="ml-3 mb-1"><br>
                    <a href="{{ url('/seller', [$sellerDetails->id]) }}" class="btn btn-success ml-2" style="margin-top: 25px;margin-left:35px !important;" target="_blank" class="viewBuyer">View</a>
                  </div>
                  <div class="col-9">
                    <p class="name mb-2"><b>Name :</b> {{$sellerDetails->name}}</p>
                    <p class="comp_name"><b>Company Name :</b> {{$sellerDetails->name}}</p>
                    <p class="comp_name"><b>Mobile :</b> {{$sellerDetails->mobile}} &nbsp;&nbsp;  | &nbsp;&nbsp;   <b>Pincode :</b> {{$sellerDetails->pincode}}</p>
                    <p class="pan_no"><b>GST No :</b> {{$sellerDetails->gst_no}}</p>
                    <p class="pan_no"><b>Email Address :</b> {{$sellerDetails->email}}</p>
                  </div>
                </div>
                <hr>
                <div class="row">
                  <div class="col-12">
                   <p><b>Bill From Address  :</b> <br>{{$sellerDetails->Addr_1}}</p>
                   <p><b>Area  :</b> &nbsp;{{$sellerDetails->area_name}} &nbsp;|&nbsp; <b>City  :</b> &nbsp;{{$sellerDetails->city_name}}&nbsp;|&nbsp; <b>State  :</b> &nbsp;{{$sellerDetails->state_name}}</p>
                  </div>
                </div>

              </div>
            </div>
            </div>

          </div>

          <hr>
          <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Order No.</th>
                      <th>Total Products</th>
                      <th>Gross Amount</th>
                      <th>CGST</th>
                      <th>SGST</th>
                      <th>Total Amount</th>
                      <th>Payment Id</th>
                      <th>Payment Date</th>
                      <th>Payment Status</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>BLUCART-{{$order->id}}</td>
                      <td>{{$order->qty}}</td>
                      <td>{{$order->gross_amt}}</td>
                      <td>{{$order->gst/2}}</td>
                      <td>{{$order->gst/2}}</td>
                      <td>{{$order->total}}</td>
                      <td>{{$order->payment_id}}</td>
                      <td>{{$order->payment_date}}</td>
                      <td>{{$order->payment_status}}</td>
                    </tr>
                  </tbody>
                </table>
              </div>
          </div>
        </div>
        <!-- <div class="card-footer">Footer</div> -->
      </div>

      <div class="card">
        <div class="card-header text-center bg-success"><b>Product details</b></div>
        <div class="card-body">
          <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Product Image</th>
                      <th>Product Name</th>
                      <th>qty</th>
                      <th>rate</th>
                      <th>gross</th>
                      <th>GST</th>
                      <th>total</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach(unserialize($order->products) as $key => $eachProduct)
                    <tr>
                    <td>{{$eachProduct['id']}}</td>
                    <td><img src="{{$eachProduct['img']}}" style="width: 100px;height: 100px;" placeholder="Product Image"></td>
                    <td>{{$eachProduct['prodName']}}</td>
                    <td>{{$eachProduct['qty']}}</td>
                    <td>{{$eachProduct['rate']}}</td>
                    <td>{{$eachProduct['gross']}}</td>
                    <td>{{$eachProduct['gst_value']}}({{$eachProduct['gst']}}%)</td>
                    <td>{{$eachProduct['total']}}</td>
                    <tr>
                    @endforeach
                  </tbody>
                </table>
        </div>
        <div class="card-footer text-center">
          <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
        </div>
      </div>
              
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <style>
      .sellerRows > td { text-align: left; }
      .invoiceBtn{
        margin-left: 5%;
      }
      .poBtn{
        margin-left: 19%;
      }
      i.pe-7s-cart,i.OrderAccept,i.readyDispatch,i.dispatched,i.Delivered,i.cancelOrder {
        margin-bottom: 47px;
        font-size: 30px;
        height: 30px;
        vertical-align: middle;
    }
    
    .cancelOrderIcon{
        border-color: red !important;
        background-color: red !important;
    }
    
    .steps .step {
    display: block;
    width: 100%;
    text-align: center
}

.steps .step .step-icon-wrap {
    display: block;
    position: relative;
    width: 100%;
    height: 44px;
    text-align: center
}

.steps .step .step-icon-wrap::before,
.steps .step .step-icon-wrap::after {
    display: block;
    position: absolute;
    top: 50%;
    width: 50%;
    height: 3px;
    margin-top: -1px;
    background-color: #e1e7ec;
    content: '';
    z-index: 1
}

.steps .step .step-icon-wrap::before {
    left: 0
}

.steps .step .step-icon-wrap::after {
    right: 0
}

.steps .step .step-icon {
    display: inline-block;
    position: relative;
    width: 50px;
    height: 50px;
    border: 1px solid #e1e7ec;
    border-radius: 50%;
    background-color: #f5f5f5;
    color: #374250;
    font-size: 38px;
    line-height: 81px;
    z-index: 5
}

.steps .step .step-title {
    margin-top: 16px;
    margin-bottom: 0;
    color: #606975;
    font-size: 14px;
    font-weight: 500
}

.steps .step:first-child .step-icon-wrap::before {
    display: none
}

.steps .step:last-child .step-icon-wrap::after {
    display: none
}

.steps .step.completed .step-icon-wrap::before,
.steps .step.completed .step-icon-wrap::after {
    background-color: #0da9ef
}

.steps .step.completed .step-icon {
    border-color: #0da9ef;
    background-color: #0da9ef;
    color: #fff
}

@media (max-width: 576px) {
    .flex-sm-nowrap .step .step-icon-wrap::before,
    .flex-sm-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

@media (max-width: 768px) {
    .flex-md-nowrap .step .step-icon-wrap::before,
    .flex-md-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

@media (max-width: 991px) {
    .flex-lg-nowrap .step .step-icon-wrap::before,
    .flex-lg-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

@media (max-width: 1200px) {
    .flex-xl-nowrap .step .step-icon-wrap::before,
    .flex-xl-nowrap .step .step-icon-wrap::after {
        display: none
    }
}

.bg-faded, .bg-secondary {
    background-color: #f5f5f5 !important;
}
    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pixeden-stroke-7-icon@1.2.3/pe-icon-7-stroke/dist/pe-icon-7-stroke.min.css">
   
  <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
    var array = $('.invoiceBtn').attr('href').split('/');
    var invoice = array[4];
    var baseUrl = window.location.origin;
    window.sessionStorage.setItem("invoice", invoice);
    var getsession = window.sessionStorage.getItem("invoice");
    $('#switch1').on('change',function(){
      if ($(this).is(":checked" )){
        $('.invoiceBtn').attr("href", baseUrl+"/invoice/"+getsession+"/1");
      }else{
        $('.invoiceBtn').attr("href", baseUrl+"/invoice/"+getsession+"/0");
      }
    });

  });
</script>

@stop

