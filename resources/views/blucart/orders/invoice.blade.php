@extends('adminlte::page')

<title>BLUCART | 2021-{{$invoice->id}}</title>

@section('content')
<!DOCTYPE html>
<html>
      <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-4 float-right">
        </div>
      </div>

            <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tax Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>

    <section class="content" >
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="invoice" style="border: 1px solid black;">
              <div class="row" style="border-bottom: 1px solid black">
                <div class="col-9 text-center">
                   <h2>Tax Invoice</h2>
                </div>
                <div class="col-3">
                    <img class="" src="{{asset('img/blumart-logo.png')}}">
                </div>               
              </div>
              <div class="row">
                <div class="col-6">
                  <div class="row">
                      <div class="col-12">
                        <b>From :</b><br>
                      <b>{{$from->name}}</b><br>
                      <b>Address :</b>{{$from->Addr_1}} <br>
                      <b>CIN : </b>{{$from->cin}}<br>
                      <b>PAN : </b>{{$from->pan}} &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>GSTN : </b>{{$from->gst_no}} <br>
                      <b>Phone :</b>{{$from->mobile}} &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b> Email :</b> {{$from->email}}
                      </div>
                  </div>
                  <hr>
                  <div class="row">
                      <div class="col-12">
                        <b>Buyer(Billed To) :</b><br>
                      <b>Name : </b>{{$invoice->name}}<br>
                      <b>Address :</b>{{$invoice->address}} <br>
                      <b>Phone :</b>{{$invoice->mobile}} &nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>Pincode : </b>{{$invoice->mobile}}<br>
                      <b> Email :</b> {{$invoice->email}}
                      </div>
                  </div>
                </div>
                <div class="col-6">
                  <div class="row">
                    <div class="col-6 border-top-0" style="border: 1px solid black;">
                      <span>&nbsp;&nbsp; Invoice No :</span><br>
                      <span>&nbsp;&nbsp; BC-2021-{{$invoice->id}}</span>
                    </div>
                    <div class="col-6 border-top-0 border-left-0" style="border: 1px solid black;">
                      &nbsp;&nbsp; Dated :<br>
                      &nbsp;&nbsp; {{\Carbon\Carbon::parse($invoice->created_at)->format('d F Y')}}
                    </div>
                    <div class="col-6 border-top-0" style="border: 1px solid black;">
                     &nbsp;&nbsp; Delivery Note :<br>
                      &nbsp;&nbsp; Asdf
                    </div>
                    <div class="col-6 border-top-0 border-left-0" style="border: 1px solid black;">
                     &nbsp;&nbsp; Mode/Terms of Payment : <br>
                     &nbsp;&nbsp; Online
                    </div>
                    <div class="col-6 border-top-0" style="border: 1px solid black;">
                      &nbsp;&nbsp;Buyers Order No :<br>
                      &nbsp;&nbsp;  asdf
                    </div>
                    <div class="col-6 border-top-0 border-left-0" style="border: 1px solid black;">
                      &nbsp;&nbsp;Dated : <br>
                      &nbsp;&nbsp; {{\Carbon\Carbon::parse($invoice->created_at)->format('d F Y')}}
                    </div>
                    <div class="col-6 border-top-0" style="border: 1px solid black;">
                      &nbsp;&nbsp;Bill of Landing/LR-RR No :<br>
                      &nbsp;&nbsp;  asdf
                    </div>
                    <div class="col-6 border-top-0 border-left-0" style="border: 1px solid black;">
                      &nbsp;&nbsp;Motor Vehicle No: <br>
                       &nbsp;&nbsp;MH 03 CD 1763
                    </div>
                    <div class="col-12 border-top-0 border-bottom-0 border-right-0" style="border: 1px solid black;height: 145px;">
                     &nbsp;&nbsp;Terms of Delivery: <br>
                       &nbsp;&nbsp;Advance payment Done
                    </div>
                  </div>

                </div>
            </div>
          
              <div class="row">
                <div class="col-12 table-responsive" style="padding: 0;">
                  <table class="table table-bordered productDescTbl" style="margin-bottom: 0;">
                    <thead>
                    <tr class="productDescTr">
                      <th>Sr.</th>
                      <th>Description of Goods</th>
                      <th>HSN</th>
                      <th>Qty</th>
                      <th>UOM</th>
                      <th>Gross</th>
                      <th>Rate</th>
                      <th>CGST %</th>
                      <th>SGST %</th>
                      
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                      @foreach(unserialize($invoice->products) as $key => $eachProduct)
                    <tr class="productDescTr">
                    <td>{{$loop->iteration}}</td>
                    <td>{{$eachProduct['prodName']}}</td>
                    <td>{{$eachProduct['hsn']}}</td>
                    <td>{{$eachProduct['qty']}}</td>
                    <td>{{$eachProduct['uom']}}</td>
                    <td>{{$eachProduct['gross']}}</td>
                    <td>{{$eachProduct['rate']}}</td>
                    <td>{{$eachProduct['gst_value']/2}}({{$eachProduct['gst']/2}}%)</td>
                    <td>{{$eachProduct['gst_value']/2}}({{$eachProduct['gst']/2}}%)</td>
                    
                    <td>{{$eachProduct['total']}}</td>
                    <tr>
                    @endforeach
                      <tr  class="productDescTr">
                        <td></td>
                        <td><b>Total</b></td>
                        <td></td>
                        <td><b>{{$invoice->qty}}</b></td>
                        <td></td>
                        <td><b>{{$invoice->gross_amt}}</b></td>
                        <td><b>{{$invoice->gst/2}}</b></td>
                        <td><b>{{$invoice->gst/2}}</b></td>
                        <td><b>{{$invoice->gross_amt}}</b></td>
                        <td><b>{{$invoice->total}}</b></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-9">
                  Amount Chargeable (in words)<br>
                  <h5>INR {{ucfirst($inWords)}} Only</h5>
                </div>
                <div class="col-3 pt-2 text-center">
                  <b>E.&.O.E</b>
                </div>
              </div>
              <div class="row">
                <div class="col-12 table-responsive" style="padding: 0;">
                  <table class="table table-bordered productDescTbl" style="margin-bottom: 0;">
                    <thead>
                    <tr class="productDescTr">
                      <th>HSN/SAC</th>
                      <th>taxable Value</th>
                      <th colspan="2">Central Tax</th>
                      <th colspan="2">State Tax</th>
                      <th>Total Amount</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                      @foreach($masterHsnArray as $key => $eachProduct)
                      <tr class="productDescTr">
                      <td>{{$key}}</td>
                      <td>{{$eachProduct['gross']}}</td>
                      <td>{{$eachProduct['gst']/2}} %</td>
                      <td>{{$eachProduct['gst_value']/2}}</td>
                      <td>{{$eachProduct['gst']/2}} %</td>
                      <td>{{$eachProduct['gst_value']/2}}</td>
                      <td>{{$eachProduct['total']}}</td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-12">
                  <p><b style="padding-left: 15px;">Declaration : </b></p>
                  <ol style="list-style-type: inherit;">
                    <li>Company's PAN : <b>AAFCI5469R</b></li>
                    <li>We Declare that this invoice shows the actual price of the services described and that all particulars are true and correct.</li>
                    <li>This is Computer generated Invoice. It does not require Signature.</li>                    
                  </ol>
                </div>
                <div class="col-6">
                  <p><b style="padding-left: 15px;">Terms and Conditions : </b></p>
                  <ol style="list-style-type: inherit;">
                    <li>Subject to Mumbai Jurisdiction.</li>  
                  </ol>
                </div>
                <div class="col-6">
                  <div class="border border-secondary">
                    <p><b>For I-ZYSK Solutions Pvt. Ltd</b></p>
                     <p class="izyskStamp">Authorised Signatory</p>
                </div> 
                </div>
            </div>
            
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>        

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>       
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
      hr{
        border-top: 1px solid black;
      }
     
      address{
        line-height: 1.7;
      }
      h2{
        font-weight: 900;
      }
      .content-wrapper>.content{
        padding: 0 !important;
        margin-left: 30px;
        margin-right: 0px;
      }
      .container-fluid{
        font-weight: 625;
        padding: 0 !important;
      }
      table.text{
        font-weight: 700;
      }
      tr.productDescTr > th{
      border-color: black !important;
      border-left: 0;
    }

    tbody >tr{
      border-color: black !important;
    }

    tr.productDescTr >td{
      border-color: black !important;
      border-left: 0;
      border-left: 0;
    }


    .col-2{
      max-width: 14% !important;
    }
    .col-6{
      padding: 0;
    }
    .div2 {
      height: 120px;
      border: 1px solid black;
    }
    .amountDetails{
      margin-left: 23px;
      margin-right: 10px;
    }
    .paymentDetails{
      margin-right: 10px;
    }
    .invoiceBoxes{
      margin-right: 20px;
      padding-right: 10px;
    }
    .row{
      margin-left: 0px;
      margin-right: 0px;
    }
    

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/subscription.js')}}"></script>

@stop

