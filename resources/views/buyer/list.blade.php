@extends('adminlte::page')

@section('title', 'BLUMART | Buyer List')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b>Active Buyer List</h4>
        </div>
        <div class="col-md-2">
          
        </div>
        <div class="col-md-6 float-right">
          <a href="{{ url('buyerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a>
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div>
        <hr>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        
                        <th class="text-left">Name</th>
                        <th class="text-left">Company Name</th>
                        <th class="text-left">Mobile No.</th>
                        <th class="text-left">Pincode</th>
                        <th class="text-left">Status</th>
                        <th class="text-left">Onboard By</th>
                        <th class="text-left">Onboard Contact</th>
                        <th class="text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($buyerList as $eachRow => $eachBuyer)
                    <tr class="sellerRows" id="{{$eachBuyer->user_id}}" >
                      <td>{{$eachBuyer->user_id}}</td>
                      <td>{{$eachBuyer->name}}</td>
                      <td>{{$eachBuyer->comp_name}}</td>
                      <td>{{$eachBuyer->mobile}}</td>
                      <td>{{$eachBuyer->pincode}}</td>
                       @if($eachBuyer->is_active == 1)
                        <td>         
                          Active
                        </td>
                        @elseif($eachBuyer->is_active == 2)
                        <td>
                          On Review
                        </td>
                        @else
                        <td>
                          Draft
                        </td>       
                        @endif
                      <td>{{$eachBuyer->sales_team_name}}</td>
                      <td>{{$eachBuyer->sales_mob}}</td>
                      <td>
                        <a href="{{ url('/buyer', [$eachBuyer->user_id]) }}" target="_blank" class="viewBuyer"><i class="fa fa-eye text-green" title="View {{$eachBuyer->comp_name}}"></i></a>&nbsp;&nbsp;
                         <a href="{{ url('/buyer/edit', [$eachBuyer->user_id]) }}" target="_blank" class="editBuyer"><i class="fa fa-pencil-square-o" title="Edit {{$eachBuyer->comp_name}}"></i></a>
                         

                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>

      </div>          
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      .sellerRows > td { text-align: left; }

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    
    <!-- <script type="text/javascript">
      $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('sellerList') }}",
        columns: [
            {data: 'user_id', name: 'Sr.No'},
            {data: 'name', name: 'name'},
            {data: 'comp_name', name: 'comp_name'},
            {data: 'mobile', name: 'mobile'},
            {data: 'email', name: 'email'},

            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]
    });
    
      });
      </script> -->
      <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
  });
</script>

@stop

