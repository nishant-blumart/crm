@extends('adminlte::page')
@section('title', 'BLUMART | Buyer Edit')
@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-8">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> {{$buyerDetails->name}} ({{$buyerDetails->comp_name}})</h4>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-2 float-right">
        </div>
      </div>
      <div class="row">
            <div class="col-12">
              <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                  @if(Session::has('alert-' . $msg))
                  <p class="alert alert-{{ $msg }}"><b>{{ Session::get('alert-' . $msg) }} </b><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                  @endif
                @endforeach
            </div>
            </div>
      </div>

<hr>
<form action="{{url('buyer/update')}}" class="was-validated" method="post">
  {{ csrf_field() }}
  <div class="card">
    <div class="card-header text-center" style="background-color:lightgreen;">
      <b>Edit General Details</b>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="buyer_name">Names :</label>
                   <input type="hidden" value="{{$buyerDetails->user_id}}" class="form-control" id="buyer_id" name="buyer_id" required>
                  <input type="text" value="{{$buyerDetails->name}}" class="form-control" id="buyer_name" placeholder="Enter Buyer Name" name="buyer_name" required>
                  @error('buyer_name')
                    <div class="error">{{ $message }}</div>
                @enderror
                </div>
              </div>
                 <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Company Name :</label>
                    <input type="text" value="{{$buyerDetails->comp_name}}" class="form-control" id="company_name" placeholder="Enter Company Name" name="company_name" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="pwd">Mobile Number :</label>
                    <input type="text" value="{{$buyerDetails->mobile}}" class="form-control" id="buyer_mobile" name="buyer_mobile" readonly>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Email Address :</label>
                    <input type="email" value="{{$buyerDetails->email}}" class="form-control" id="buyer_email" placeholder="Enter Email Address" name="buyer_email">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Whatsapp Number :</label>
                    <input type="text" value="{{$buyerDetails->whatsapp_mob_no}}" class="form-control" id="buyer_whatsapp" placeholder="Enter Whatsapp Number" name="buyer_whatsapp">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Alternate Number :</label>
                    <input type="text" value="{{$buyerDetails->altr_mob_no}}" class="form-control" id="altr_mob_no" placeholder="Enter Alternative Number" name="altr_mob_no">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="buyer_address">Address 1 :</label>
                    <input type="text" value="{{$buyerDetails->Addr_1}}" class="form-control" id="buyer_address1" placeholder="Enter Address" name="buyer_address1">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="buyer_address">Address 2 :</label>
                    <input type="text" value="{{$buyerDetails->Addr_2}}" class="form-control" id="buyer_address2" placeholder="Enter Address" name="buyer_address2">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="buyer_address">Pincode :</label>
                    <input type="text" value="{{$buyerDetails->pincode}}" class="form-control" id="buyer_pincode" placeholder="Enter Pincode" name="buyer_pincode">
                  </div>
                </div>
              </div>
              <hr>  
          </div>
        </div>
      </div>
    </div>

    <div class="card">
    <div class="card-header text-center" style="background-color:lightgreen;">
      <b>Edit Documents Details</b>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="buyer_name">Pan Number :</label>
                  <input type="text" value="{{$buyerDetails->pan_no}}" class="form-control" id="pan_no" placeholder="Enter Pan Number" name="pan_no" required>
                </div>
              </div>
                 <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">GST Number :</label>
                    <input type="text" value="{{$buyerDetails->gst_no}}" class="form-control" id="gst_no" placeholder="Enter GST Number" name="gst_no">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="pwd">Aadhar Number :</label>
                    <input type="text" value="{{$buyerDetails->aadhar_no}}" class="form-control" placeholder="Enter Aadhar Number" id="aadhar_no" name="aadhar_no">
                  </div>
                </div>
              </div>
              <hr>  
          </div>
        </div>
      </div>
    </div>

    <div class="card">
    <div class="card-header text-center" style="background-color: lightgreen;">
      <b>Edit Beat Mapping</b>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="buyer_name">zoness :</label>
                  <select class="form-control" name="zone_id" id="zone_id" required id="zone_id">
                     @foreach($zones as $eachZone)
                        <option value="{{ $eachZone->zone_id }}"  {{ $eachZone->zone_id == $buyerDetails->zone_id ? 'selected="selected"' : '' }}>{{ $eachZone->zone_name}}</option>
                      @endforeach
                </select>
                </div>
              </div>
                 <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Region :</label>
                    <select class="form-control" name="region_id" required id="region_id">
                     @foreach($regions as $eachRegion)
                        <option value="{{ $eachRegion->region_id }}"  {{ $eachRegion->region_id == $buyerDetails->region_id ? 'selected="selected"' : '' }}>{{ $eachRegion->region_name}}</option>
                    @endforeach
                </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="pwd">Area :</label>
                    <select class="form-control" name="area_id" required id="area_id">
                     @foreach($area as $eachArea)
                        <option value="{{ $eachArea->area_id }}"  {{ $eachArea->area_id == $buyerDetails->area_id ? 'selected="selected"' : '' }}>{{ $eachArea->area_name}}</option>
                    @endforeach
                </select>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="pwd">Route :</label>
                    <select class="form-control" name="route_id" required id="route_id">
                     @foreach($route as $eachRoutes)
                        <option value="{{ $eachRoutes->route_id }}"  {{ $eachRoutes->route_id == $buyerDetails->route_id ? 'selected="selected"' : '' }}>{{ $eachRoutes->route_name}}</option>
                    @endforeach
                </select>
                  </div>
                </div>
              </div>
              <hr>  
          </div>
        </div>
      </div>
    </div>
     <div class="card">
        <div class="card-header text-center">
          <b>Update All Changes</b>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                  <label for="plan_desc">Updated By :</label>
                  <input type="text" value="{{ Auth::user()->name }}" class="form-control" name="updated_by" readonly>
                </div>
            </div>
            <div class="col-md-8">
              <div class="form-group">
                <label for="plan_desc">Update Button :</label>
                  <input type="submit" class="btn-block btn btn-success" name="update" value="UPDATE">
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/buyer.js')}}"></script>

@stop

