@extends('adminlte::page')

@section('title', 'BLUMART | Seller Information')

@section('content')
<meta name="_token" content="{{ csrf_token() }}" />

      <div class="row">
        <div class="col-12">
               <div class="card">
    <div class="card-header">
      
      <div class="d-flex">
        <div class="title">
         <h6>Seller Name : {{$sellerDetails->name}} ({{$sellerDetails->user_id}})</h6>
          <!-- <p class="mb-4">Created with &#x1F9E1; by <a href="https://elmah.io/?utm_source=codepen&utm_medium=social&utm_campaign=codepen" target="_blank">elmah.io</a> team.</p> -->
        </div>
        <div class="ml-auto">
          <a class="text-dark" href="https://elmah.io/?utm_source=codepen&utm_medium=social&utm_campaign=codepen" target="_blank">
            <div class="elmahio-ad d-flex">
              <div class="logo">
                <img src="https://elmah.io/images/logo.png" />
              </div>
              <div class="motto d-none d-sm-block px-2"><a href="{{ url('sellerExport') }}" class="export btn btn-success btn-sm float-right">Export</a></div>
            </div>
          </a>
        </div> 
      </div>
      
      <!-- START TABS DIV -->
      <div class="tabbable-responsive">
        <div class="tabbable">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">Basic Info</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="second-tab" data-toggle="tab" href="#second" role="tab" aria-controls="second" aria-selected="false">Documents</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="third-tab" data-toggle="tab" href="#third" role="tab" aria-controls="third" aria-selected="false">Products</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="fourth-tab" data-toggle="tab" href="#fourth" role="tab" aria-controls="fourth" aria-selected="false">Orders</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="fifth-tab" data-toggle="tab" href="#fifth" role="tab" aria-controls="fifth" aria-selected="false">Ownership</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="sixth-tab" data-toggle="tab" href="#sixth" role="tab" aria-controls="sixth" aria-selected="false">Sixth Tab</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="card-body">
      
      <div class="tab-content">
        <div class="tab-pane fade show active" id="first" role="tabpanel" aria-labelledby="first-tab">
          <div class="row">
              <div class="col-md-4">
                <b>NAME</b> : {{$sellerDetails->name}}
              </div>
              <div class="col-md-4">
                <b>Company NAME</b> : {{$sellerDetails->comp_name}}
              </div>
              <div class="col-md-4">
                <b>Mobile</b> : {{$sellerDetails->mobile}}
              </div>
          </div> 
              <hr>
           <div class="row">
               <div class="col-md-4">
                <b>Alternate Mob</b> : {{$sellerDetails->altr_mob_no}}
              </div>
              <div class="col-md-4">
                <b>Whatsapp</b> : {{$sellerDetails->whatsapp_mob_no}}
              </div>
              <div class="col-md-4">
                <b>Email</b> : {{$sellerDetails->email}}
              </div>
          </div>
           <hr>
           <div class="row">    
              <div class="col-md-4">
                <b>GST No.</b> : {{$sellerDetails->gst_no}}
              </div>
              <div class="col-md-4">
                <b>Aadhar No.</b> : {{$sellerDetails->aadhar_no}}
              </div>
              <div class="col-md-4">
                <b>Pancard No.</b> : {{$sellerDetails->pan_no}}
              </div>
          </div>
           <hr>
           <div class="row">    
               <div class="col-md-4 py-3 ">
                @if($sellerDetails->is_active == 1)         
                      <div> <b>Status</b> :<img src="{{asset('img/active-user.png')}}" style="width: 7%;" class="ml-3 mb-1"></div>
                @else
                     <div> <b>Status</b> :<img src="{{asset('img/inactive-user.png')}}" style="width: 7%;" class="ml-3 mb-1"></div>       
                @endif
              </div>
              <div class="col-md-4">
                <b>Seller ID :</b> <span id="sellerId">{{$sellerDetails->user_id}}</span>
              </div>
          </div>

        </div>
        <div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
          <h5 class="card-title">Second Tab header</h5>
          <p class="card-text">In hac habitasse platea dictumst. Cras sit amet massa fermentum risus eleifend malesuada vel nec erat. Cras massa tellus, volutpat efficitur feugiat eu, accumsan vel felis. Nullam ornare tellus eu dolor rhoncus, ut tempor lectus tincidunt. Ut in condimentum lectus. Praesent non pretium mauris, efficitur condimentum ex. Nam ante lorem, eleifend in egestas a, rhoncus at ex.</p>
        </div>

        <div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
          <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        
                        <th class="text-center">Product Name</th>
                        <th class="text-center">HSN</th>
                        <th class="text-center">CGST</th>
                        <th class="text-center">SGST</th>
                        <th class="text-center">MRP</th>
                        <!-- <th class="text-center">Pincodes</th> -->
                        <th class="text-center">View Rates</th>

                    </tr>
                </thead>
                <tbody>
                  @foreach($sellerProducts as $eachRow => $eachProduct)
                    <tr class="sellerRows" id="{{$eachProduct->prod_id}}" >
                      <td>{{$eachProduct->prod_id}}</td>
                      <td>{{$eachProduct->product_name}}</td>
                      <td>{{$eachProduct->hsn_no}}</td>
                      <td>{{$eachProduct->gst}}</td>
                      <td>{{$eachProduct->sgst}}</td>
                      <td>{{$eachProduct->mrp}}</td>
                      <!-- <td>{{$eachProduct->approved_pin}}</td> -->
                      <td>
                        <a href="" data-toggle="modal" data-target="#myModal"  class="viewProduct btn btn-success btn-sm">View</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>

      </div>
        </div>

        <div class="tab-pane fade" id="fourth" role="tabpanel" aria-labelledby="fourth-tab">
          <h5 class="card-title">Fourth Tab header</h5>
          <p class="card-text">Nulla dignissim justo sed nulla dignissim pellentesque. Maecenas rhoncus faucibus finibus. Mauris eget tincidunt metus. Morbi bibendum nunc sed nisl aliquam, sit amet lacinia lectus pharetra. Cras accumsan convallis risus. Morbi nisi libero, consequat eget leo vel, finibus rhoncus nulla. Mauris tempus risus quis efficitur sollicitudin. Suspendisse potenti. Quisque ut leo interdum ipsum tristique ultrices.</p>
        </div>
        <div class="tab-pane fade" id="fifth" role="tabpanel" aria-labelledby="fifth-tab">
          <h5 class="card-title">Fifth Tab header</h5>
          <p class="card-text">Nunc lacinia sodales ex, in mattis nulla eleifend in. Quisque molestie, dolor non egestas ornare, diam sapien accumsan erat, non malesuada nulla est ac purus. Donec pharetra molestie leo sit amet posuere. Etiam feugiat mi nisi, id semper neque dignissim ut. Praesent vitae accumsan eros. Curabitur a nisi non arcu suscipit rutrum at ut orci. Praesent nec eros eros. Quisque tempus neque ut nibh viverra, ut commodo dolor dapibus.</p>
        </div>
        <div class="tab-pane fade" id="sixth" role="tabpanel" aria-labelledby="sixth-tab">
          <h5 class="card-title">Sixth Tab header</h5>
          <p class="card-text">Proin ornare purus vitae magna viverra suscipit. Etiam rutrum lorem cursus libero scelerisque lacinia. Praesent bibendum risus id aliquam finibus. Donec sed orci sodales, viverra dolor a, dignissim mi. Pellentesque nec lectus enim. Suspendisse eu ligula ac tortor mollis lobortis. Nulla a laoreet neque, sit amet luctus dolor. Nam facilisis at odio ac commodo. Nullam vehicula blandit dui, vel suscipit orci.</p>
        </div>
      </div>
      <!-- END TABS DIV -->
      
    </div>
    <div class="card-footer p-0">
      <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
    </div>
  </div>
        </div>
      </div>

     <!-- Extra large modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <div class="modal-body">
              <table class="table table-dark">
                  <thead>
                    <tr>
                      <th>Product ID</th>
                      <th>MRP</th>
                      <th>Rates</th>
                      <th>inventory</th>
                      <th>Min Order quantity</th>
                      <th>Pincodes</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td id="prod_id"></td>
                      <td id="mrp"></td>
                      <td id="rates"></td>
                      <td id="inventory"></td>
                      <td id="moq"></td>
                      <td id="pincodes"></td>
                    </tr>
                  </tbody>
              </table>
             <!--  <div class="row">
                  <div class="col-md-2">
                    <b>Product ID:</b><p class="prod_id"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Mrp:</b><p class="mrp"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Rate:</b><p class="rate"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Inventory:</b><p class="inventory"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Min Order quantity:</b><p class="moq"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Pincodes:</b><p class="pincodes"></p>
                  </div>
                
              </div> -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>


@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap_tabs.css')}}" type="text/css">
 <style>
      .sellerRows > td { text-align: center; }
    </style>

@stop
@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/bootstrap-tabs.js')}}"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
      });
    </script>
@stop

