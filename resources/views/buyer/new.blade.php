@extends('adminlte::page')

@section('title', 'BLUMART | Buyer Registration')



@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Buyer Registration</h4>
        </div>
        <div class="col-md-2">
        </div>
        <div class="col-md-6 float-right">
        </div>
      </div>
<hr>
  <div class="card">
    <div class="card-header text-center">
      <b>General Details</b>
    </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <form action="/action_page.php" class="was-validated">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label for="buyer_name">Names:</label>
                  <input type="text" class="form-control" id="buyer_name" placeholder="Enter Buyer Name" name="buyer_name" required>
                </div>
              </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <label for="pwd">Mobile Number:</label>
                    <input type="text" class="form-control" id="buyer_mobile" placeholder="Enter Mobile Numbers" name="buyer_mobile" required>
                  </div>
                </div>
                 <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Company Name:</label>
                    <input type="text" class="form-control" id="company_name" placeholder="Enter Company Name" name="company_names" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Email Address:</label>
                    <input type="email" class="form-control" id="buyer_email" placeholder="Enter Email Address" name="buyer_email" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Whatsapp Number</label>
                    <input type="text" class="form-control" id="buyer_whatsapp" placeholder="Enter Whatsapp Number" name="buyer_whatsapp" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="company_name">Alternate Number</label>
                    <input type="text" class="form-control" id="buyer_whatsapp" placeholder="Enter Whatsapp Number" name="buyer_whatsapp" required>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <label for="buyer_address">Address :</label>
                <textarea id="buyer_address" class="form-control" placeholder="Enter Buyers Address" name="buyer_address" rows="3" cols="42" required></textarea>
                  </div>
                </div>
                <!-- <div class="col-md-4">
                  <div class="form-group form-check">
                    <label class="form-check-label">
                      <input class="form-check-input" type="checkbox" name="remember" required> I agree on blabla.
                      <div class="valid-feedback">Valid.</div>
                      <div class="invalid-feedback">Check this checkbox to continue.</div>
                    </label>
                  </div>
                </div> -->
            </div>  
          </div>
      </div>
        </div>
      </div>

    <div class="card">
        <div class="card-header text-center">
          <b>DOCUMENTS</b>
        </div>
    <div class="card-body">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="company_pan">Pan Card Number:</label>
                <input type="text" class="form-control" id="company_pancard" placeholder="Enter Pan Card Number" name="company_pancard" required>
                <div class="valid-feedback">Valid.</div>
                <div class="invalid-feedback">Please fill out this field.</div>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="form-group">
                 <label for="company_pan">Upload Pan Card:</label>
                 <input type="file" class="form-control" id="company_pancard_doc" placeholder="Upload Pan Card" name="company_pancard_doc" required>
              </div>
            </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="pwd">Aadhar Card Number:</label>
                  <input type="text" class="form-control" id="buyer_aadhar" placeholder="Enter Aadhar Number" name="buyer_aadhar" required>
                  <div class="valid-feedback">Valid.</div>
                  <div class="invalid-feedback">Please fill out this field.</div>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                 <label for="buyer_aadhar">Upload Aadhar Card:</label>
                 <input type="file" class="form-control" id="buyer_aadhar_doc" placeholder="Upload Aadhar Card" name="buyer_aadhar_doc" required>
              </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                  <label for="buyer_gst">GST Number:</label>
                  <input type="text" class="form-control" id="buyer_gst" placeholder="Enter GST Number" name="buyer_gst" required>
                  <div class="valid-feedback">Valid.</div>
                  <div class="invalid-feedback">Please fill out this field.</div>
                </div>
              </div>
              <div class="col-md-6">
              <div class="form-group">
                 <label for="buyer_aadhar">Upload GST Certificates:</label>
                 <input type="file" class="form-control" id="buyer_gst_doc" placeholder="Upload GST Certificate" name="buyer_gst_doc" required>
              </div>
            </div>

              <div class="col-md-6">
              <div class="form-group">
                 <label for="buyer_food_doc">Upload Food Licence:</label>
                 <input type="file" class="form-control" id="buyer_food_doc" placeholder="Upload Food Licence" name="buyer_food_doc" required>
              </div>
            </div>

          </div>  
        </div>
      </div>
        </div>
      </div>


      <div class="card">
        <div class="card-header text-center">
          <b>BEAT MAPPING</b>
        </div>
        <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="zone">Select Zone</label>
                  <select name="selectZone" class="form-control" id="zone">
                    @foreach ($zones as $eachRow => $zoneData)
                        <option value="{{ $zoneData->zone_code }}">{{ $zoneData->zone_name }}</option>
                    @endforeach
                    </select>
                </div>
              </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pswd" required>
                    <div class="valid-feedback">Valid.</div>
                    <div class="invalid-feedback">Please fill out this field.</div>
                  </div>
                </div>
            </div>  
          <button type="submit" class="btn btn-success">Submit</button>
        </form>
        </div>
      </div>
        </div>
      </div>


    </div>
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/buyer.js')}}"></script>

@stop

