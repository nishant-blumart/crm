@extends('adminlte::page')

@section('title', 'BLUMART | Orders List')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Order List</h4>
        </div>
        <div class="col-md-2">
          
        </div>
        <div class="col-md-6 float-right">
          <a href="{{ url('buyerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a>
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div>
        <hr>
      </div>
      <hr>
      
          
          <form name="delType" method="post" action="{{route('deliveryType')}}">
            @csrf
            <div class="row"> 
              <div class="col-md-4">
                <div class="form-group">
                  <label for="zone">Select Delivery Type</label>
                  <select name="deliveryType" class="form-control" id="deliveryType">
                    <option value="2">Accepted</option>
                    <option value="1">Other</option>
                  </select>
                </div>
              </div>
            
              <div class="col-md-4 mt-2">
                <div class="form-group">
                <label for="zone"></label>
                  <input type="submit" class="btn btn-primary form-control" value="Submit"/>
                </div>
              </div>
              </div>
          </form>
      <hr>

      @if(!empty($orderList))
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">Date</th>
                        <th class="text-left">Buyer Name</th>
                        <th class="text-left">Total Products</th>
                        <th class="text-left">Total Amount</th>
                        <th class="text-left">Sub Orders</th>
                        <th class="text-left">Expected Delivery</th>
                        <th class="text-left">Payment Type</th>
                        <th class="text-left">Comission</th>
                        <th class="text-left">Placed By</th>
                        <th class="text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($orderList as $eachRow => $order)
                    <tr class="sellerRows" id="{{$order->po_id}}" >
                      <td>{{$order->po_id}}</td>
                      <td>{{$order->createdAt}}</td>
                      <td>
                        <a href="{{ url('/buyer', [$order->buyer_user_id]) }}" target="_blank" class="viewBuyer">{{$order->name}}</a>
                      </td>
                      <td>{{$order->total_prd}}</td>
                      <td>{{$order->po_total}}</td>
                      <td>
                      @foreach(explode(',', $order->child_po_id) as $echChildPo)
                      <a href="{{ url('/order', [$echChildPo]) }}" target="_blank" class="viewOrder">{{$echChildPo}}</a><br>
                      @endforeach
                     </td>
                      <td>{{$order->exp_del_date}}</td>
                      <td>{{$order->payment_method}}</td>
                      <td>{{$order->commission_amt}}</td>
                      <td>{{$order->ba_name}}</td>
                      <td>
                         <a href="{{ url('/order', [$order->po_id]) }}" target="_blank" class="viewOrder"><i class="fa fa-eye text-blue" title="View Order"></i></a>
                         @foreach(explode(',', $order->child_po_id) as $echChildPo)
                      <a href="{{ url('/invoice', [$echChildPo,'0']) }}" target="_blank" class="viewOrder"><i class="fa fa-file text-green" title="{{$echChildPo}}"></i></a>
                      @endforeach
                         
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>
      </div>    
      
      @endif      
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      .sellerRows > td { text-align: left; }

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    
    <script type="text/javascript">
      $('#deliveryType').on('change',function(){
        var deliveryType = $(this).val();
        $.ajax({
          url: "/delivery/",
          type: "post",
          data:{deliveryType:deliveryType},
          cache: false,
          success: function(data){
            }
    });

        
      });

    
      </script>
      <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
  });
</script>

@stop

