@extends('adminlte::page')

<title>BLUMART | {{ $delivery->invoice_id }}</title>

@section('content')
<!DOCTYPE html>
<html>
      <div class="row">
        <!-- <div class="col-md-6">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Invoice</h4>
        </div> -->
        <div class="col-md-2">
        </div>
        <div class="col-md-4 float-right">
        </div>
      </div>

            <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Tax Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>

      </div><!-- /.container-fluid -->
    </section>

    <section class="content" >
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="invoice p-3 mb-3" style="border: 1px solid black;">
              <div class="row p-2">
                <div class="col-9 text-center">
                   <h2 class="pl-5">Tax Invoice</h2>
                </div>
                <div class="col-3">
                    <img class="" src="{{asset('img/blumart-logo.png')}}">
                </div>               
              </div>
              <hr>
              <div class="row">
                <div class="invoiceBoxes" style="border-right: 1px solid black; margin-left: 20px;">
                  <b>Invoice No :</b><br> {{$delivery->invoice_id}}
                </div>
                <div class="invoiceBoxes" style="border-right: 1px solid black;">
                  <b>Invoice Date :</b><br> {{ date('d-m-Y', strtotime($delivery->del_date)) }}
                </div>
                <div class="invoiceBoxes" style="border-right: 1px solid black;">
                  <b>Seller Reference :</b><br> <!-- {{$delivery->ref_id}} -->
                </div>
                <div class="invoiceBoxes" style="border-right: 1px solid black;">
                  <b>Buyers PO No :</b><br> {{$parentOrder->po_id}}
                </div>
                <div class="invoiceBoxes" style="border-right: 1px solid black;">
                   <b>PO Date :</b><br> {{ date('d-m-Y', strtotime($parentOrder->createdAt)) }}
                </div>
                <div class="invoiceBoxes" style="border-right: 1px solid black;">
                   <b>Delivery Note :</b><br> {{$delivery->comment}}
                </div>
                 <div class="invoiceBoxes">
                   <b>Vehicle No :</b>
                </div>
            </div>

              <hr>
              <!-- info row -->
              <div class="row invoice-info">
                @if($supplierStatus == 1)
                <div class="col-sm-4 invoice-col" style="border-right: 1px solid black;">
                  <p class="mb-2"><b>From :</b></p>
                  <address>
                    <b>{{$from->name}}</b><br>
                    <b>Address :</b> {{$from->address}}<br>
                    <b>CIN : </b>{{$from->cin}}<br>
                    <b>PAN : </b> {{$from->pan}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>GSTN : </b> {{$from->gst}}<br>
                    <b>Phone :</b> {{$from->mobile}}<br>
                    <b> Email : </b>{{$from->email}}
                    
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col" style="border-right: 1px solid black;">
                  <p class="mb-2"><b>To :</b></p>
                  <address>
                    <b>{{$buyerDetails->comp_name}}</b><br>
                   <b>Address : </b>{{$buyerDetails->Addr_1}}&nbsp;{{$buyerDetails->Addr_2}},
                    {{$buyerDetails->area_name}}<br>
                    {{$buyerDetails->city_name}},{{$buyerDetails->state_name}},{{$buyerDetails->pincode}}<br>
                    <b>PAN : </b>{{$buyerDetails->pan_no}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>GST : </b>{{$buyerDetails->gst_no}}<br>
                    <b>Phone :</b> {{$buyerDetails->mobile}}<br>
                    <b> Email : </b>{{$buyerDetails->email}}
                  </address>
                </div>
                
                <div class="col-sm-4 invoice-col supplierBy">
                  <p class="mb-2"><b>Supplied By :</b></p>
                  <address>
                    <b>{{$sellerDetails->comp_name}}</b><br>
                    <b>Address :</b> {{$sellerDetails->Addr_1}}<br>
                    {{$sellerDetails->Addr_2}},{{$sellerDetails->area_name}}<br>
                    {{$sellerDetails->city_name}},{{$sellerDetails->state_name}},{{$sellerDetails->pincode}}<br>
                    <b>PAN : </b>{{$sellerDetails->pan_no}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>GST :</b> {{$sellerDetails->gst_no}}<br>
                    
                    <b>Phone :</b> {{$sellerDetails->mobile}}<br>
                    <b>Email :</b> {{$sellerDetails->email}}<br>
                  </address>
                </div>
                @else
                 <div class="col-sm-6 invoice-col" style="border-right: 1px solid black;">
                  <p class="mb-2"><b>From :</b></p>
                  <address>
                    <b>{{$from->name}}</b><br>
                    <b>Address :</b> {{$from->address}}<br>
                    <b>CIN : </b>{{$from->cin}}<br>
                    <b>PAN : </b> {{$from->pan}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>GSTN : </b> {{$from->gst}}<br>
                    <b>Phone : </b>{{$from->mobile}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>Email : </b>{{$from->email}}
                    
                    
                  </address>
                </div>
                <!-- /.col -->
                <div class="col-sm-6 invoice-col">
                  <p class="mb-2"><b>To :</b></p>
                  <address>
                    <b>{{$buyerDetails->comp_name}}</b><br>
                    
                   <b>Address : </b>{{$buyerDetails->Addr_1}}{{$buyerDetails->Addr_2}}<br>
                    <b>Area: </b>{{$buyerDetails->area_name}}&nbsp;&nbsp;
                    <b>City: </b>{{$buyerDetails->city_name}}<br><b>State: </b>{{$buyerDetails->state_name}},&nbsp;&nbsp;<b>Pincode: </b>{{$buyerDetails->pincode}}<br>
                    <b>PAN : </b>{{$buyerDetails->pan_no}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b>GST : </b>{{$buyerDetails->gst_no}}<br>
                   <b>Phone :</b> {{$buyerDetails->mobile}}&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;<b> Email : </b>{{$buyerDetails->email}}
                  </address>
                </div>
                @endif
              </div>
              
              <br>
              <div class="row">
                <div class="col-12 table-responsive">
                  <table class="table table-striped">
                    <thead>
                    <tr>
                      <th>Sr.</th>
                      <th>Particulars</th>
                      <th>HSN</th>
                      <th>UOM</th>
                      <th>Qty</th>
                      <th>Rate</th>
                      <th>Base Amount</th>
                       @if($gstStatus ==1)
                      <th>CGST %</th>
                      <th>SGST %</th>
                      @else
                        <th>IGST %</th>
                      @endif
                      <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>
                        
                      @foreach($buyerOrder as $eachRow => $eachProduct)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$eachProduct->product_name}}</td>
                        <td>{{$eachProduct->hsn}}</td>
                        <td>{{$eachProduct->uom}}</td>
                        @if($orderStatus == 2)
                          <td>{{$eachProduct->approved_qty}}</td>
                          <td>{{$eachProduct->rate}}</td>
                          <td>@php echo $eachProduct->approved_qty*$eachProduct->rate @endphp </td>
                          @if($gstStatus ==1)
                             <td>@php echo $cgst = round((($eachProduct->approved_qty*$eachProduct->rate)*$eachProduct->cgst_rate)/100 ,2); echo "(".$eachProduct->cgst_rate."%)" @endphp</td>
                             <td>@php echo $sgst =round((($eachProduct->approved_qty*$eachProduct->rate)*$eachProduct->sgst_rate)/100,2); echo "(".$eachProduct->cgst_rate."%)" @endphp</td>
                            <td> @php echo round(($eachProduct->approved_qty*$eachProduct->rate)+($cgst+$sgst),2) @endphp</td>
                            @else
                              <td>@php echo $igst = round((($eachProduct->approved_qty*$eachProduct->rate)*($eachProduct->cgst_rate+$eachProduct->sgst_rate))/100 ,2); echo "(".($eachProduct->cgst_rate+$eachProduct->sgst_rate)."%)" @endphp</td>
                              <td> @php echo round(($eachProduct->approved_qty*$eachProduct->rate)+($igst),2) @endphp</td>
                            @endif

                        @elseif($orderStatus == 3)
                              <td>{{$eachProduct->approved_qty}}</td>
                              <td>{{$eachProduct->buyer_rate}}</td>
                              <td>@php echo $eachProduct->approved_qty*$eachProduct->buyer_rate @endphp </td>
                              @if($gstStatus ==1)
                              <td>{{$eachProduct->b_cgst_amt}} ({{$eachProduct->cgst_rate}}%)</td>
                              <td>{{$eachProduct->b_sgst_amt}} ({{$eachProduct->sgst_rate}}%)</td>
                              @else
                              <td>{{$eachProduct->b_cgst_amt+$eachProduct->b_cgst_amt}} ({{$eachProduct->cgst_rate+$eachProduct->sgst_rate}}%)</td>
                              @endif
                              <td>{{$eachProduct->b_rate}}</td>
                        @else
                        <td>{{$eachProduct->qty}}</td>
                        <td>{{$eachProduct->rate}}</td>
                            @if($gstStatus ==1)
                              <td>@php echo $eachProduct->item_total-(2*$eachProduct->cgst_amt) @endphp </td>
                              <td>{{$eachProduct->cgst_amt}}({{$eachProduct->cgst_rate}}%)</td>
                              <td>{{$eachProduct->sgst_amt}}({{$eachProduct->sgst_rate}}%)</td>
                            @else
                              <td>@php echo $eachProduct->item_total-(2*$eachProduct->cgst_amt) @endphp </td>
                              <td>{{$eachProduct->cgst_amt+$eachProduct->sgst_amt}}({{$eachProduct->cgst_rate+$eachProduct->sgst_rate}}%)</td>
                            @endif
                        <td> {{$eachProduct->item_total}}</td>
                        @endif
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>

               <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>Products</th>
                          <th>Base Amount</th>
                          @if($gstStatus ==1)
                          <th>CGST</th>
                          <th>SGST</th>
                          @else
                          <th>IGST</th>
                          @endif
                          <th>Round Off</th>
                          <th>Total</th>
                          <th>Amount In Words</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>{{$delivery->total_prd}}</td>
                          <td>{{$delivery->b_baseamt}}</td>
                          @if($gstStatus ==1)
                            <td>{{$delivery->b_cgst_amt}}</td>
                            <td>{{$delivery->b_cgst_amt}}</td>
                            <td>{{$roundOff}}</td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i> {{round($delivery->b_baseamt+$delivery->b_sgst_amt+$delivery->b_cgst_amt, 0)}}</td>
                            <td>{{ucfirst($inWords)}}</td>
                          @else
                            <td>{{$delivery->b_cgst_amt+$delivery->b_sgst_amt}}</td>
                            <td>{{$roundOff}}</td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i> {{round($delivery->b_baseamt+$delivery->b_sgst_amt+$delivery->b_cgst_amt, 0)}}</td>
                            <td>{{ucfirst($inWords)}}</td>
                          @endif
                          
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
          <hr>
              <div class="row">
                <div class="col-6 mb-4" style="border-right: 1px solid black;">
                  <p><b>Declaration : </b></p>
                  <ol style="list-style-type: inherit;">
                    <li>Company's PAN : <b>AAFCI5469R</b></li>
                    <li>We Declare that this invoice shows the actual price of the services described and that all particulars are true and correct.</li>
                    <li>This is Computer generated Invoice. It does not require Signature.</li>                    
                    <!-- <li>This is Computer generated Invoice. it does not require Signature.</li> -->
                  </ol>

                  <p><b>Terms and Conditions : </b></p>
                  <ol style="list-style-type: inherit;">
                    <!-- <li>Arvind sir's terms and Conditions will come here.arvind sir's terms and Conditions will come here</li> -->
                    <li>Subject to Mumbai Jurisdiction.</li>  
                  </ol>

                </div>
                <div class="col-6">
                  <div class="border border-secondary" style="height:270px;width:450px;padding:5px;border-radius:20px;">
                  <div clas="izyskStampUpper"style="position: relative;margin-left: 220px;margin-top: 3px;" >
                    <p style="float: right;position: absolute;">For I-ZYSK Solutions Pvt. Ltd</p>
                     <p class="izyskStamp" style="float: right;margin-top:100px;position: relative;">Authorised Signatory</p>
                  </div>
                </div> 
                </div>
                <p style="margin-bottom: 0px;margin-left: 20px;"><b>E. & O.E.</b></p>   
            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

<section class="content" style="margin-top: 40px;" >
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
                <div class="div2">
                  <p style="margin-left: 10px;margin-top: 10px;margin-bottom: 30px;"><b>Payment Received Details : </b></p>
                  <div class="row">
                    <div class="amountDetails">
                      <p><b>Amount : __________________</b></p>
                    </div>
                    <div class="paymentDetails">
                       <p><b>Mode : _______________________</b></p>
                    </div>
                     <div class="paymentDetails">
                       <p><b>Date : ______________________</b></p>
                    </div>
                    <div class="paymentDetails">
                       <p><b>Receiver's Signature : _____________________</b></p>
                    </div>
                  </div>
                </div>
          </div>
        </div>
      </div>
</section>        

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

<!-- jQuery -->
<script src="../../plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../../plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>       
@stop

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
      hr{
        border-top: 1px solid black;
      }
      .invoiceColumn{
        padding-bottom: 10px;
        border-bottom: 1px solid rgba(0,0,0,.1);
      }
      address{
        line-height: 1.7;
      }
      h2{
        font-weight: 900;
      }
      .content-wrapper>.content{
        padding: 0 !important;
        margin-left: 30px;
        margin-right: 0px;
      }
      .container-fluid{
        font-weight: 625;
        padding: 0 !important;
      }
      table.text{
        font-weight: 700;
      }
      th {
      font-weight: 800;
    }

    td {
      font-weight: 700;
    }
    .col-2{
      max-width: 14% !important;
    }
    .div2 {
      height: 120px;
      border: 1px solid black;
    }
    .amountDetails{
      margin-left: 23px;
      margin-right: 10px;
    }
    .paymentDetails{
      margin-right: 10px;
    }
    .invoiceBoxes{
      margin-right: 20px;
      padding-right: 10px;
    }
    

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/subscription.js')}}"></script>

@stop

