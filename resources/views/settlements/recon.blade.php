@extends('adminlte::page')

@section('title', 'BLUMART | Settlement List')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Settlement List</h4>
        </div>
        <div class="col-md-4">
          <form action="{{url('subscription/settlement/recon')}}"  method="post">
            {{ csrf_field() }}
            <label for="reconDate">Search:</label>
              <input type="date" id="reconDate" name="reconDate">
              <input type="submit" class="btn btn-success" value="Get">
               <a href="#" class="btn btn-danger">Reset</a>
           </form>
        </div>
        <div class="col-md-5 float-right">
          <a href="{{ url('buyerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a>
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div>
        <hr>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        <th class="text-left">Company</th>
                        <th class="text-left">UTR</th>
                        <th class="text-left">Amount</th>
                        <th class="text-left">Credit</th>
                        <th class="text-left">Fees</th>
                        <th class="text-left">Charges(%)</th>
                        <th class="text-left">Status</th>
                        <th class="text-left">Created At</th>
                        <th class="text-left">Settled At</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($settlementList as $eachSettlement)
                    <tr class="sellerRows" id="{{$eachSettlement['settlement_id']}}" >
                      <td>{{$eachSettlement['settlement_id']}}</td>
                      <td>{{$eachSettlement['comp_name']}}</td>
                      <td>{{$eachSettlement['settlement_utr']}}</td>
                      <td>@php echo $eachSettlement['amount']/100 @endphp</td>
                      <td>@php echo $eachSettlement['credit']/100 @endphp</td>
                      <td>@php echo $eachSettlement['fee']/100 @endphp</td>
                      <td>@php echo ($eachSettlement->fee/$eachSettlement->amount)*100  @endphp %</td>
                      <td>{{$eachSettlement['settled']}}</td>
                      <td><?php echo date('d-m-Y', $eachSettlement['created_at']);?></td>
                      <td><?php echo date('d-m-Y', $eachSettlement['settled_at']);?></td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>

      </div>          
    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      .sellerRows > td { text-align: left; }

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    
    <!-- <script type="text/javascript">
      $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('sellerList') }}",
        columns: [
            {data: 'user_id', name: 'Sr.No'},
            {data: 'name', name: 'name'},
            {data: 'comp_name', name: 'comp_name'},
            {data: 'mobile', name: 'mobile'},
            {data: 'email', name: 'email'},

            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]
    });
    
      });
      </script> -->
      <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
  });
</script>

@stop

