<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <style>
  
  </style>
</head>
<body>
  
<div class="container">
    <div class="card">
      <div class="row">
        <div class="col-4 text-center">
          <img src="{{asset('img/blumart-logo.png')}}" style="width: 50%;margin-top: 10px;" placeholder="Blumart-logo">
        </div>
        <div class="col-8">
          
        </div>
      </div>

      <div class="row text-center">
        <div class="col-md-12 text-center" style="height: 30px;background-color: orange;color: #FFF;">
          <h3 class="pt-2"><b>Enjoy 48 DAYS Credit</b></h3>
        </div>
        <div class="col-md-12">
          <img src="{{asset('img/ICICI_Bank-01.png')}}" class="image-fluid" style="width: 50%;margin-top: 10px;" placeholder="Blumart-logo">
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-12 text-center">
          <h3 class="card-heade text-center" style="text-decoration: underline;margin-bottom: 25px;">Document Upload Form</h3>
      </div>
    </div>
     
      <div class="card-body">
        <form name="lodForm" method="post" enctype="multipart/form-data" action="{{route('lodSave')}}">
          @csrf
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">BA Name</label>
                <select name="ba_name" class="form-control" required>
                  <option value="0">Select</option>
                  <option value="107">Dilip Babu</option>
                  <option value="99">Pawan Tiwari</option>
                  <option value="98">Ajay Vishwakarma</option>
                  <option value="104">Chakravarti Sharma</option>
                  <option value="97">Raees Sheikh</option>
                  <option value="80">Deepak Agale</option>
                  <option value="105">Prakash Arora</option>
                  <option value="55">Rohit Pande</option>
                  <option value="58">Satyam Mishra</option>
                  <option value="112">Anil Kumar</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Customer Name</label><span style="color:red;">{{$errors->first('name')}}</span>
                <input type="text" class="form-control" value="{{old('customer_name')}}" id="customer_name" name="customer_name" placeholder="Enter Customer Full Name Here" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Business Name</label><span style="color:red;">{{$errors->first('comp_name')}}</span>
                <input type="text" class="form-control" id="comp_name" value="{{old('comp_name')}}" name="comp_name" placeholder="Enter Company Name Here" required>
              </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Mobile</label><span style="color:red;">{{$errors->first('mobile')}}</span>
                  <input type="number" class="form-control" id="mobile" value="{{old('mobile')}}" name="mobile" placeholder="Enter Your Mobile Number" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email Address</label><span style="color:red;">{{$errors->first('email')}}</span>
                  <input type="email" class="form-control" id="email" value="{{old('email')}}" name="email" placeholder="Enter Your Email Address">
              </div>
            </div>
            <div class="col-md-6">
                 <div class="form-group">
                  <label for="exampleInputPassword1">Address</label><span style="color:red;">{{$errors->first('address')}}</span>
                  <input type="text" class="form-control" id="address" value="{{old('address')}}" name="address" placeholder="Enter Your Address">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputPassword1">PAN No</label><span style="color:red;">{{$errors->first('pan_no')}}</span>
                <input type="text" class="form-control" id="pan_no" value="{{old('pan_no')}}" name="pan_no" placeholder="Enter Your Pan No">
            </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputPassword1">Aadhar No</label><span style="color:red;">{{$errors->first('aadhar_no')}}</span>
                <input type="number" class="form-control" id="aadhar_no" value="{{old('aadhar_no')}}" name="aadhar_no" placeholder="Enter Your Aadhar No">
            </div>
            </div>
          </div> 

          <div class="panel panel-default">
            <h4 class="panel-body text-center">DOCUMENTS TO BE UPLOADED</h4>
            <hr>
            <div class="row">
              <div class="col-md-4 px-1">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Customer Photograph</label><span style="color:red;">{{$errors->first('pan_doc')}}</span>
                    <input type="file" class="form-control" id="customer_pic" name="customer_pic" accept="image/*" required>
                </div>
              </div>
                <div class="col-md-4 px-1">
                  <div class="form-group">
                    <label for="exampleInputPassword1">PANCARD</label><span style="color:red;">{{$errors->first('pan_doc')}}</span>
                    <input type="file" class="form-control" id="pan_doc" name="pan_doc" accept="image/*" required>
                </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputPassword1">AADHAR CARD</label><span style="color:red;">{{$errors->first('aadhar_doc')}}</span>
                    <input type="file" class="form-control" id="aadhar_doc" accept="image/*" name="aadhar_doc">
                </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputPassword1">GST CERTIFICATE</label><span style="color:red;">{{$errors->first('gst_doc')}}</span>
                    <input type="file" class="form-control" id="gst_doc" accept="image/*" name="gst_doc">
                </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputPassword1">SHOP ESTABLISHMENT</label><span style="color:red;">{{$errors->first('shop_doc')}}</span>
                    <input type="file" class="form-control" id="shop_doc" accept="image/*" name="shop_doc">
                </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputPassword1">ITR 2018-2019</label><span style="color:red;">{{$errors->first('itr1_doc')}}</span>
                    <input type="file" class="form-control" id="itr1_doc" accept="image/*" name="itr1_doc" multiple="multiple" required>
                </div>
              </div>
              <div class="col-md-4">
                  <div class="form-group">
                    <label for="exampleInputPassword1">ITR 2019-2020</label><span style="color:red;">{{$errors->first('itr2_doc')}}</span>
                    <input type="file" class="form-control" id="itr2_doc" accept="image/*" name="itr2_doc" multiple="multiple" required>
                </div>
              </div>
            </div>
          </div>
          <div class="row text-center">
            <div class="col-12">
            <button type="submit" class="btn btn-primary text-center mb-2">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>


</div>

</body>
</html>
