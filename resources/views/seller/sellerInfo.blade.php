@extends('adminlte::page')

@section('title', 'BLUMART | Seller Information')

@section('content')
<meta name="_token" content="{{ csrf_token() }}" />


    <div class="flash-message">
    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
      @if(Session::has('alert-' . $msg))

      <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
      @endif
    @endforeach
  </div>

      <div class="row">
        <div class="col-12">
               <div class="card">
    <div class="card-header">
      
      <div class="d-flex">
        <div class="title">
         <h6>Seller Name : {{$sellerDetails->name}} ({{$sellerId}})</h6>
          <!-- <p class="mb-4">Created with &#x1F9E1; by <a href="https://elmah.io/?utm_source=codepen&utm_medium=social&utm_campaign=codepen" target="_blank">elmah.io</a> team.</p> -->
        </div>
        <div class="ml-auto">
          <a class="text-dark" href="https://elmah.io/?utm_source=codepen&utm_medium=social&utm_campaign=codepen" target="_blank">
            <div class="elmahio-ad d-flex">
              <div class="logo">
                <img src="https://elmah.io/images/logo.png" />
              </div>
              <div class="motto d-none d-sm-block px-2"><a href="{{ url('sellerExport') }}" class="export btn btn-success btn-sm float-right">Export</a></div>
            </div>
          </a>
        </div> 
      </div>
      
      <!-- START TABS DIV -->
      <div class="tabbable-responsive">
        <div class="tabbable">
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="first-tab" data-toggle="tab" href="#first" role="tab" aria-controls="first" aria-selected="true">Basic Info</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="second-tab" data-toggle="tab" href="#second" role="tab" aria-controls="second" aria-selected="false">Documents</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="third-tab" data-toggle="tab" href="#third" role="tab" aria-controls="third" aria-selected="false">Products</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="fourth-tab" data-toggle="tab" href="#fourth" role="tab" aria-controls="fourth" aria-selected="false">Orders</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="fifth-tab" data-toggle="tab" href="#fifth" role="tab" aria-controls="fifth" aria-selected="false">Ownership</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="sixth-tab" data-toggle="tab" href="#sixth" role="tab" aria-controls="sixth" aria-selected="false">Subscriptions</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="card-body">
      
      <div class="tab-content">
        <div class="tab-pane fade show active" id="first" role="tabpanel" aria-labelledby="first-tab">
          <div class="row">
              <div class="col-md-4">
                <b>NAME</b> : {{$sellerDetails->name}}
              </div>
              <div class="col-md-4">
                <b>Company NAME</b> : {{$sellerDetails->comp_name}}
              </div>
              <div class="col-md-4">
                <b>Mobile</b> : {{$sellerDetails->mobile}}
              </div>
          </div> 
              
           <hr>
           <div class="row">    
              <div class="col-md-4">
                <b>GST No.</b> : {{$sellerDetails->gst_no}} 
                <a href="#" data-id="{{$sellerDetails->gst_no}}" id="verifyGst" data-toggle="modal" data-target=".gst_details" class="btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-check pr-2" aria-hidden="true"></i>Verify</a>
              </div>
              <div class="col-md-4">
                <b>Aadhar No.</b> : {{$sellerDetails->aadhar_no}}
              </div>
              <div class="col-md-4">
                <b>Pancard No.</b> : {{$sellerDetails->pan_no}}
              </div>
          </div>
          <hr>
           <div class="row">
               <div class="col-md-4">
                <b>Alternate Mob</b> : {{$sellerDetails->altr_mob_no}}
              </div>
              <div class="col-md-4">
                <b>Whatsapp</b> : {{$sellerDetails->whatsapp_mob_no}}
              </div>
              <div class="col-md-4">
                <b>Email</b> : {{$sellerDetails->email}}
              </div>
          </div>

           <hr>
           <div class="row">    
              <div class="col-md-8">
                <b>Address</b> : {{$sellerDetails->Addr_1}}
              </div>
              <div class="col-md-4">
                <b>Pincode</b> : {{$sellerDetails->ss_pincode}}
              </div>
          </div>
          <hr>
           <div class="row">    
              <div class="col-md-4">
                <b>State</b> : {{$sellerDetails->state_name}}
              </div>
              <div class="col-md-4">
                <b>City</b> : {{$sellerDetails->city_name}}
              </div>
              <div class="col-md-4">
                <b>Area</b> : {{$sellerDetails->area_name}}
              </div>
          </div>
          <hr>
           <div class="row">    
              <div class="col-md-4">
                <b>Bank Name : </b> : {{$sellerDetails->bank_name}}
              </div>
              <div class="col-md-4">
                <b>Bank IFSC : </b> : {{$sellerDetails->bank_ifsc}}
              </div>
              <div class="col-md-4">
                <b>A/C No : </b> : {{$sellerDetails->acct_no}}
              </div>
          </div>
          <hr>
           <div class="row">    
               <div class="col-md-4 py-3 ">
                @if($sellerDetails->is_active == 1)         
                      <div> <b>Status</b> :<img src="{{asset('img/active-user.png')}}" style="width: 7%;" class="ml-3 mb-1"></div>
                @else
                     <div> <b>Status</b> :<img src="{{asset('img/inactive-user.png')}}" style="width: 7%;" class="ml-3 mb-1"></div>       
                @endif
              </div>
              <div class="col-md-4">
                <b>Seller ID :</b> <span id="sellerId">{{$sellerDetails->user_id}}</span>
              </div>
              <div class="col-md-4">
                <b>Created At :</b> <span id="sellerId">{{$sellerDetails->createdAt}}</span>
              </div>
          </div>


        </div>
        <div class="tab-pane fade" id="second" role="tabpanel" aria-labelledby="second-tab">
          <h5 class="card-title">Second Tab header</h5>
          <p class="card-text">In hac habitasse platea dictumst. Cras sit amet massa fermentum risus eleifend malesuada vel nec erat. Cras massa tellus, volutpat efficitur feugiat eu, accumsan vel felis. Nullam ornare tellus eu dolor rhoncus, ut tempor lectus tincidunt. Ut in condimentum lectus. Praesent non pretium mauris, efficitur condimentum ex. Nam ante lorem, eleifend in egestas a, rhoncus at ex.</p>
        </div>

        <div class="tab-pane fade" id="third" role="tabpanel" aria-labelledby="third-tab">
          <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="">#</th>
                        
                        <th class="">Product Name</th>
                        <th class="">HSN</th>
                        <th class="">CGST</th>
                        <th class="">SGST</th>
                        <th class="">MRP</th>
                        <!-- <th class="text-center">Pincodes</th> -->
                        <th class="">View Rates</th>

                    </tr>
                </thead>
                <tbody>
                  @foreach($sellerProducts as $eachRow => $eachProduct)
                    <tr class="sellerRows" id="{{$eachProduct->prod_id}}" >
                      <td>{{$eachProduct->prod_id}}</td>
                      <td>{{$eachProduct->product_name}}</td>
                      <td>{{$eachProduct->hsn_no}}</td>
                      <td>{{$eachProduct->gst}}</td>
                      <td>{{$eachProduct->sgst}}</td>
                      <td>{{$eachProduct->mrp}}</td>
                      <!-- <td>{{$eachProduct->approved_pin}}</td> -->
                      <td>
                        <a href="" data-toggle="modal" data-target="#myModal"  class="viewProduct btn btn-success btn-sm">View</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>

      </div>
        </div>

        <div class="tab-pane fade" id="fourth" role="tabpanel" aria-labelledby="fourth-tab">
          <div class="col-md-12 mt-10">
            <table class="table table-hover" id="order-table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">PO Date</th>
                        <th class="text-center">Total Items</th>
                        <th class="text-center">Total Amount</th>
                        <th class="text-center">CGST</th>
                        <th class="text-center">SGST</th>
                        <th class="text-center">Commission</th>
                        <th class="text-center">Created By</th>
                        <th class="text-center">View Rates</th>

                    </tr>
                </thead>
                <tbody>
                  @foreach($sellerPoDetails as $eachRow => $eachOrder)
                    <tr class="sellerRows" id="{{$eachOrder->po_id}}" >
                      <td>{{$eachOrder->po_id}}</td>
                      <td>{{$eachOrder->createdAt}}</td>
                      <td>{{$eachOrder->total_prd}}</td>
                      <td>{{$eachOrder->po_total}}</td>
                      <td>{{$eachOrder->cgst_amt}}</td>
                      <td>{{$eachOrder->cgst_amt}}</td>
                     <td>{{$eachOrder->commission_amt}}</td>
                     <td>{{$eachOrder->created_by}}</td>
                      <td>
                        <a href="" data-toggle="modal" data-target="#order"  class="viewOrder btn btn-success btn-sm">View</a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
        </div>

        <div class="tab-pane fade" id="fifth" role="tabpanel" aria-labelledby="fifth-tab">
          <h5 class="card-title">Fifth Tab header</h5>
          <p class="card-text">Nunc lacinia sodales ex, in mattis nulla eleifend in. Quisque molestie, dolor non egestas ornare, diam sapien accumsan erat, non malesuada nulla est ac purus. Donec pharetra molestie leo sit amet posuere. Etiam feugiat mi nisi, id semper neque dignissim ut. Praesent vitae accumsan eros. Curabitur a nisi non arcu suscipit rutrum at ut orci. Praesent nec eros eros. Quisque tempus neque ut nibh viverra, ut commodo dolor dapibus.</p>
        </div>

        <div class="tab-pane fade" id="sixth" role="tabpanel" aria-labelledby="sixth-tab">
             <div class="tab-pane fade show active" id="first" role="tabpanel" aria-labelledby="first-tab">
          <div class="row">
            <div class="col-md-4">
                <b>Subscription Status</b> : 
                @if($subscription == '')
                      <b style="color:red;">Inactive</b>
                  @else
                      <b style="color:green;">Active</b>
                  @endif
              </div>
              <div class="col-md-4">
                <b>Pay Subscription</b> : 
               @if($subscription == '')
                      <button type="button" data-id="{{$sellerDetails->user_id}}" data-mobile="{{$sellerDetails->mobile}}" id="SubscriptionBtn"  class="btn btn-outline-success">Autopay</button>
                  @else
                      <button type="button" data-id="{{$sellerDetails->user_id}}" data-mobile="{{$sellerDetails->mobile}}" id="SubscriptionBtn" class="btn btn-outline-success" disabled>Autopay</button>
                  @endif
                
              </div>
              <div class="col-md-4">
                <b>Send Payment Link</b> :
                @if($subscription == '')
                      <button type="button" data-id="{{$sellerDetails->user_id}}" id="paymentLinkBtn" class="btn btn-outline-info" disabled>Send</button>
                  @else
                      <button type="button" data-id="{{$sellerDetails->user_id}}" id="paymentLinkBtn" class="btn btn-outline-info">Send</button>
                  @endif
                
              </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-4">
                <b>Cancel Subscription</b> :
                @if($subscription == '')
                      <button type="button" data-id="{{$sellerId}}" id="cancelBtn" class="btn btn-outline-danger" disabled>Cancel</button>
                  @else
                      <button type="button" data-id="{{$sellerId}}" data-subscr="{{$sellerSubscriptionInvoices[0]['subscription_id']}}" id="cancelBtn" class="btn btn-outline-danger">Cancel</button>
                  @endif
              </div>
            <div class="col-md-4">
                <b>Pause Subscription</b> :
                @if($subscription == '')
                      <button type="button" data-id="{{$sellerDetails->user_id}}" id="pauseBtn" class="btn btn-outline-info" disabled>Pause</button>
                  @else
                      <button type="button" data-id="{{$sellerDetails->user_id}}" id="pauseBtn" class="btn btn-outline-info">Pause</button>
                  @endif
               
            </div>
            <div class="col-md-4">
                <b>Resume Subscription</b> :
                @if($subscription == '')
                      <button type="button" data-id="{{$sellerDetails->user_id}}" id="resumeBtn" class="btn btn-outline-success">Resume</button>
                  @else
                      <button type="button" data-id="{{$sellerDetails->user_id}}" id="resumeBtn" class="btn btn-outline-success" disabled>Resume</button>
                  @endif
                
            </div>
          </div> 
           <hr>
           <div class="row">
             <div class="col-4">
               @empty($sellerSubscriptionInvoices)
                <b>Subscription ID :</b>
                @else
                <b>Subscription ID :</b> {{$sellerSubscriptionInvoices[0]['subscription_id']}}
                @endempty
             </div>
           </div>
         </div>
         <hr>
                <table class="table table-hover" id="subscription-table">
                <thead>
                    <tr>
                        <th class="text-center">Invoice Id</th>
                        <th class="text-center">Payment Id</th>
                        <th class="text-center">Order Id</th>
                        <th class="text-center">Amount</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Paid At</th>
                        <th class="text-center">URL</th>
                        <th class="text-center">View Invoice</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($sellerSubscriptionInvoices as $eachRow => $eachOrder)
                    <tr class="sellerRows" id="{{$eachOrder['id']}}" >
                      <td>{{$eachOrder['id']}}</td>
                      <td>{{$eachOrder['payment_id']}}</td>
                      <td>{{$eachOrder['order_id']}}</td>
                      <td>@php echo $eachOrder['amount']/100 @endphp</td>
                      <td>{{$eachOrder['status']}}</td>
                      @if($eachOrder['status'] == 'paid')
                      <td><?php echo date('d-m-Y', $eachOrder['paid_at']);?></td>
                      @else
                      <td><?php echo 'NA';?></td>
                      @endif
                     <td>{{$eachOrder['short_url']}}</td>
                     @if($eachOrder['status'] == 'paid')
                      <td>
                        <!-- <a href="{{url('subscription/invoice/'.$eachOrder['id'].'/user/'.$sellerDetails->user_id)}}" target="_blank"  class="viewOrder btn btn-success btn-sm">View</a> -->
                        <a href="{{url('subscription/invoice/'.$eachOrder['id'].'/user/'.$sellerDetails->user_id)}}" target="_blank" class="viewOrder"><i class="fa fa-eye text-green" title="View {{$eachOrder['id']}}"></i></a>&nbsp;&nbsp;
                        <a href="{{url('subscription/invoice/'.$eachOrder['id'].'/user/'.$sellerDetails->user_id)}}" target="_blank" class="viewOrder"><i class="fa fa-eye text-green" title="View {{$eachOrder['id']}}"></i></a>
                      </td>
                      @else
                      <td>
                        <a href="javascript:void(0);" class="viewOrder btn btn-success btn-sm">View</a>
                      </td>
                      @endif
                      
                    </tr>
                  @endforeach
                </tbody>
            </table>



        </div>
      </div>
      <!-- END TABS DIV -->
      
    </div>
    <div class="card-footer p-0">
      <h5 class="text-center"><small class="text-muted"><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b></small></h5>
    </div>
  </div>
        </div>
      </div>



     <!-- Extra large modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              
            </div>
            <div class="modal-body">
              <table class="table table-dark">
                  <thead>
                    <tr>
                      <th>Product ID</th>
                      <th>MRP</th>
                      <th>Rates</th>
                      <th>inventory</th>
                      <th>Min Order quantity</th>
                      <th>Pincodes</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td id="prod_id"></td>
                      <td id="mrp"></td>
                      <td id="rates"></td>
                      <td id="inventory"></td>
                      <td id="moq"></td>
                      <td id="pincodes"></td>
                    </tr>
                  </tbody>
              </table>
             <!--  <div class="row">
                  <div class="col-md-2">
                    <b>Product ID:</b><p class="prod_id"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Mrp:</b><p class="mrp"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Rate:</b><p class="rate"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Inventory:</b><p class="inventory"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Min Order quantity:</b><p class="moq"></p>
                  </div>
                  <div class="col-md-2">
                    <b>Pincodes:</b><p class="pincodes"></p>
                  </div>
                
              </div> -->
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

    <div class="modal fade gst_details" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">GST Verification</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-6">
                  <label>System GST Number : </label> {{$sellerDetails->gst_no}}
                </div>
                <div class="col-6">
                  <label>Company NAME : </label> {{$sellerDetails->comp_name}}
                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-6">
                  <label>GST Portal Number : </label> <span id="portalGst"></span>
                </div>
                <div class="col-6">
                  <label>Company NAME : </label> <span id="portalCompanyName"></span>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>

   <div class="modal fade" tabindex="-1" id="subscriptionMobile" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Subscription Mobile Number </h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-12">
                  <form method="post" action="/subscription/new">
                    @csrf
                    <div class="form-group">
                      <label for="Mobile">Mobile Numer : </label>
                      <input type="text" name="mobile" placeholder="Enter Your Mobile Number" class="form-control" id="mobile" maxlength="10">
                      <input type="hidden" name="id" value="{{$sellerId}}" id="id">
                      <input type="hidden" name="user_type" value="1" id="user_type">
                    </div>
                    <div class="form-group">
                      <input type="submit" class="btn btn-success" value="Send">
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
    </div>


@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap_tabs.css')}}" type="text/css">
 <!--style>
      .sellerRows > td { text-align: center; }
    </style-->

@stop
@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/bootstrap-tabs.js')}}"></script>
    <script src="{{asset('js/seller.js')}}"></script>


    <script type="text/javascript">
      $(document).ready(function() {
        var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
        var table2 = $('#order-table').DataTable({order:[[0,"desc"]]});
        var table3 = $('#subscription-table').DataTable({order:[[0,"desc"]]});

        
      });
    </script>
@stop

