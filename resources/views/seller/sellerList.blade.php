@extends('adminlte::page')

@section('title', 'BLUMART | Active Seller List')



@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-4">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Active Seller List</h4>
        </div>
        <div class="col-md-2">

        </div>
        <!-- <div class="col-md-4">
           <a href="#" class="export btn btn-warning btn-md mr-3">Import Format</a>
            <a href="#" class="export btn btn-primary btn-md mr-3">Import</a>
          <a href="{{ url('sellerExport') }}" class="export btn btn-success btn-md ">Export</a>
        </div> -->
        <div class="col-md-6 float-right">
          <!--  <a href="#" class="export btn btn-outline-success mr-3">Import Format</a> -->
          <a href="{{ url('sellerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i>Export</a>
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i>Import</a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i>Import Format</a>
           
            
          <!-- <a href="{{ url('buyerExport') }}" class="export btn btn-outline-danger ">Export</a> -->
        </div>

        <hr>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="Product-table">
                <thead>
                    <tr>
                        <th class="text-left">#</th>
                        
                        <th class="text-left">Name</th>
                        <th class="text-left">Company Name</th>
                        <th class="text-left">Mobile No.</th>
                        <th class="text-left">Email</th>
                        <th class="text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($sellerList as $eachRow => $eachSeller)
                    <tr class="sellerRows" id="{{$eachSeller->user_id}}" >
                      <td>{{$eachSeller->user_id}}</td>
                      <td>{{$eachSeller->name}}</td>
                      <td>{{$eachSeller->comp_name}}</td>
                      <td>{{$eachSeller->mobile}}</td>
                      <td>{{$eachSeller->email}}</td>
                      <td>
                        <a href="{{ url('/seller', [$eachSeller->user_id]) }}" target="_blank" class="viewSeller btn btn-outline-success ">View</a>
                        
                      </td>
                    </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
    </div>

      </div>          
    </div>
@stop

@section('css')
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      .sellerRows > td { text-align: left; }

    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script> -->


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="{{asset('js/seller.js')}}"></script>

    
    <!-- <script type="text/javascript">
      $(function () {
    
    var table = $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('sellerList') }}",
        columns: [
            {data: 'user_id', name: 'Sr.No'},
            {data: 'name', name: 'name'},
            {data: 'comp_name', name: 'comp_name'},
            {data: 'mobile', name: 'mobile'},
            {data: 'email', name: 'email'},

            {data: 'action', name: 'action', orderable: false, searchable: false},

        ]
    });
    
      });
      </script> -->
      <script type="text/javascript">
  $(document).ready(function() {
    var table = $('#Product-table').DataTable({order:[[0,"desc"]]});
  });
</script>

@stop

