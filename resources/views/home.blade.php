@extends('adminlte::page')

@section('title', 'BLUMART | Dashboard')

@section('content_header')
    
    <div class="row">
      <div class="col-8">
        <h1>Dashboard</h1>
      </div>
      
    </div>
@stop

@section('content')
@php
$salesTeamReportReceived =$combinedData['salesTeam']['received'];
$callCenterTeamReceived=$combinedData['callCenterTeam']['received'];
$buyerReceived=$combinedData['buyer']['received'];

$orderReceived= round($salesTeamReportReceived,2)+round($callCenterTeamReceived,2)+round($buyerReceived,2);

$salesTeamReportAccept =$combinedData['salesTeam']['accepted'];
$callCenterTeamAccept=$combinedData['callCenterTeam']['accepted'];
$buyerAccept=$combinedData['buyer']['accepted'];

$orderAccept= round($salesTeamReportAccept,2)+round($callCenterTeamAccept,2)+round($buyerAccept,2);

@endphp
<meta http-equiv="Refresh" content="300">
   <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="border-radius:16px; background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">{{$sellers}}</h3>

                <p style="color: #656464;">Active Sellers</p>
              </div>
              <div class="icon">
                <i class="fa fa-user" style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="{{url('seller/list')}}" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="border-radius:16px;background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">{{$buyers}}</h3>

                <p style="color: #656464;">Active Buyers</p>
              </div>
              <div class="icon">
                <i class="fa fa-users"  style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="{{url('buyer/list')}}" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning" style="border-radius:16px; background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">{{$orders}}</h3>

                <p style="color: #656464;">Active Orders</p>
              </div>
              <div class="icon">
                <i class="fa fa-cart-arrow-down" style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="{{url('orders')}}" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
           <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning" style="border-radius:16px; background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">{{$cancelOrders}}</h3>

                <p style="color: #656464;">Cancelled Orders</p>
              </div>
              <div class="icon">
                <i class="fa fa-times" style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="{{url('orders/cancel')}}" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box" style="border-radius:16px; background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">{{$products}}</h3>

                <p style="color: #656464;">Active Products</p>
              </div>
              <div class="icon">
                <i class="fa fa-shopping-bag" style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="{{route('activeProductsList')}}" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-dark text-white" style="border-radius:16px; background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">{{$smsCredit}}</h3>

                <p style="color: #656464;">SMS Remaining</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope" style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="#" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-secondary text-white" style="border-radius:16px; background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">{{$subscriptions}}</h3>

                <p style="color: #656464;">Subscriptions</p>
              </div>
              <div class="icon">
                <i class="fa fa-spinner" style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="#" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>

          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-secondary text-white" style="border-radius:16px; background-color: #d2cece!important;color:#000!important;">
              <div class="inner">
                <h3 style="color: #656464;">?</h3>

                <p style="color: #656464;">Orders Delivered</p>
              </div>
              <div class="icon">
                <i class="fa fa-truck" style="color:#0c8eae;" aria-hidden="true"></i>
              </div>
              <a href="#" class="small-box-footer" style="color: #05637a !important;">View All <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        <hr>

        <div class="row">
          <div class="col-12 text-center">
            <h5>Auto Refresh In : <b><span id="demo"></span></b> Minutes</h5>
          </div>
        </div>

        <div class="row">
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box">
              <span class="info-box-icon elevation-1" style="color: #ffc107;"><i class="fas fa-shopping-bag"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Orders Received</span>
                <h4 class="info-box-number">
                  <!-- <i class="fas fa-rupee-sign"></i> &nbsp;&nbsp;&nbsp;{{$totalSalesToday}}-->
                 <i class="fas fa-rupee-sign"></i> &nbsp;&nbsp;&nbsp;{{$orderReceived}}
                </h4>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon elevation-1" style="color: #ffc107;"><i class="fas fa-thumbs-up"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Order Accepted</span>
                <h4 class="info-box-number">
                  <!--<i class="fas fa-rupee-sign"></i> &nbsp;&nbsp;&nbsp;{{$totalActualSales}}-->
                  <i class="fas fa-rupee-sign"></i> &nbsp;&nbsp;&nbsp;{{$orderAccept}}
                </h4>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <!-- fix for small devices only -->
          <div class="clearfix hidden-md-up"></div>

          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon elevation-1" style="color: #ffc107;"><i class="fas fa-times"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Orders Rejected (Seller)</span>
                <h4 class="info-box-number">
                  <i class="fas fa-rupee-sign"></i> &nbsp;&nbsp;&nbsp;{{round($totalRejectedToday[0]->amount,2)}}
                </h4>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <div class="col-12 col-sm-6 col-md-3">
            <div class="info-box mb-3">
              <span class="info-box-icon elevation-1" style="color: #ffc107;"><i class="fas fa-trash-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Orders Cancelled</span>
                <h4 class="info-box-number">
                  <i class="fas fa-rupee-sign"></i> &nbsp;&nbsp;&nbsp;{{round($totalCancelledToday[0]->amount,2)}}
                </h4>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->
          <!-- /.col -->
          <!-- <div class="col-12 col-sm-6 col-md-2 ">
            <div class="info-box mb-3">
              <span class="info-box-icon elevation-1" style="color: #ffc107;"><i class="fas fa-trash-alt"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Orders Return (Buyer)</span>
                <h4 class="info-box-number">
                  <i class="fas fa-rupee-sign"></i> &nbsp;&nbsp;&nbsp;{{round($totalCancelledToday[0]->amount,2)}}
                </h4>
              </div>
              
            </div>
           
          </div> -->
          <!-- /.col -->
        </div>

        <hr>

        <div class="row">
          <div class="col-md-6">
            <div class="card card-widget widget-user">
              <div class="widget-user-header bg-info">
                <h3 class="widget-user-username"><b>Sales Team Report</b></h3>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{asset('img/sales-representative.png')}}" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h2 class="description-header" style="font-size: 32px;">{{round($combinedData['salesTeam']['received'],2)}}</h2>
                      <i class="fa fa-thumbs-up" aria-hidden="true" style="color: royalblue;"></i> &nbsp;&nbsp;<span class="description-text">Received</span>
                    </div>
                  </div>
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header" style="font-size: 32px;">{{round($combinedData['salesTeam']['accepted'],2)}}</h5>
                      <i class="fa fa-check" aria-hidden="true" style="color: green;"></i> &nbsp;&nbsp;<span class="description-text">Accepted</span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header" style="font-size: 32px;">{{round($combinedData['salesTeam']['rejected'],2)}}</h5>
                      <i class="fa fa-thumbs-down" aria-hidden="true" style="color: red;"></i> &nbsp;&nbsp;<span class="description-text">Rejected</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-widget widget-user">
              <div class="widget-user-header bg-warning">
                <h3 class="widget-user-username"><b>Call Center Team Report</b></h3>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{asset('img/call-center-representative.png')}}" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header" style="font-size: 32px;" >{{round($combinedData['callCenterTeam']['received'],2)}}</h5>
                      <i class="fa fa-thumbs-up" aria-hidden="true" style="color: royalblue;"></i>&nbsp;&nbsp;<span class="description-text">Received</span>
                    </div>
                  </div>
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header" style="font-size: 32px;">{{round($combinedData['callCenterTeam']['accepted'],2)}}</h5>
                      <i class="fa fa-check" aria-hidden="true" style="color: green;"></i> &nbsp;&nbsp;<span class="description-text">Accepted</span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header" style="font-size: 32px;">{{round($combinedData['callCenterTeam']['rejected'],2)}}</h5>
                      <i class="fa fa-thumbs-down" aria-hidden="true" style="color: red;"></i> &nbsp;&nbsp;<span class="description-text">Rejected</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col-md-6">
            <div class="card">
              <div class="card-header text-center"><h5><b>Daily Sales Team Report</b></h5></div>
                <div class="card-body">
                  <div id="container"></div>
                </div>
              </div>
          </div>
          <div class="col-md-6 mb-4">
            <div class="card">
              <div class="card-header text-center"><h5><b>Daily Call Center Team Report</b></h5></div>
                <div class="card-body">
                  <div id="container5"></div>
                </div>
              </div>
          </div>

          <div class="col-md-12">
            <div class="card card-widget widget-user">
              <div class="widget-user-header bg-dark">
                <h3 class="widget-user-username"><b>Buyer's Order Report</b></h3>
              </div>
              <div class="widget-user-image">
                <img class="img-circle elevation-2" src="{{asset('img/shopkeeper.jpeg')}}" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h2 class="description-header" style="font-size: 32px;">{{round($combinedData['buyer']['received'],2)}}</h2>
                      <i class="fa fa-thumbs-up" aria-hidden="true" style="color: royalblue;"></i> &nbsp;&nbsp;<span class="description-text">Received</span>
                    </div>
                  </div>
                  <div class="col-sm-4 border-right">
                    <div class="description-block">
                      <h5 class="description-header" style="font-size: 32px;">{{round($combinedData['buyer']['accepted'],2)}}</h5>
                      <i class="fa fa-check" aria-hidden="true" style="color: green;"></i> &nbsp;&nbsp;<span class="description-text">Accepted</span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="description-block">
                      <h5 class="description-header" style="font-size: 32px;">{{round($combinedData['buyer']['rejected'],2)}}</h5>
                      <i class="fa fa-thumbs-down" aria-hidden="true" style="color: red;"></i> &nbsp;&nbsp;<span class="description-text">Rejected</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>


          <div class="col-md-12">
            <div class="card">
              <div class="card-header text-center"><h5><b>Sales- Buyers App</b></h5></div>
                <div class="card-body">
                  <div id="container6"></div>
                </div>
              </div>
          </div>
        </div>
       
        <div class="row mt-4">
          <div class="col-12">
            <div class="card">
              <div class="card-header text-center"><h5><b>Monthly Daywise Deliveries</b></h5></div>
                <div class="card-body">
                  <div id="container1"></div>
                </div>
              </div>
            
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-6">
                <div id="container4"></div>
          </div>
          <div class="col-6">
                <div id="container2"></div>
          </div>
        </div>
        <div class="row mt-4">
          <div class="col-12">
                <div class="card">
                  <div class="card-header text-center"><h5><b>Top 10 Sellers This Month(Delivery wise)</b></h5></div>
                  <div class="card-body">
                    <div class="row">
                      <div class="col-6">
                        <table class="table table-hover">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Company Name</th>
                              <th scope="col">Amount</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($topSellers as $eachSeller)
                            <tr>
                              <td>{{$loop->iteration}}</td>
                              <td>{{$eachSeller['name']}}</td>
                              <td>{{round($eachSeller['y'],2)}}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                      <div class="col-6">
                        <div id="container3"></div>
                      </div>
                    </div>
                  </div>
                  <div class="card-footer"></div>
                </div>
          </div>
          <div class="col-12">
                <div class="card">
                  <div class="card-header text-center"><h5><b>Top 10 Buyers This Month(Order wise)</b></h5></div>
                  <div class="card-body">
                    
                  </div>
                  <div class="card-footer"></div>
                </div>
          </div>
        </div>  
@stop

@section('css')
    <style>
   
    </style>
@stop

@section('js')
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/full-screen.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>

<script type="text/javascript">

var countDownDate = new Date().getTime() + 5 * 60 * 1000;
var x = setInterval(function() {
var now = new Date().getTime();
var distance = countDownDate - now;
var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
var seconds = Math.floor((distance % (1000 * 60)) / 1000);
document.getElementById("demo").innerHTML =  hours + ":"
  + minutes + ":" + seconds;
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "Refreshing Now";
  }
}, 1000);

  var days =  <?php echo json_encode($deliveryDays) ?>;
  var DelAmt =  <?php echo json_encode($deliveryAmounts) ?>;
  var topProducts =  <?php echo json_encode($topProducts) ?>;
  var string = <?php echo json_encode($topSellers)?>;
  var monthNames = ["January", "February", "March", "April", "May","June","July", "August", "September", "October", "November","December"];
  const d = new Date();

  Highcharts.chart('container', {
    title: {
        text: ''
    },
    subtitle: {
            text: 'Source: Blumart RetailZ Application'
        },
    xAxis: {
      lineWidth: 3,
            labels: {
                  style: {
                      fontSize: '13px',
                      color: '#000000',
                      format: '{value:%H:%M:%S.%L}',

                  }
              },
        categories: <?php echo json_encode($labels) ?>
    },
    labels: {
        items: [{
            html: '',
            style: {
                left: '20px',
                top: '10px',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Total',
        data: <?php echo json_encode($amounts) ?>
    }, {
        type: 'column',
        name: 'Cancelled',
        data: <?php echo json_encode($cancelled) ?>
    }, {
        type: 'column',
        name: 'Rejected',
        data: <?php echo json_encode($rejected) ?>
    }]
});

  Highcharts.chart('container5', {
    title: {
        text: ''
    },
    subtitle: {
            text: 'Source: Blumart Admin Panel'
        },
    xAxis: {
      lineWidth: 3,
            labels: {
                  style: {
                      fontSize: '13px',
                      color: '#000000',
                      format: '{value:%H:%M:%S.%L}',

                  }
              },
        categories: <?php echo json_encode($labels1) ?>
    },
    labels: {
        items: [{
            html: '',
            style: {
                left: '20px',
                top: '10px',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Total',
        data: <?php echo json_encode($amounts1) ?>
    }, {
        type: 'column',
        name: 'Cancelled',
        data: <?php echo json_encode($cancelled1) ?>
    }, {
        type: 'column',
        name: 'Rejected',
        data: <?php echo json_encode($rejected1) ?>
    }]
});

    Highcharts.chart('container6', {
    title: {
        text: ''
    },
    subtitle: {
            text: 'Source: Blumart Admin Panel'
        },
    xAxis: {
      lineWidth: 3,
            labels: {
                  style: {
                      fontSize: '13px',
                      color: '#000000',
                      format: '{value:%H:%M:%S.%L}',

                  }
              },
        categories: <?php echo json_encode($labels2) ?>
    },
    labels: {
        items: [{
            html: '',
            style: {
                left: '20px',
                top: '10px',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'black'
            }
        }]
    },
    series: [{
        type: 'column',
        name: 'Total',
        data: <?php echo json_encode($amounts2) ?>
    }, {
        type: 'column',
        name: 'Cancelled',
        data: <?php echo json_encode($cancelled2) ?>
    }, {
        type: 'column',
        name: 'Rejected',
        data: <?php echo json_encode($rejected2) ?>
    }]
});

    

Highcharts.chart('container1', {
    chart: {
        type: 'line'
    },
    title: {
        text: ''
    },
    subtitle: {
        text: 'Source: Blumart '
    },
    xAxis: {
        categories: days
    },
    yAxis: {
        title: {
            text: 'Total Delivery Amount'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: true
            },
            enableMouseTracking: false
        }
    },
    series: [{
        name: 'Daywise Deliveries',
        data: DelAmt
    }]
});

//Total Products Sold Today
Highcharts.chart('container4', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45,
            beta: 0
        }
    },
    title: {
        text: 'Total Products Sold Today'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            depth: 35,
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            },
            style: {
                    fontSize: 13.5   
                }
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        data: <?php echo json_encode($ProductsSoldToday) ?>,
    }]
});



//Top 10 Selling Products This Month graph
 
Highcharts.setOptions({
  colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
    return {
      radialGradient: {
        cx: 0.5,
        cy: 0.3,
        r: 0.7
      },
      stops: [
        [0, color],
        [1, Highcharts.color(color).brighten(-0.5).get('rgb')] // darken
      ]
    };
  })
});

// Build the chart
Highcharts.chart('container2', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'Top 10 Selling Products This Month'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  accessibility: {
    point: {
      valueSuffix: '%'
    }
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b>: {point.percentage:.4f} %',
        connectorColor: 'green',
        style: {
                    fontSize: 13.5   
                }
      }
    }
  },
  series: [{
    name: 'Share',
    data: 
      topProducts
    
  }]
});


Highcharts.chart('container3', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false
    },
    title: {
        text: 'BLUMART<br>'+ monthNames[d.getMonth()]+'<br>'+'2021',
        align: 'center',
        verticalAlign: 'middle',
        y: 60
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: -50,
                style: {
                    fontWeight: 'bold',
                    color: 'white'
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '110%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Browser share',
        innerSize: '50%',
        data: 
            string
    }]
});



</script>
@stop

