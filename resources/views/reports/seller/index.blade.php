@extends('adminlte::page')

@section('title', 'BLUMART | Consolidated Report')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Consolidated Reports</h4>
        </div>
        
        <div class="col-md-6 float-right">
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div>
        <hr>
      </div>
<hr>
      <div class="row">
          <div class="col-md-12">
            <form action="{{url('reports/consolidated/generate')}}" name="reconForm" id="reconForm" method="post">
              {{ csrf_field() }}
              <div class="row">
                <div class="form-group col-2">
                  <label for="email">Type:</label>
                  <select name="type" id="type" class="form-control">
                    @if(Session::get('type') == 1)
                    <!-- <option value="1" selected >Delivery</option> -->
                    <option value="2" >Purchase Order (Accepted)</option>
                    @elseif(Session::get('type') == 2)
                    <!-- <option value="1" >Delivery</option> -->
                    <option value="2" selected>Purchase Order (Accepted)</option>
                    @else
                    <!-- <option value="1" >Delivery</option> -->
                    <option value="2" >Purchase Order (Accepted)</option>
                    @endif
                  </select>
                </div>
                <!-- <div class="form-group col-2">
                  <label for="status">Status:</label>
                  <select name="status" id="status" class="form-control">
                    <option value="1">New</option>
                    <option value="2">Accepted</option>
                    <option value="3">Rejected</option>
                    <option value="4">Cancelled</option>
                    <option value="5">Completed</option>
                    <option value="6">Exipred</option>
                    <option value="7">Delivery Expired</option>
                    <option value="8">Pending for Payment</option>
                    <option value="9">Payment Declined</option>
                  </select>
                </div> --> 
                 <div class="form-group col-2">
                  <label for="email">Seller Name:</label>
                  <select name="sellerName" id="sellerName" class="form-control">
                  <option value="0">All</option>
                    @foreach($sellerList as $eachSeller)
                      <option value="{{$eachSeller->user_id}}">{{$eachSeller->comp_name}}</option>
                    @endforeach
                  </select>
                </div>
                 <div class="form-group col-2">
                  <label for="email">From:</label>
                  <input type="date" id="from" class="form-control" value="{{ Session::get('from')}}" name="from" max="<?php echo date("Y-m-d"); ?>">
                </div>
                <div class="form-group col-2">
                  <label for="email">To:</label>
                  <input type="date" id="to" class="form-control" value="{{ Session::get('to')}}" name="to" max="<?php echo date("Y-m-d"); ?>">
                </div>
                <div class="form-group col-3">
                   
                </div>
                <div class="form-group col-3" style="margin-top: 30px;">
                  <input type="submit" id="reportBtn" value="Show Records" class="btn btn-success"></a>
                  <a href="{{url('/reports/order/download')}}" id="downloadReport" class="btn btn-primary">Download Report</a>
                  <a href="{{url('/reports/consolidated')}}" class="btn btn-danger">Reset</a>
                </div>
              </div>    
             </form>
        </div>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
          <div class="row">
           @foreach($data as $eachSeller)
              <div class="col-md-12" id="{{$eachSeller[0]['user_id']}}">
                <div class="card">
                  <div class="card-header text-center">
                    <div class="row">
                      <div class="col-6">
                        <b>{{$eachSeller[0]['comp_name']}}</b>&nbsp;&nbsp; | &nbsp;&nbsp;
                        <b>Mobile : {{$eachSeller[0]['mobile']}}</b>&nbsp;&nbsp; | &nbsp;&nbsp;
                      </div>
                      <div class="col-6">
                        <form name="poForm" action="{{route('consolidatedPo')}}" method="post">
                          @csrf
                          <input type="hidden" value="{{$eachSeller[0]['user_id']}}" name="id">
                          <input type="hidden" value="{{ Session::get('from')}}" name="from" class="from">
                          <input type="hidden" value="{{ Session::get('to')}}" name="to" class="to">
                          <a href="#" class="btn btn-outline-info sellerBtn" data-id="{{$eachSeller[0]['user_id']}}"><i class="fa fa-print" style="color: red" aria-hidden="true"></i> Print</a>
                        </form>
                        
                      </div>
                    </div>

                  </div>
                  <div class="card-body">
                    <table class="table table-hover" border="1" cellpadding="3" id="buyerTable">
                      <thead>
                          <tr>
                            <th class="text-left">#</th>
                            <!-- <th class="text-left">Company Name</th> -->
                            <th class="text-left">Product Name</th>
                            <th class="text-left">PO #</th>
                            <th class="text-left">Invoice #</th>
                            <th class="text-left">HSN</th>
                            <th class="text-left">Approved Qty</th>
                            <th class="text-left">Base Total (Without GST)</th>
                            <th class="text-left">CGST</th>
                            <th class="text-left">SGST</th>
                            <th class="text-left">Total (Including GST)</th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($eachSeller as $eachSellerData)
                          <tr class="sellerRows" id="" >
                            <td>{{$loop->iteration}}</td>
                            <td>{{$eachSellerData['product_name']}}</td>
                            <td>
                              @foreach(explode(',', $eachSellerData['poList']) as $info)
                                  <a href="{{ url('/order', [$info]) }}" class="btn btn-primary btn-sm mb-1">{{ $info }}</a>
                              @endforeach
                            </td>
                            <td>
                              @foreach(explode(',', $eachSellerData['invoiceList']) as $invoice)
                                  <a href="{{ url('/invoice', [$invoice]) }}" class="btn btn-success btn-sm mb-1">{{ $invoice }}</a>
                              @endforeach
                            </td>
                            <td>{{$eachSellerData['hsn']}}</td>
                            <td>{{$eachSellerData['Quantity']}}</td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i>  {{round($eachSellerData['grossAmt'], 2)}}</td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i> @php echo round($eachSellerData['cgst'],2) @endphp</td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i> @php echo round($eachSellerData['cgst'],2) @endphp</td>
                            <td><i class="fa fa-inr" aria-hidden="true"></i> @php echo round($eachSellerData['amount'],2) @endphp</td>
                          </tr>
                         @endforeach
                      </tbody>
                  </table>
                  </div>
                </div>
              </div>
          @endforeach
          </div>
        </div>
    </div>
      </div>
      <div class="ajaxLoader">
        <img src="{{asset('img/Pinwheel.gif')}}" class="loader" placeholder="loader">
      </div>

    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    <style>
      .sellerRows > td { text-align: left; }
      .ajaxLoader{position:relative; height:100px;width:1000px;}
      .loader{position:absolute;left:0;right:0;top:0;bottom:0;margin:auto}
    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
   $(document).ready(function() {

     $('#downloadReport').css('display','none');
      $('.loader').css('display','none');
      //var table = $('#buyerTable').DataTable({order:[[0,"desc"]]});

        /*$('#reportBtn').click(function(e){
          $('.loader').css('display','block');
          table.clear().draw();
          var from = $('#from').val();
          var to = $('#to').val();
          if(from == ''){
            alert('Please select FROM Date');
            e.preventDefault();
          }else if(to == '' ){
            alert('Please select TO Date');
            e.preventDefault();
          }else{
              dates = [];
              dates[0] = from;
              dates[1] = to;
                  $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "/reports/order/datewise",
                    method: "POST",
                    data: {data:dates},
                    cache: false,
                    success: function(data){
                      var optionsAsString =''; 
                        $.each( data, function( key, value ) {
                         
                          table.row.add( [
                              value.vend_prod_id,
                              value.comp_name,
                              value.product_name,
                              value.hsn,
                              value.uom,
                              value.qty,
                              value.cgst,
                              value.amount
                          ] ).draw();
                      });
                        $('.loader').css('display','none');
                      }

              }); 
                  
                  $('#downloadReport').show();
                   $(this).css('display','none');  
          }
        });*/

        $('#reportBtn').click(function(e){
          var from = $('#from').val();
          var to = $('#to').val();
          $('.from').val(from);
          $('.to').val(to);
        });

       $("#from").change(function(){   // 1st
          $('#downloadReport').hide();
          $('#reportBtn').show();
          var from = $('#from').val();
          $('.from').val(from);
      });

        $("#to").change(function(){   // 1st
          $('#downloadReport').hide();
          $('#reportBtn').show();
          var to = $('#to').val();
          $('.to').val(to);
      });


        $('#datewiseOrderExport').click(function(e){
          table.clear().draw();
          var from = $('#from').val();
          var to = $('#to').val();
          if(from == ''){
            alert('Please select FROM Date');
            e.preventDefault();
          }else if(to == '' ){
            alert('Please select TO Date');
            e.preventDefault();
          }else{
              dates = [];
              dates[0] = from;
              dates[1] = to;
                  $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "/datewiseOrderExport/",
                    type: "POST",
                    data: { 
                            from: from,
                            to:to
                          },
                    cache: false,
                    success: function(data){
                      var blobData = new Blob([data], {type: "application/xlsx"})
                      saveAs(blobData, filename+'.xlsx')
                      }
              });
          }
        });


      $('.sellerBtn').click(function(e){
          var id= $(this).attr('data-id');
          var prtContent = document.getElementById(id);
          var WinPrint = window.open();
          WinPrint.document.write(prtContent.innerHTML);
          WinPrint.document.close();
          WinPrint.focus();
          WinPrint.print();
          WinPrint.close();
       });

      });
</script>

@stop

