@extends('adminlte::page')

@section('title', 'BLUMART | Buyer Reports')

@section('content')
   <div class="container-fluid">
      <div class="row">
        <div class="col-md-6">
            <h4><b style="color:#00a9d0;">BLU</b><b style="color:#faa61a;">MART</b> Buyer OnBoarding Reports</h4>
        </div>
        
        <div class="col-md-6 float-right">
          <!-- <a href="#" id="datewiseBuyerExport" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a> -->
        <!-- <a href="{{ url('datewiseBuyerExport') }}" class="ml-2 btn btn-outline-success btn-rounded waves-effect float-right"><i class="fas fa-upload pr-2"
        aria-hidden="true"></i><b>Export</b></a> -->
           <a href="#" class="ml-2 btn btn-outline-info btn-rounded waves-effect float-right"><i class="fas fa-download pr-2"
        aria-hidden="true"></i><b>Import</b></a>
            <a href="#" class="btn btn-outline-warning btn-rounded waves-effect float-right"><i class="fas fa-cogs pr-2"
        aria-hidden="true"></i><b>Import Format</b></a>
        </div>
        <hr>
      </div>
<hr>
      <div class="row">
          <div class="col-md-12">
            <form action="{{url('reports/buyer/datewise')}}" name="reconForm" id="reconForm" method="post">
              {{ csrf_field() }}
              <label for="reconDate">From:</label>
                <input type="date" id="from" name="from" max="<?php echo date("Y-m-d"); ?>">&nbsp;&nbsp;&nbsp;
              <label for="reconDate">To:</label>
                <input type="date" id="to" name="to" max="<?php echo date("Y-m-d"); ?>">
                <a href="#" id="reportBtn" class="btn btn-success">Show Records</a>
                <a href="{{url('/reports/buyer/download')}}" id="downloadReport" class="btn btn-primary">Download Report</a>
                 <a href="{{url('/reports/buyer')}}" class="btn btn-danger">Reset</a>
             </form>
        </div>
      </div>
      <hr>
      <div class="sellerList-table">
        <div class="row">
        <div class="col-md-12 mt-10">
            <table class="table table-hover" id="buyerTable">
                <thead>
                    <tr>
                       <th class="text-left">#</th>
                       <th class="text-left">Date</th>
                        <th class="text-left">Name</th>
                        <th class="text-left">Company</th>
                        <th class="text-left">Mobile</th>
                        <th class="text-left">Address</th>
                        <th class="text-left">Pincode</th>
                        <th class="text-left">status</th>
                        <th class="text-left">Onboard By</th>
                    </tr>
                </thead>
                <tbody>
                  
                </tbody>
            </table>
        </div>
    </div>
      </div>
      <div class="ajaxLoader">
        <img src="{{asset('img/Glowing_ring.gif')}}" class="loader" placeholder="loader">
      </div>

    </div>
@stop

@section('css')
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/admin_custom.css')}}">
    <style>
      .sellerRows > td { text-align: left; }
      .ajaxLoader{position:relative; height:100px;width:1000px;}
      .loader{position:absolute;left:0;right:0;top:0;bottom:0;margin:auto}
    </style>
@stop

@section('js')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>     
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
   $(document).ready(function() {
     $('#downloadReport').css('display','none');
      $('.loader').css('display','none');
    var table = $('#buyerTable').DataTable({order:[[0,"desc"]]});

        $('#reportBtn').click(function(e){
          $('.loader').css('display','block');
          table.clear().draw();
          var from = $('#from').val();
          var to = $('#to').val();
          if(from == ''){
            alert('Please select FROM Date');
            e.preventDefault();
          }else if(to == '' ){
            alert('Please select TO Date');
            e.preventDefault();
          }else{
              dates = [];
              dates[0] = from;
              dates[1] = to;
                  $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "/reports/buyer/datewise",
                    method: "POST",
                    data: {data:dates},
                    cache: false,
                    success: function(data){
                      var optionsAsString =''; 
                        $.each( data, function( key, value ) {
                          if(value.is_active == 1){
                            var status = 'Active';
                          }else
                          if(value.is_active == 4){
                            var status = 'Draft';
                          }else{
                            var status = 'Request for Review';
                          }
                          table.row.add( [
                              value.user_id,
                              value.createdAt,
                              value.name,
                              value.comp_name,
                              value.mobile,
                              value.Addr_1,
                              value.pincode,
                              status,
                              value.sales_team_name,
                          ] ).draw();
                      });
                        $('.loader').css('display','none');
                      }

              }); 
                  
                  $('#downloadReport').show();
                   $(this).css('display','none');  
          }
        });

       $("#from").change(function(){   // 1st
          $('#downloadReport').hide();
          $('#reportBtn').show();
      });

        $("#to").change(function(){   // 1st
          $('#downloadReport').hide();
          $('#reportBtn').show();
      });


        $('#datewiseBuyerExport').click(function(e){
          table.clear().draw();
          var from = $('#from').val();
          var to = $('#to').val();
          if(from == ''){
            alert('Please select FROM Date');
            e.preventDefault();
          }else if(to == '' ){
            alert('Please select TO Date');
            e.preventDefault();
          }else{
              dates = [];
              dates[0] = from;
              dates[1] = to;
                  $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "/datewiseBuyerExport/",
                    type: "POST",
                    data: { 
                            from: from,
                            to:to
                          },
                    cache: false,
                    success: function(data){
                      var blobData = new Blob([data], {type: "application/xlsx"})
                      saveAs(blobData, filename+'.xlsx')
                      }
              });
          }
        });
      });
</script>

@stop

