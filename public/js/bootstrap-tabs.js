$('.viewProduct').click(function(){
	var productId = $(this).closest('tr').attr('id');
	var sellerId = $('#sellerId').text();
   $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
      }
  });
   jQuery.ajax({
      url: "/product/details",
      method: 'get',
      data: {
         productId: productId,
         sellerId: sellerId
      },
      success: function(result){
      	$('.modal-title').html(result[0]['product_name']);
      		$('#prod_id').html(result[0]['prod_id']);
      			$('#mrp').html(result[0]['mrp']);
      				$('#rates').html(result[0]['rate']);
      					$('#inventory').html(result[0]['inv_qty']);
      						$('#moq').html(result[0]['ret_minord_qnty']);
      							$('#pincodes').html(result[0]['approved_pin']);
      							console.log(result[0]);
      }});
});