 $(document).ready(function() {

        $('#zone').change(function(){
        	var zoneId = $(this).val();
        	$.ajax({
				  url: 'zones/'+zoneId,
				  type: "get",
				  success: function(data){
				  			console.log(data);
					  }
				});
        
        });
      });

 $(document).on('click', '#cancelBtn', function(event) {
        var buyerId = $(this).attr('data-id');
        var subscrId = $(this).attr('data-subscr');
        $.ajax({
				  url: "/subscription/cancel/user/"+buyerId+"/subscr/"+subscrId,
				  type: "get",
				  cache: false,
				  success: function(data){
				  			console.log(data);
					  }
		});
    });

 $('#zone_id').change(function () {
    var zone =$(this).find(":selected").val();
    $.ajax({
				  url: "/zones/"+zone,
				  type: "get",
				  cache: false,
				  success: function(data){
				  	var optionsAsString =''; 
				  		$.each( data, function( key, value ) {
						 optionsAsString += "<option value='" + key + "'>" + value + "</option>";	
						});
						$('#region_id').html('');
				  		$( 'select[name="region_id"]' ).append( optionsAsString );
				  			
					  }
		});
});

$('#region_id').change(function () {
    var region =$(this).find(":selected").val();
    $.ajax({
				  url: "/regions/"+region,
				  type: "get",
				  cache: false,
				  success: function(data){
				  	var optionsAsString =''; 
				  		$.each( data, function( key, value ) {
						 optionsAsString += "<option value='" + key + "'>" + value + "</option>";	
						});
						$('#area_id').html('');
				  		$( 'select[name="area_id"]' ).append( optionsAsString );
				  			
					  }
		});
});


$('#area_id').change(function () {
    var area =$(this).find(":selected").val();
    $.ajax({
				  url: "/area/"+area,
				  type: "get",
				  cache: false,
				  success: function(data){
				  	var optionsAsString =''; 
				  		$.each( data, function( key, value ) {
						 optionsAsString += "<option value='" + key + "'>" + value + "</option>";	
						});
						$('#route_id').html('');
				  		$( 'select[name="route_id"]' ).append( optionsAsString );
				  			
					  }
		});
});

 $(document).on('click', '#pauseBtn', function(event) {
        var sellerId = $(this).attr('data-id');
        var subscrId = $(this).attr('data-subscr');
        $.ajax({
				  url: "/subscription/pause/user/"+sellerId+"/subscr/"+subscrId,
				  type: "get",
				  cache: false,
				  success: function(data){
				  			//console.log(data);
					  }
		});

    });

 $(document).on('click', '#resumeBtn', function(event) {
        var sellerId = $(this).attr('data-id');
        var subscrId = $(this).attr('data-subscr');
        $.ajax({
				  url: "/subscription/resume/user/"+sellerId+"/subscr/"+subscrId,
				  type: "get",
				  cache: false,
				  success: function(data){
				  			//console.log(data);
					  }
		});

    });