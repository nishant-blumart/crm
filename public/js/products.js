 $(document).ready(function() {
        $('.catagory').change(function(){
		$('.sub_catagory').children().not(':first').remove();
		$('.child_catagory').children().not(':first').remove();
        	var id = $('option:selected', this).val();
        	$.ajax({
				  url: 'subcat/'+id,
				  type: "get",
				  success: function(data){
				  			$.each(data, function (i, item) {
							    $('.sub_catagory').append($('<option>', { 
							        value: item.sub_cat_id,
							        text : item.sub_cat_name 
							    }));
							});
					}
				});
        	});
      	});

  $(document).ready(function() {
        $('.sub_catagory').change(function(){
			$('.child_catagory').children().not(':first').remove();
        	var id = $('option:selected', this).val();
        	$.ajax({
				  url: 'childcat/'+id,
				  type: "get",
				  success: function(data){
				  			$.each(data, function (i, item) {
							    $('.child_catagory').append($('<option>', { 
							        value: item.child_cat_id,
							        text : item.child_cat_name 
							    }));
							});
					}
				});
        	});
      	});

 $(document).on('click', '#searchProductsBtn', function(event) {
 	var data = {};
 	$('#productsData').empty();
 	$("#productsData").append(data);
 	data.catagory = $('.catagory').find(":selected").val();
 	data.sub_catagory = $('.sub_catagory').find(":selected").val();
 	data.child_catagory = $('.child_catagory').find(":selected").val();
 	data.product_Search = $('.product_Search').val();
 	data.brand = $('.brand').find(":selected").val();
    $.ajax({
    			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
				url: "/products/filter",
				type: "post",
				data: data,
				success: function(data){
				  			 $("#productsData").append(data);
					  }
		});
    });

 $('#zone_id').change(function () {
    var zone =$(this).find(":selected").val();
    $.ajax({
				  url: "/zones/"+zone,
				  type: "get",
				  cache: false,
				  success: function(data){
				  	var optionsAsString =''; 
				  		$.each( data, function( key, value ) {
						 optionsAsString += "<option value='" + key + "'>" + value + "</option>";	
						});
						$('#region_id').html('');
				  		$( 'select[name="region_id"]' ).append( optionsAsString );
				  			
					  }
		});
});

$('#region_id').change(function () {
    var region =$(this).find(":selected").val();
    $.ajax({
				  url: "/regions/"+region,
				  type: "get",
				  cache: false,
				  success: function(data){
				  	var optionsAsString =''; 
				  		$.each( data, function( key, value ) {
						 optionsAsString += "<option value='" + key + "'>" + value + "</option>";	
						});
						$('#area_id').html('');
				  		$( 'select[name="area_id"]' ).append( optionsAsString );
				  			
					  }
		});
});


$('#area_id').change(function () {
    var area =$(this).find(":selected").val();
    $.ajax({
				  url: "/area/"+area,
				  type: "get",
				  cache: false,
				  success: function(data){
				  	var optionsAsString =''; 
				  		$.each( data, function( key, value ) {
						 optionsAsString += "<option value='" + key + "'>" + value + "</option>";	
						});
						$('#route_id').html('');
				  		$( 'select[name="route_id"]' ).append( optionsAsString );
				  			
					  }
		});
});

 $(document).on('click', '#pauseBtn', function(event) {
        var sellerId = $(this).attr('data-id');
        var subscrId = $(this).attr('data-subscr');
        $.ajax({
				  url: "/subscription/pause/user/"+sellerId+"/subscr/"+subscrId,
				  type: "get",
				  cache: false,
				  success: function(data){
				  			//console.log(data);
					  }
		});

    });

 $(document).on('click', '#resumeBtn', function(event) {
        var sellerId = $(this).attr('data-id');
        var subscrId = $(this).attr('data-subscr');
        $.ajax({
				  url: "/subscription/resume/user/"+sellerId+"/subscr/"+subscrId,
				  type: "get",
				  cache: false,
				  success: function(data){
				  			//console.log(data);
					  }
		});

    });