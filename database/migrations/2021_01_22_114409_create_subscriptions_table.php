<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->id();
            $table->string('subscr_id');
            $table->foreignId('user_id');
            $table->integer('user_type');
            $table->string('plan_id');
            $table->string('status');   
            $table->string('current_start');
            $table->string('current_end');
            $table->string('ended_at');
            $table->integer('quantity');
            $table->string('notes');
            $table->string('charge_at');
            $table->string('start_at');
            $table->string('end_at');
            $table->integer('total_count');
            $table->integer('paid_count');
            $table->string('customer_notify');
            $table->string('razor_created_at');
            $table->string('expire_by');
            $table->string('short_url');
            $table->string('has_scheduled_changes');
            $table->string('change_scheduled_at');
            $table->string('source');
            $table->integer('remaining_count');
            $table->string('sms_id');
            $table->integer('sentBy_id');
            $table->string('sentBy_name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
