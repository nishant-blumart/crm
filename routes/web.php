<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\pincode;
use App\Http\Controllers\sellerController;
use App\Http\Controllers\buyerController;
use App\Http\Controllers\beatController;
use App\Http\Controllers\subscriptionController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\orderController;
use App\Http\Controllers\deliveryController;
use App\Http\Controllers\LiveSearchController;
use App\Http\Controllers\cartController;
use App\Http\Controllers\LodController;
use App\Http\Controllers\catagorySettingsController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/pincodes', [App\Http\Controllers\pincode::class, 'index'])->name('pincodes');

Route::get('/seller/list',[App\Http\Controllers\sellerController::class, 'index'])->name('sellerList');
Route::get('sellerExport', [App\Http\Controllers\sellerController::class, 'export'])->name('sellerExport');
Route::post('sellerImport', [App\Http\Controllers\importExportController::class, 'importExcelSeller'])->name('sellerImport');
Route::get('/seller/{id}',[App\Http\Controllers\sellerController::class, 'getDetailsById'])->name('getDetailsById');
Route::get('/product/details',[App\Http\Controllers\sellerController::class, 'getProductDetailsById'])->name('getProductDetailsById');


Route::get('/buyer/list',[App\Http\Controllers\buyerController::class, 'index'])->name('buyerList');
Route::get('buyerExport', [App\Http\Controllers\buyerController::class, 'export'])->name('buyerExport');
Route::get('/buyer/new',[App\Http\Controllers\buyerController::class, 'new'])->name('new');
Route::get('/buyer/{id}',[App\Http\Controllers\buyerController::class, 'getDetailsById'])->name('getDetailsById');
Route::get('/buyer/edit/{id}',[App\Http\Controllers\buyerController::class, 'edit'])->name('editBuyer');
Route::post('/buyer/update',[App\Http\Controllers\buyerController::class, 'update'])->name('updateBuyer');
Route::get('/invoice/{invoice_id}/{supplierStatus}',[App\Http\Controllers\InvoiceController::class, 'buyerInvoice'])->name('buyerInvoice');
Route::get('buyer/list/pincodewise', function () {
    return view('buyer.pincodewise');
});
Route::post('/buyers/pincodewise',[App\Http\Controllers\buyerController::class, 'pincodewise'])->name('pincodewise');

Route::get('/sales/team',[App\Http\Controllers\salesTeamController::class, 'index'])->name('salesTeamList');

Route::get('/products/list',[App\Http\Controllers\productController::class, 'list'])->name('productsList');
Route::get('/products/active/list',[App\Http\Controllers\productController::class, 'activeProductsList'])->name('activeProductsList');

Route::get('products/subcat/{id}',[App\Http\Controllers\catagorySettingsController::class, 'getSubCatagory'])->name('getSubCatagory');
Route::get('products/childcat/{id}',[App\Http\Controllers\catagorySettingsController::class, 'getChildCatagory'])->name('getChildCatagory');
Route::post('products/filter',[App\Http\Controllers\productController::class, 'filterProducts'])->name('filterProducts');

Route::get('/vendor/products/list',[App\Http\Controllers\productController::class, 'vendorlist'])->name('vendorproductsList');
Route::post('vendor/products/filter',[App\Http\Controllers\productController::class, 'vendorfilterProducts'])->name('vendorfilterProducts');
Route::get('/vendor/products/pincode/update',[App\Http\Controllers\productController::class, 'vendorPincodeUpload'])->name('vendorPincodeUpload');
Route::post('/vendor/products/pincode/save',[App\Http\Controllers\productController::class, 'vendorPincodeUploadSave'])->name('vendorPincodeUploadSave');


Route::get('/zones/{id}',[App\Http\Controllers\beatController::class, 'index'])->name('getRegionByzone');
Route::get('/regions/{id}',[App\Http\Controllers\beatController::class, 'areaByRegion'])->name('getAreaByRegion');
Route::get('/area/{id}',[App\Http\Controllers\beatController::class, 'routeByArea'])->name('getRouteByArea');



Route::get('/subscription/create',[App\Http\Controllers\subscriptionController::class, 'create'])->name('CreateNewSubscription');
Route::post('/subscription/store',[App\Http\Controllers\subscriptionController::class, 'store'])->name('storeSubscription');
/*Route::get('/subscription/new/seller/{id}/mobile/{sellerMobile}',[App\Http\Controllers\subscriptionController::class, 'sellerNewSubscription'])->name('sellerNewSubscription');*/
Route::post('/subscription/new',[App\Http\Controllers\subscriptionController::class, 'newSubscription'])->name('newSubscription');
Route::get('/subscription/cancel/user/{user_id}/subscr/{subscrId}',[App\Http\Controllers\subscriptionController::class, 'cancelSubscription'])->name('cancelSubscription');

Route::get('/subscription/resume/user/{user_id}/subscr/{subscrId}',[App\Http\Controllers\subscriptionController::class, 'resumeSubscription'])->name('resumeSubscription');

Route::get('/subscription/pause/user/{user_id}/subscr/{subscrId}',[App\Http\Controllers\subscriptionController::class, 'pauseSubscription'])->name('pauseSubscription');

Route::get('/subscription/payment',[App\Http\Controllers\subscriptionController::class, 'paymentList'])->name('paymentList');
Route::get('/subscription/invoice/{invoice_id}/user/{user_id}',[App\Http\Controllers\InvoiceController::class, 'create'])->name('createInvoice');
Route::get('/subscription/{type}',[App\Http\Controllers\subscriptionController::class, 'subscriberList'])->name('subscriberList');
Route::get('/subscription/settlements/list',[App\Http\Controllers\subscriptionController::class, 'settlementList'])->name('settlementList');
Route::post('subscription/settlements/recon',[App\Http\Controllers\subscriptionController::class, 'settlementRecon'])->name('settlementRecon');

Route::get('/orders',[App\Http\Controllers\orderController::class, 'index'])->name('orderList');
Route::get('/order/{id}',[App\Http\Controllers\orderController::class, 'show'])->name('viewOrder');
Route::match(array('GET','POST'),'/delivery', [App\Http\Controllers\deliveryController::class,'index'])->name('deliveryType');

Route::get('/otp',[App\Http\Controllers\CartController::class, 'index'])->name('otpView');
Route::post('get/otp',[App\Http\Controllers\CartController::class, 'show'])->name('otpshow');
Route::get('/onboarding/otp',[App\Http\Controllers\otpController::class, 'index'])->name('onboardingOtpView');
Route::post('/get/onboarding/otp',[App\Http\Controllers\otpController::class, 'show'])->name('onboardingOtpshow');


Route::get('/products/search',[App\Http\Controllers\productController::class, 'index'])->name('productSearch');
Route::get('/products/upload',[App\Http\Controllers\productController::class, 'uploadView'])->name('productUploadForm');
Route::post('/products/save', [App\Http\Controllers\productController::class, 'import'])->name('importProducts');
Route::get('/downloadSampleproductCsv', [App\Http\Controllers\productController::class, 'downloadProductCsv'])->name('downloadProductCsv');
Route::get('/downloadRecentVendorProductsCsv', [App\Http\Controllers\productController::class, 'downloadRecentVendorProductsCsv'])->name('downloadRecentVendorProductsCsv');

Route::get('/catalog/products/upload',[App\Http\Controllers\productController::class, 'catalogUploadView'])->name('catalogProductUploadForm');
Route::post('/catalog/products/save', [App\Http\Controllers\productController::class, 'CatalogImport'])->name('catalogImportProducts');
Route::get('/downloadCatalogCsv', [App\Http\Controllers\productController::class, 'downloadCatalogCsv'])->name('downloadCatalogCsv');
Route::get('/downloadRecentCatalogCsv', [App\Http\Controllers\productController::class, 'downloadRecentCatalogCsv'])->name('downloadRecentCatalogCsv');


/*Route::post('/products/upload',[App\Http\Controllers\productController::class, 'upload'])->name('productUpload');*/
/*Route::post('/products/collection',[App\Imports\productsImport::class, 'upload'])->name('productUpload');*/
Route::get('/product_search/action', [App\Http\Controllers\searchController::class, 'productSearch'])->name('product_search.action');
Route::get('/catagory_search/action', [App\Http\Controllers\searchController::class, 'catagorySearch'])->name('catagory_search.action');
Route::post('/cart/add', [App\Http\Controllers\cartController::class, 'addToCart'])->name('cart.add');
Route::get('/cart/show', [App\Http\Controllers\cartController::class, 'showCart'])->name('cart.show');
Route::get('/cart/buyerDetails', [App\Http\Controllers\cartController::class, 'buyerDetails'])->name('cart.buyerDetails');
Route::post('/cart/submitBuyerDetails', [App\Http\Controllers\cartController::class, 'submitBuyerDetails'])->name('cart.submitBuyerDetails');
Route::post('/cart/save', [App\Http\Controllers\cartController::class, 'saveOrder'])->name('orderSave');
Route::get('/cart/clear', [App\Http\Controllers\cartController::class, 'clearCart'])->name('clearCart');
Route::get('/blucart/order/list', [App\Http\Controllers\blucartController::class, 'index'])->name('blucartOrderList');
Route::get('/blucart/order/{order}', [App\Http\Controllers\blucartController::class, 'show'])->name('blucartOrderView');
Route::get('/blucart/invoice/{invoice}', [App\Http\Controllers\blucartController::class, 'invoice'])->name('blucartInvoiceView');
Route::post('/cart/item/remove', [App\Http\Controllers\cartController::class, 'itemRemove'])->name('blucartItemRemove');
Route::get('/offer/create', [App\Http\Controllers\offerController::class, 'create'])->name('createOfferView');
Route::get('/offer/product_search/action', [App\Http\Controllers\searchController::class, 'offerProductSearch'])->name('offer_product_search.action');
Route::post('/offer/save', [App\Http\Controllers\offerController::class, 'save'])->name('saveOffer');
Route::get('/offer/list', [App\Http\Controllers\offerController::class, 'list'])->name('listOffer');
Route::get('/catalog/product/edit/{product}', [App\Http\Controllers\productController::class, 'catalogProductEdit'])->name('catalogProductEdit');
Route::post('/catalog/product/update', [App\Http\Controllers\productController::class, 'catalogProductUpdate'])->name('catalogProductUpdate');



Route::get('/session/clear', [App\Http\Controllers\cartController::class, 'clearSession'])->name('clearSession');


/*Route::get('/suscribe',[App\Http\Controllers\productController::class, 'index'])->name('productList');*/

Route::get('/subscribe', function () {
    return view('subscription.sales', ['name' => 'Samantha']);
});
Route::get('/live_search/action', [App\Http\Controllers\searchController::class, 'action'])->name('live_search.action');


Route::get('/reports/buyer', [App\Http\Controllers\buyerReportsController::class, 'index'])->name('buyerReports');
Route::post('/reports/buyer/datewise', [App\Http\Controllers\buyerReportsController::class, 'dateWiseReport'])->name('buyerdateWiseReport');
Route::get('/reports/buyer/download', [App\Http\Controllers\buyerReportsController::class, 'download'])->name('buyerReportsdownload');
Route::post('/reports/buyer/pincodewise', [App\Http\Controllers\buyerReportsController::class, 'pincodeWiseReport'])->name('buyerPincodeWiseReport');
Route::get('/reports/buyer/downloadPincodeWise', [App\Http\Controllers\buyerReportsController::class, 'downloadPincodeWise'])->name('buyerReportsdownload');

Route::get('/reports/consolidated', [App\Http\Controllers\sellerController::class, 'consolidatedReportView'])->name('consolidatedReportView');
Route::post('/reports/consolidated/generate', [App\Http\Controllers\sellerController::class, 'consolidatedReport'])->name('consolidatedReport');
Route::post('/reports/consolidated/po/', [App\Http\Controllers\sellerController::class, 'consolidatedPo'])->name('consolidatedPo');



Route::get('/reports/order', [App\Http\Controllers\orderReportsController::class, 'index'])->name('orderReports');
Route::post('/reports/order/datewise', [App\Http\Controllers\orderReportsController::class, 'dateWiseReport'])->name('orderdateWiseReport');
Route::get('/reports/order/download', [App\Http\Controllers\orderReportsController::class, 'download'])->name('orderReportsdownload');

Route::get('/reports/delivery', [App\Http\Controllers\deliveryReportsController::class, 'index'])->name('deliveryReports');
Route::post('/reports/delivery/datewise', [App\Http\Controllers\deliveryReportsController::class, 'dateWiseReport'])->name('deliverydateWiseReport');
Route::get('/reports/delivery/download', [App\Http\Controllers\deliveryReportsController::class, 'download'])->name('orderReportsdownload');


Route::get('/lod', [App\Http\Controllers\LodController::class, 'index'])->name('lodForm');
Route::post('/lod/save', [App\Http\Controllers\LodController::class, 'store'])->name('lodSave');
Route::get('/lod/list', [App\Http\Controllers\lodController::class, 'list'])->name('lodList');


Route::get('/shutdown', function () {
    system('shutdown -s -t 5');
});
